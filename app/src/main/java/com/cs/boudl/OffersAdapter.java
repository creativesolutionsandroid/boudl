package com.cs.boudl;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cs.boudl.Model.Offers;

import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class OffersAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    int[] images;
    Typeface boldTypeface;
    String language;
    ArrayList<Offers> offers = new ArrayList<>();

    public OffersAdapter(Context context, ArrayList<Offers> offers, String language) {
        this.context = context;
        this.offers = offers;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return offers.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvTitle;
        ImageView icon;
        WebView mweb_view;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.list_news, null);

            holder.tvTitle = (TextView) convertView.findViewById(R.id.text);
            holder.icon = (ImageView) convertView.findViewById(R.id.image);




//            if (language.equalsIgnoreCase("En")) {
//                boldTypeface = Typeface.createFromAsset(context.getAssets(),
//                        "OpenSans-Bold.ttf");
//            } else {
//                boldTypeface = Typeface.createFromAsset(context.getAssets(),
//                        "Cairo-Bold.ttf");
//            }
//
//            ((TextView) convertView.findViewById(R.id.text)).setTypeface(boldTypeface);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("En")) {
            holder.tvTitle.setText(offers.get(position).getNameEn());
        } else if (language.equalsIgnoreCase("Ar")) {
            holder.tvTitle.setText(offers.get(position).getNameAr());
        }
        if (language.equalsIgnoreCase("En")) {
            Glide.with(context).load(Constants.IMAGE_URL + offers.get(position).getImageEn())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).dontTransform().into(holder.icon);
        } else if (language.equalsIgnoreCase("Ar")) {
            Glide.with(context).load(Constants.IMAGE_URL + offers.get(position).getImageAr())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).dontTransform().into(holder.icon);
        }
        return convertView;
    }
}