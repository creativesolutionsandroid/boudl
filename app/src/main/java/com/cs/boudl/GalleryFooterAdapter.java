package com.cs.boudl;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.cs.boudl.Model.Gallery;
import com.cs.boudl.Model.Hotalname;
import com.cs.boudl.activity.PhotoGalleryFragment;

import java.util.ArrayList;
import java.util.List;

import static com.cs.boudl.activity.PhotoGalleryFragment.galleryRecyclerAdapter;
import static com.cs.boudl.activity.PhotoGalleryFragment.img;
import static com.cs.boudl.activity.PhotoGalleryFragment.selectedHotel;

public class GalleryFooterAdapter extends RecyclerView.Adapter<GalleryFooterAdapter.ViewHolder> {


    ArrayList<Gallery> galleryArrayList = new ArrayList<>();
    ArrayList<String> city_name_en = new ArrayList<>();
    ArrayList<String> city_name_ar = new ArrayList<>();

    ArrayList<Hotalname> hotalnames = new ArrayList<>();
//    ArrayList<String> hotelname_Ar = new ArrayList<>();
//    public static String value = "null";


    private LayoutInflater mInflater;
    String language;
    public static ImageView itemImage;
    Activity parentActivity;
    public Context context;
    ViewPager viewpager;
    ViewPageAdapter viewPageAdapter;
    boolean dropdown = false;
    private static RecyclerViewClickListener itemListener;
    List<TextView> cardViewList = new ArrayList<>();


    public GalleryFooterAdapter(Context context, RecyclerViewClickListener itemListener, ArrayList<Hotalname> hotelnames, ArrayList<Gallery> galleryArrayList, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.hotalnames = hotelnames;
//        this.hotelname_Ar = hotelname_Ar;
        this.galleryArrayList = galleryArrayList;
        this.itemListener = itemListener;
        this.parentActivity = parentActivity;
        this.language = language;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.i("TAG", "curst size1 " + orderList.size());
        View view = null;
//        if (language.equalsIgnoreCase("En")) {
        view = mInflater.inflate(R.layout.gallery_footer, parent, false);
//        } else if (language.equalsIgnoreCase("Ar")) {
//            view = mInflater.inflate(R.layout.curst_list, parent, false);
//        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        if (language.equalsIgnoreCase("En")) {
            holder.title.setText(hotalnames.get(position).getHotalname_en());

        } else {
            holder.layout.setRotationY(180);
            holder.title.setText(hotalnames.get(position).getHotalname_ar());
        }

        if (hotalnames.get(position).getHotalname_en().equalsIgnoreCase("Aber Hotel") || hotalnames.get(position).getHotalname_en().equalsIgnoreCase("Narcissus")) {

            holder.spinner.setMinimumWidth(100);

        } else {

            holder.spinner.setMinimumWidth(70);

        }
        cardViewList.add(holder.title);
        if (selectedHotel.equalsIgnoreCase(hotalnames.get(position).getHotalname_en())) {
            holder.title.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_selected_bg));
            holder.title.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.title.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_bg));
            holder.title.setTextColor(context.getResources().getColor(R.color.black));
        }

        holder.spinner.setVisibility(View.GONE);


        if (language.equalsIgnoreCase("En")) {
            if (position != 0) {
                final ArrayList<String> cities = new ArrayList<>();
                for (int i = 0; i < hotalnames.get(position).getCitynames().size(); i++) {
                    cities.add(hotalnames.get(position).getCitynames().get(i).getCityname_en());
                }

//
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.list_spinner_citys, cities) {
//                    public View getView(int position, View convertView, ViewGroup parent) {
//                        View v = super.getView(position, convertView, parent);
//
//                        ((TextView) v).setTextSize(0);
//                        ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.white));
//
//                        return v;
//                    }
//
//                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                        View v = super.getDropDownView(position, convertView, parent);
//                        v.setBackgroundResource(R.color.white);
//                        ((TextView) v).setTextSize(15);
//                        ((TextView) v).setGravity(Gravity.LEFT);
//                        ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.black));
//
//                        return v;
//                    }
//                };
//
//                holder.spinner.setAdapter(adapter);
//                holder.spinner.setSelection(0, false);
            }

            holder.title.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectedHotel = hotalnames.get(position).getHotalname_en();
                    PhotoGalleryFragment.img.clear();
                    PhotoGalleryFragment.mediaid.clear();
                    if (selectedHotel.equalsIgnoreCase("all")) {
                        for (int i = 1; i < hotalnames.size(); i++) {
                            for (int j = 0; j < hotalnames.get(i).getCitynames().size(); j++) {
                                PhotoGalleryFragment.img.addAll(hotalnames.get(i).getCitynames().get(j).getImagePath());
                                PhotoGalleryFragment.mediaid.addAll(hotalnames.get(i).getCitynames().get(j).getMediaTypeId());
                            }
                        }
                    } else {
                        for (int i = 1; i < hotalnames.size(); i++) {
                            if (hotalnames.get(i).getHotalname_en().equalsIgnoreCase(selectedHotel)) {
                                for (int j = 0; j < hotalnames.get(i).getCitynames().size(); j++) {
                                    PhotoGalleryFragment.img.addAll(hotalnames.get(i).getCitynames().get(j).getImagePath());
                                    PhotoGalleryFragment.mediaid.addAll(hotalnames.get(i).getCitynames().get(j).getMediaTypeId());
                                }
                                break;
                            }
                        }
                    }
                    galleryRecyclerAdapter = new GalleryRecyclerAdapter(context, PhotoGalleryFragment.mediaid, PhotoGalleryFragment.img, language);
                    PhotoGalleryFragment.Gallery_view.setAdapter(galleryRecyclerAdapter);

                    for (TextView cardView : cardViewList) {
                        cardView.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_bg));
                        cardView.setTextColor(context.getResources().getColor(R.color.black));
                    }
                    holder.title.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_selected_bg));
                    holder.title.setTextColor(context.getResources().getColor(R.color.white));
//                    holder.spinner.performClick();
//                        notifyDataSetChanged();
                    return true;
                }
            });
//            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
//                    PhotoGalleryFragment.img.clear();
//                    PhotoGalleryFragment.mediaid.clear();
//
//                    PhotoGalleryFragment.img = hotalnames.get(position).getCitynames().get(pos).getImagePath();
//                    PhotoGalleryFragment.mediaid = hotalnames.get(position).getCitynames().get(pos).getMediaTypeId();
//
//                    Log.d("whw", "spinner city selected: " + PhotoGalleryFragment.img.size());
//                    galleryRecyclerAdapter = new GalleryRecyclerAdapter(context, PhotoGalleryFragment.mediaid, PhotoGalleryFragment.img, language);
//                    PhotoGalleryFragment.Gallery_view.setAdapter(galleryRecyclerAdapter);
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                }
//            });

        } else if (language.equalsIgnoreCase("Ar")) {

            if (position != 0) {
                final ArrayList<String> cities_ar = new ArrayList<>();
                for (int i = 0; i < hotalnames.get(position).getCitynames().size(); i++) {
                    cities_ar.add(hotalnames.get(position).getCitynames().get(i).getCityname_ar());
                }

//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.list_spinner_citys, cities_ar) {
//                    public View getView(int position, View convertView, ViewGroup parent) {
//                        View v = super.getView(position, convertView, parent);
//
//                        ((TextView) v).setTextSize(0);
//                        ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.white));
//
//                        return v;
//                    }
//
//                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                        View v = super.getDropDownView(position, convertView, parent);
//                        v.setBackgroundResource(R.color.white);
//                        ((TextView) v).setTextSize(15);
//                        ((TextView) v).setGravity(Gravity.LEFT);
//                        ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.black));
//
//                        return v;
//                    }
//                };
//
//                holder.spinner.setAdapter(adapter);
//                holder.spinner.setSelection(0, true);
            }

            holder.title.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectedHotel = hotalnames.get(position).getHotalname_en();
                    PhotoGalleryFragment.img.clear();
                    PhotoGalleryFragment.mediaid.clear();
                    if (selectedHotel.equalsIgnoreCase("all")) {
                        for (int i = 1; i < hotalnames.size(); i++) {
                            for (int j = 0; j < hotalnames.get(i).getCitynames().size(); j++) {
                                PhotoGalleryFragment.img.addAll(hotalnames.get(i).getCitynames().get(j).getImagePath());
                                PhotoGalleryFragment.mediaid.addAll(hotalnames.get(i).getCitynames().get(j).getMediaTypeId());
                            }
                        }
//                    }else if (selectedHotel.equalsIgnoreCase("Aber Hotel")) {
//                        for (int i = 1; i < hotalnames.size(); i++) {
//                            if (hotalnames.get(i).getHotalname_en().equalsIgnoreCase("Aber Hotel")) {
//                                for (int j = 0; j < hotalnames.get(i).getCitynames().size(); j++) {
//                                    PhotoGalleryFragment.img.addAll(hotalnames.get(i).getCitynames().get(j).getImagePath());
//                                    PhotoGalleryFragment.mediaid.addAll(hotalnames.get(i).getCitynames().get(j).getMediaTypeId());
//                                }
//                                break;
//                            }
//                        }
//
                    } else {
                        for (int i = 1; i < hotalnames.size(); i++) {
                            if (hotalnames.get(i).getHotalname_en().equalsIgnoreCase(selectedHotel)) {
                                for (int j = 0; j < hotalnames.get(i).getCitynames().size(); j++) {
                                    PhotoGalleryFragment.img.addAll(hotalnames.get(i).getCitynames().get(j).getImagePath());
                                    PhotoGalleryFragment.mediaid.addAll(hotalnames.get(i).getCitynames().get(j).getMediaTypeId());
                                }
                                break;
                            }
                        }
                    }


                    Log.d("TAG", "name: " + selectedHotel);
                    Log.d("TAG", "onTouch: " + img.size());
                    galleryRecyclerAdapter = new GalleryRecyclerAdapter(context, PhotoGalleryFragment.mediaid, PhotoGalleryFragment.img, language);
                    PhotoGalleryFragment.Gallery_view.setAdapter(galleryRecyclerAdapter);

                    for (TextView cardView : cardViewList) {
                        cardView.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_bg));
                        cardView.setTextColor(context.getResources().getColor(R.color.black));
                    }
                    holder.title.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_selected_bg));
                    holder.title.setTextColor(context.getResources().getColor(R.color.white));
//                    holder.spinner.performClick();
//                        notifyDataSetChanged();
                    return true;
                }
            });
//            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
//                    PhotoGalleryFragment.img.clear();
//                    PhotoGalleryFragment.mediaid.clear();
//
//                    if (pos == 0) {
//
//                        PhotoGalleryFragment.img = hotalnames.get(position).getCitynames().get(0).getImagePath();
//                        PhotoGalleryFragment.mediaid = hotalnames.get(position).getCitynames().get(0).getMediaTypeId();
//                    } else {
//
//                        PhotoGalleryFragment.img = hotalnames.get(position).getCitynames().get(pos).getImagePath();
//                        PhotoGalleryFragment.mediaid = hotalnames.get(position).getCitynames().get(pos).getMediaTypeId();
//                    }
//
//
//                    Log.d("TAG", "pos: " + pos);
//                    Log.d("TAG", "name: " + holder.spinner.getSelectedItem().toString());
//                    Log.d("TAG", "spinner city selected: " + PhotoGalleryFragment.img.size());
//                    galleryRecyclerAdapter = new GalleryRecyclerAdapter(context, PhotoGalleryFragment.mediaid, PhotoGalleryFragment.img, language);
//                    PhotoGalleryFragment.Gallery_view.setAdapter(galleryRecyclerAdapter);
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                }
//            });

        }

    }


    @Override
    public int getItemCount() {
        return hotalnames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout layout;
        TextView title;
        Spinner spinner;

//        ImageView curstImage;

        public ViewHolder(View itemView) {
            super(itemView);
//            Log.i("TAG", "curst size4 " + orderList.size());

            title = itemView.findViewById(R.id.gallery_footer_txt);
            itemImage = itemView.findViewById(R.id.image1);
            viewpager = itemView.findViewById(R.id.pager);
            layout = itemView.findViewById(R.id.layout);
            spinner = itemView.findViewById(R.id.city_spinner);
//            curstImage = itemView.findViewById(R.id.curst_img_layout);

//            layout.setOnClickListener(this);
        }

//        @Override
//        public void onClick(View v) {
//
//
//
//            int pos = getAdapterPosition();
//
//            for (int i = 0; i < galleryArrayList.size(); i++) {
//                if (galleryArrayList.get(i).getHotalnameEn().contains(hotelname_En.get(pos))) {
//
//                    if (!city_name_en.contains(galleryArrayList.get(i).getCityNameEn())) {
//                        city_name_en.add(galleryArrayList.get(i).getCityNameEn());
//                    }
//
//                }
//            }
//
//            Log.d("TAG", "adapterpos: " + pos);
//
//            if (hotelname_En.get(pos).contains(hotelname_En.get(pos))) {
//
//                spinner.setBackgroundResource(R.drawable.gallery_footer_selected_bg);
//
//            }
//
//
//        }
    }
}

