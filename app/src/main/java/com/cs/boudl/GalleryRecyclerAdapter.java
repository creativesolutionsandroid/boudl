package com.cs.boudl;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.boudl.Model.Gallery;

import java.util.ArrayList;

public class GalleryRecyclerAdapter extends BaseAdapter {

    ArrayList<Gallery> orderList = new ArrayList<>();
    ArrayList<String> img = new ArrayList<>();
    ArrayList<String> mediaid = new ArrayList<>();

    private LayoutInflater mInflater;
    String language;
    public static ImageView itemImage;
    Activity parentActivity;
    public Context context;
    ViewPager viewpager;
    ViewPageAdapter viewPageAdapter;


    public GalleryRecyclerAdapter(Context context, ArrayList<String> mediaid, ArrayList<String> img, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.mediaid = mediaid;
        this.img = img;
        this.parentActivity = parentActivity;
        this.language = language;
        this.context = context;
//        Log.i("TAG", "curst size " + this.orderList.size());
    }

//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.i("TAG", "curst size1 " + orderList.size());
//        View view = null;
////        if (language.equalsIgnoreCase("En")) {
//        view = mInflater.inflate(R.layout.gallery_list, parent, false);
////        } else if (language.equalsIgnoreCase("Ar")) {
////            view = mInflater.inflate(R.layout.curst_list, parent, false);
////        }
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolder holder, final int position) {
//        Log.i("TAG", "curst size2 " + orderList.size());

//        holder.title.setText(orderList.get(position).getAdditionalName());

//        if (orderList.get(position).getMediatypeid().equals("1")) {
//
//            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(position).getImagepath()).placeholder(R.drawable.boudl_logo).into(itemImage);
//
//        } else {
//
//            String videoId = orderList.get(position).getImagepath();
//
//            Log.e("VideoId is->","" + videoId);
//
//            String img_url="http://img.youtube.com/vi/"+videoId+"/0.jpg";
//
//            Glide.with(context)
//                    .load(img_url)
//                    .placeholder(R.drawable.boudl_logo)
//                    .into(itemImage);
//
//        }
//
////        if (orderList.get(position).getMediatypeid().equals("1")) {
//
//            itemImage.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//             /
////                    vi
////
////                    viewPageAdapter = new ViewPageAdapter(context, orderList , position, (Activity) context);
////                    viewpager.setCurrentItem(position);
////                    viewpager.setAdapter(viewPageAdapter);
//                }
//            });

//        } else {
//
//            itemImage.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//            });
//        }
//
//        Log.i("TAG", "curst size " + orderList.size());
//        Log.i("TAG", "name " + orderList.get(position).getAdditionalName());
//        Log.i("TAG", "img " + orderList.get(position).getImages());

//    }

//    @Override
//    public int getItemCount() {
//        Log.i("TAG", "curst size3 " + orderList.size());
//        return orderList.size();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
////        TextView title;
//
////        ImageView curstImage;
//
//        public ViewHolder(View itemView) {
//            super(itemView);
//            Log.i("TAG", "curst size4 " + orderList.size());
//
////            title = itemView.findViewById(R.id.curst_name);
//            itemImage = itemView.findViewById(R.id.image1);
//            viewpager = itemView.findViewById(R.id.pager);
////            curstImage = itemView.findViewById(R.id.curst_img_layout);
//
//
//        }
//    }

    @Override
    public int getCount() {
        return img.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder{
        TextView title;
        ImageView play_image;
        RelativeLayout layout;
    }

    @Override
    public View getView( int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;

            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.gallery_list, null);

            itemImage = convertView.findViewById(R.id.image1);
            viewpager = convertView.findViewById(R.id.pager);
            holder.play_image = (ImageView) convertView.findViewById(R.id.play_img);
            holder.layout = convertView.findViewById(R.id.layout1);

//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }

        if (language.equalsIgnoreCase("Ar")){

            holder.layout.setRotationY(180);

        }

        if (mediaid.get(position).equals("1")) {

            holder.play_image.setVisibility(View.GONE);

            Glide.with(context).load(Constants.IMAGE_URL + img.get(position)).into(itemImage);

        } else {

            holder.play_image.setVisibility(View.VISIBLE);

            String videoId = img.get(position);

            Log.e("VideoId is->","" + videoId);


            String img_url="http://img.youtube.com/vi/"+videoId+"/0.jpg";

            Glide.with(context)
                    .load(img_url)
                    .placeholder(R.drawable.boudl_logo)
                    .into(itemImage);

        }

//        if (orderList.get(position).getMediatypeid().equals("1")) {

//        itemImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                    vi
//
//                    viewPageAdapter = new ViewPageAdapter(context, orderList , position, (Activity) context);
//                    viewpager.setCurrentItem(position);
//                    viewpager.setAdapter(viewPageAdapter);
//            }
//        });


        return convertView;
    }

}
