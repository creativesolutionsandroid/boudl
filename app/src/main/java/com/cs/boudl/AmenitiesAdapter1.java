package com.cs.boudl;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class AmenitiesAdapter1 extends BaseAdapter {

    String[] orderList;

    private LayoutInflater mInflater;
    String language;
    public static ImageView itemImage;
    Activity parentActivity;
    public Context context;
    ViewPager viewpager;
    ViewPageAdapter viewPageAdapter;
    String anemite_name, anemite_icon;
    ArrayList<String> amenities_name_en = new ArrayList<>();
    ArrayList<String> amenities_name_ar = new ArrayList<>();
    ArrayList<String> amenities_icon = new ArrayList<>();
    Typeface lightTypeface, semiBoldTypeface, boldTypeface;


    public AmenitiesAdapter1(Context context, ArrayList<String> anemties_name_en, ArrayList<String> anemties_name_ar, ArrayList<String> anemties_icon, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.amenities_name_en = anemties_name_en;
        this.amenities_name_ar = anemties_name_ar;
        this.amenities_icon = anemties_icon;
        this.parentActivity = parentActivity;
        this.language = language;
        this.context = context;
//        Log.i("TAG", "curst size " + this.orderList.length);
    }

    @Override
    public int getCount() {
        return amenities_name_en.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView amenities_txt;
        TextView amenities_img;
        LinearLayout layout;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if (language.equalsIgnoreCase("En")) {
                convertView = mInflater.inflate(R.layout.amenities, null);
            } else {
                convertView = mInflater.inflate(R.layout.amenities_ar, null);
            }

            holder.amenities_img = convertView.findViewById(R.id.amenities_img);
            holder.amenities_txt = convertView.findViewById(R.id.amenities_txt);
            holder.layout = convertView.findViewById(R.id.layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(context.getAssets(),
                    "OpenSans-Regular.ttf");
            boldTypeface = Typeface.createFromAsset(context.getAssets(),
                    "OpenSans-Bold.ttf");
        } else {
            lightTypeface = Typeface.createFromAsset(context.getAssets(),
                    "Cairo-Regular.ttf");
            boldTypeface = Typeface.createFromAsset(context.getAssets(),
                    "Cairo-Bold.ttf");
        }

        if (language.equalsIgnoreCase("Ar")) {
            holder.layout.setRotationY(180);
        }

        ((TextView) convertView.findViewById(R.id.amenities_txt)).setTypeface(lightTypeface);

        Typeface font = Typeface.createFromAsset(context.getAssets(), "fontello.ttf");
        Typeface font1 = Typeface.createFromAsset(context.getAssets(), "FontAwesome.ttf");

        String fontawesome = amenities_icon.get(position);
//
//        fontawesome = fontawesome.replace(" ","");
//        Log.d("TAG", "getView before: "+fontawesome);
//
//        fontawesome = fontawesome.substring(0, 1) + "u" + fontawesome.substring(1, fontawesome.length());
//
//        Log.d("TAG", "getView after: "+fontawesome);


        Log.d("TAG", "getView: unicode " + amenities_icon.get(position).replace(" ", ""));


        fontawesome = fontawesome.replace(" ", "");

        holder.amenities_img.setTypeface(font);

        holder.amenities_img.setText(new String(Character.toChars(Integer.parseInt(fontawesome.substring(2), 16))));

//        holder.amenities_img.setText(anemties_icon[position].replace(" ",""));

        if (language.equalsIgnoreCase("En"))
            holder.amenities_txt.setText(amenities_name_en.get(position));
        else {
            holder.amenities_txt.setText(amenities_name_ar.get(position));
        }

//        Log.d("TAG", "getView: " + "\uf640");


        return convertView;
    }
}
