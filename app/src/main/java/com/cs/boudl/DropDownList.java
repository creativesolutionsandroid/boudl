package com.cs.boudl;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class DropDownList extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<String> city_en = new ArrayList<>();
    ArrayList<String> city_ar = new ArrayList<>();
    String language;
    Activity parentActivity;
//    private DataBaseHelper myDbHelper;
    int value;


    public DropDownList (Context context, ArrayList<String> city_en, ArrayList<String> city_ar, String language) {
        this.context = context;
        this.city_en = city_en;
        this.city_ar = city_ar;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
    }

    @Override
    public int getCount() {
        return city_en.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView drop_list;
//        ImageView itemImage, minus, plus;
//        LinearLayout grid_layout;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.dropdownlist, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.drop_list = (TextView) convertView.findViewById(R.id.dropdown);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

//        if (orderList.get(value).getChildItems().get(position).getBuildingType().equals("1")){
//            holder.drop_list.setVisibility(View.GONE);
//        }else if (orderList.get(value).getChildItems().get(position).getBuildingType().equals("2")){
//            holder.drop_list.setVisibility(View.VISIBLE);
            if (language.equalsIgnoreCase("En")) {
                holder.drop_list.setText("" + city_en.get(position));
            }else if (language.equalsIgnoreCase("Ar")) {
                holder.drop_list.setText("" + city_ar.get(position));
            }
            Log.e("TAG","drop_list "+city_en.get(position));

//        holder.drop_list.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                PastriesSelection.value1 = position;
//                ((PastriesSelection)context).refreshData();
//            }
//        });



        return convertView;
    }
}
