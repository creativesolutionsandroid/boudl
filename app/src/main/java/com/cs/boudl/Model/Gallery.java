package com.cs.boudl.Model;

import java.io.Serializable;

public class Gallery implements Serializable {

    String id, hotelid, branchid, branchnameEn, branchnameAr, mediatypeid, imagepath, hotalnameEn, hotalnameAr, CityNameAr, CityNameEn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHotelid() {
        return hotelid;
    }

    public void setHotelid(String hotelid) {
        this.hotelid = hotelid;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public String getBranchnameEn() {
        return branchnameEn;
    }

    public void setBranchnameEn(String branchnameEn) {
        this.branchnameEn = branchnameEn;
    }

    public String getBranchnameAr() {
        return branchnameAr;
    }

    public void setBranchnameAr(String branchnameAr) {
        this.branchnameAr = branchnameAr;
    }

    public String getMediatypeid() {
        return mediatypeid;
    }

    public void setMediatypeid(String mediatypeid) {
        this.mediatypeid = mediatypeid;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getHotalnameEn() {
        return hotalnameEn;
    }

    public void setHotalnameEn(String hotalnameEn) {
        this.hotalnameEn = hotalnameEn;
    }

    public String getHotalnameAr() {
        return hotalnameAr;
    }

    public void setHotalnameAr(String hotalnameAr) {
        this.hotalnameAr = hotalnameAr;
    }

    public String getCityNameAr() {
        return CityNameAr;
    }

    public void setCityNameAr(String cityNameAr) {
        CityNameAr = cityNameAr;
    }

    public String getCityNameEn() {
        return CityNameEn;
    }

    public void setCityNameEn(String cityNameEn) {
        CityNameEn = cityNameEn;
    }
}
