package com.cs.boudl.Model;

import java.io.Serializable;

public class AmenitiesBooking implements Serializable {

    String Id, IconClass, UniCode, NameEn, NameAr;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getIconClass() {
        return IconClass;
    }

    public void setIconClass(String iconClass) {
        IconClass = iconClass;
    }

    public String getUniCode() {
        return UniCode;
    }

    public void setUniCode(String uniCode) {
        UniCode = uniCode;
    }

    public String getNameEn() {
        return NameEn;
    }

    public void setNameEn(String nameEn) {
        NameEn = nameEn;
    }

    public String getNameAr() {
        return NameAr;
    }

    public void setNameAr(String nameAr) {
        NameAr = nameAr;
    }
}
