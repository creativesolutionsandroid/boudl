package com.cs.boudl.Model;

import java.io.Serializable;

public class Banners implements Serializable {

    String MediaTypId, SourcePath;

    public String getMediaTypId() {
        return MediaTypId;
    }

    public void setMediaTypId(String mediaTypId) {
        MediaTypId = mediaTypId;
    }

    public String getSourcePath() {
        return SourcePath;
    }

    public void setSourcePath(String sourcePath) {
        SourcePath = sourcePath;
    }
}
