package com.cs.boudl.Model;

import java.io.Serializable;

public class Offers implements Serializable {

    String Id, HotelId, HotelNameEn, HotelNameAr, Mode, OfferBookingUrl, NameEn, NameAr, DescriptionEn, DescriptionAr, ImageEn, ImageAr, OfferPrice, StartDate, EndDate, BookingRedirection, SortIndex, IsActive, AmenitiesListEn, AmenitiesListAr,  AmenityIds, AmenitiesUniCode;


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getHotelId() {
        return HotelId;
    }

    public void setHotelId(String hotelId) {
        HotelId = hotelId;
    }

    public String getHotelNameEn() {
        return HotelNameEn;
    }

    public void setHotelNameEn(String hotelNameEn) {
        HotelNameEn = hotelNameEn;
    }

    public String getHotelNameAr() {
        return HotelNameAr;
    }

    public void setHotelNameAr(String hotelNameAr) {
        HotelNameAr = hotelNameAr;
    }

    public String getMode() {
        return Mode;
    }

    public void setMode(String mode) {
        Mode = mode;
    }

    public String getOfferBookingUrl() {
        return OfferBookingUrl;
    }

    public void setOfferBookingUrl(String offerBookingUrl) {
        OfferBookingUrl = offerBookingUrl;
    }

    public String getNameEn() {
        return NameEn;
    }

    public void setNameEn(String nameEn) {
        NameEn = nameEn;
    }

    public String getNameAr() {
        return NameAr;
    }

    public void setNameAr(String nameAr) {
        NameAr = nameAr;
    }

    public String getDescriptionEn() {
        return DescriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        DescriptionEn = descriptionEn;
    }

    public String getDescriptionAr() {
        return DescriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        DescriptionAr = descriptionAr;
    }

    public String getImageEn() {
        return ImageEn;
    }

    public void setImageEn(String imageEn) {
        ImageEn = imageEn;
    }

    public String getImageAr() {
        return ImageAr;
    }

    public void setImageAr(String imageAr) {
        ImageAr = imageAr;
    }

    public String getOfferPrice() {
        return OfferPrice;
    }

    public void setOfferPrice(String offerPrice) {
        OfferPrice = offerPrice;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getBookingRedirection() {
        return BookingRedirection;
    }

    public void setBookingRedirection(String bookingRedirection) {
        BookingRedirection = bookingRedirection;
    }

    public String getSortIndex() {
        return SortIndex;
    }

    public void setSortIndex(String sortIndex) {
        SortIndex = sortIndex;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getAmenitiesListEn() {
        return AmenitiesListEn;
    }

    public void setAmenitiesListEn(String amenitiesListEn) {
        AmenitiesListEn = amenitiesListEn;
    }

    public String getAmenitiesListAr() {
        return AmenitiesListAr;
    }

    public void setAmenitiesListAr(String amenitiesListAr) {
        AmenitiesListAr = amenitiesListAr;
    }

    public String getAmenityIds() {
        return AmenityIds;
    }

    public void setAmenityIds(String amenityIds) {
        AmenityIds = amenityIds;
    }

    public String getAmenitiesUniCode() {
        return AmenitiesUniCode;
    }

    public void setAmenitiesUniCode(String amenitiesUniCode) {
        AmenitiesUniCode = amenitiesUniCode;
    }
}
