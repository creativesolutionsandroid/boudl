package com.cs.boudl.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class Hotalname implements Serializable {

    String hotalname_en, hotalname_ar;
    ArrayList<Cityname> citynames;

    public ArrayList<Cityname> getCitynames() {
        return citynames;
    }

    public void setCitynames(ArrayList<Cityname> citynames) {
        this.citynames = citynames;
    }

    public String getHotalname_en() {
        return hotalname_en;
    }

    public void setHotalname_en(String hotalname_en) {
        this.hotalname_en = hotalname_en;
    }

    public String getHotalname_ar() {
        return hotalname_ar;
    }

    public void setHotalname_ar(String hotalname_ar) {
        this.hotalname_ar = hotalname_ar;
    }
}
