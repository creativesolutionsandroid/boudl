package com.cs.boudl.Model;

import java.io.Serializable;

public class Branch implements Serializable{

    String id, branchName, branchNameAr, link, link_en, link_ar;

    public String getLink_en() {
        return link_en;
    }

    public void setLink_en(String link_en) {
        this.link_en = link_en;
    }

    public String getLink_ar() {
        return link_ar;
    }

    public void setLink_ar(String link_ar) {
        this.link_ar = link_ar;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchNameAr() {
        return branchNameAr;
    }

    public void setBranchNameAr(String branchNameAr) {
        this.branchNameAr = branchNameAr;
    }
}
