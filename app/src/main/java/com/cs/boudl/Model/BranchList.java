package com.cs.boudl.Model;

public class BranchList {

    String id, hotelid, countryid, cityid, districtid, name_en, name_ar, bookingurl, UrlEn, UrlAr;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHotelid() {
        return hotelid;
    }

    public void setHotelid(String hotelid) {
        this.hotelid = hotelid;
    }

    public String getCountryid() {
        return countryid;
    }

    public void setCountryid(String countryid) {
        this.countryid = countryid;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getDistrictid() {
        return districtid;
    }

    public void setDistrictid(String districtid) {
        this.districtid = districtid;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getBookingurl() {
        return bookingurl;
    }

    public void setBookingurl(String bookingurl) {
        this.bookingurl = bookingurl;
    }

    public String getUrlEn() {
        return UrlEn;
    }

    public void setUrlEn(String urlEn) {
        UrlEn = urlEn;
    }

    public String getUrlAr() {
        return UrlAr;
    }

    public void setUrlAr(String urlAr) {
        UrlAr = urlAr;
    }
}
