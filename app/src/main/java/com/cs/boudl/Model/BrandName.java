package com.cs.boudl.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class BrandName implements Serializable {

    String brandname_en, brandname_ar;

    ArrayList<String> ImagePath ;
    ArrayList<String> MediaTypeId;


    public String getBrandname_en() {
        return brandname_en;
    }

    public void setBrandname_en(String brandname_en) {
        this.brandname_en = brandname_en;
    }

    public String getBrandname_ar() {
        return brandname_ar;
    }

    public void setBrandname_ar(String brandname_ar) {
        this.brandname_ar = brandname_ar;
    }

    public ArrayList<String> getImagePath() {
        return ImagePath;
    }

    public void setImagePath(ArrayList<String> imagePath) {
        ImagePath = imagePath;
    }

    public ArrayList<String> getMediaTypeId() {
        return MediaTypeId;
    }

    public void setMediaTypeId(ArrayList<String> mediaTypeId) {
        MediaTypeId = mediaTypeId;
    }
}
