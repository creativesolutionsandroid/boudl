package com.cs.boudl.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class Brand implements Serializable{
    String id, brandName, brandNameAr, logoEn, logoAr;
    ArrayList<City> cityArrayList;

    public String getLogoEn() {
        return logoEn;
    }

    public void setLogoEn(String logoEn) {
        this.logoEn = logoEn;
    }

    public String getLogoAr() {
        return logoAr;
    }

    public void setLogoAr(String logoAr) {
        this.logoAr = logoAr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandNameAr() {
        return brandNameAr;
    }

    public void setBrandNameAr(String brandNameAr) {
        this.brandNameAr = brandNameAr;
    }

    public ArrayList<City> getCityArrayList() {
        return cityArrayList;
    }

    public void setCityArrayList(ArrayList<City> cityArrayList) {
        this.cityArrayList = cityArrayList;
    }
}
