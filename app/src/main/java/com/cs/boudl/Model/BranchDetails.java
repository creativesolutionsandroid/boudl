package com.cs.boudl.Model;

import java.io.Serializable;

public class BranchDetails implements Serializable {

    String Id, UserId,HotelId, HotelNameEn, CountryId, CityId, DistrictId, NameEn, NameAr, AboutEn, AboutAr, Phone, Email, Landline, FaxNo, AddressEn, AddressAr, Latitude,
            Longitude, BookingUrl, ImagePath, BranchRating, IsActive, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy, DeletedOn, DeletedBy, IsDeleted, BranchList, AmenityIds, message, FlagId;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getHotelId() {
        return HotelId;
    }

    public void setHotelId(String hotelId) {
        HotelId = hotelId;
    }

    public String getHotelNameEn() {
        return HotelNameEn;
    }

    public void setHotelNameEn(String hotelNameEn) {
        HotelNameEn = hotelNameEn;
    }

    public String getCountryId() {
        return CountryId;
    }

    public void setCountryId(String countryId) {
        CountryId = countryId;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public String getDistrictId() {
        return DistrictId;
    }

    public void setDistrictId(String districtId) {
        DistrictId = districtId;
    }

    public String getNameEn() {
        return NameEn;
    }

    public void setNameEn(String nameEn) {
        NameEn = nameEn;
    }

    public String getNameAr() {
        return NameAr;
    }

    public void setNameAr(String nameAr) {
        NameAr = nameAr;
    }

    public String getAboutEn() {
        return AboutEn;
    }

    public void setAboutEn(String aboutEn) {
        AboutEn = aboutEn;
    }

    public String getAboutAr() {
        return AboutAr;
    }

    public void setAboutAr(String aboutAr) {
        AboutAr = aboutAr;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLandline() {
        return Landline;
    }

    public void setLandline(String landline) {
        Landline = landline;
    }

    public String getFaxNo() {
        return FaxNo;
    }

    public void setFaxNo(String faxNo) {
        FaxNo = faxNo;
    }

    public String getAddressEn() {
        return AddressEn;
    }

    public void setAddressEn(String addressEn) {
        AddressEn = addressEn;
    }

    public String getAddressAr() {
        return AddressAr;
    }

    public void setAddressAr(String addressAr) {
        AddressAr = addressAr;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getBookingUrl() {
        return BookingUrl;
    }

    public void setBookingUrl(String bookingUrl) {
        BookingUrl = bookingUrl;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getBranchRating() {
        return BranchRating;
    }

    public void setBranchRating(String branchRating) {
        BranchRating = branchRating;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getDeletedOn() {
        return DeletedOn;
    }

    public void setDeletedOn(String deletedOn) {
        DeletedOn = deletedOn;
    }

    public String getDeletedBy() {
        return DeletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        DeletedBy = deletedBy;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public String getBranchList() {
        return BranchList;
    }

    public void setBranchList(String branchList) {
        BranchList = branchList;
    }

    public String getAmenityIds() {
        return AmenityIds;
    }

    public void setAmenityIds(String amenityIds) {
        AmenityIds = amenityIds;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFlagId() {
        return FlagId;
    }

    public void setFlagId(String flagId) {
        FlagId = flagId;
    }
}
