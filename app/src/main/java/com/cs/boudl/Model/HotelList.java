package com.cs.boudl.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class HotelList implements Serializable{

    String id, name_en, name_ar, logo_en, logo_ar;

    ArrayList<CountrySubList> countrySubLists;

    public String getLogo_en() {
        return logo_en;
    }

    public void setLogo_en(String logo_en) {
        this.logo_en = logo_en;
    }

    public String getLogo_ar() {
        return logo_ar;
    }

    public void setLogo_ar(String logo_ar) {
        this.logo_ar = logo_ar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public ArrayList<CountrySubList> getCountrySubLists() {
        return countrySubLists;
    }

    public void setCountrySubLists(ArrayList<CountrySubList> countrySubLists) {
        this.countrySubLists = countrySubLists;
    }
}
