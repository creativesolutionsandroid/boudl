package com.cs.boudl.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class Cityname implements Serializable{

    String cityname_en, cityname_ar;

    ArrayList<BrandName> brandnames;
    ArrayList<String> ImagePath ;
    ArrayList<String> MediaTypeId;

    public ArrayList<BrandName> getBrandnames() {
        return brandnames;
    }

    public void setBrandnames(ArrayList<BrandName> brandnames) {
        this.brandnames = brandnames;
    }

    public ArrayList<String> getImagePath() {
        return ImagePath;
    }

    public void setImagePath(ArrayList<String> imagePath) {
        ImagePath = imagePath;
    }

    public ArrayList<String> getMediaTypeId() {
        return MediaTypeId;
    }

    public void setMediaTypeId(ArrayList<String> mediaTypeId) {
        MediaTypeId = mediaTypeId;
    }

    public String getCityname_en() {
        return cityname_en;
    }

    public void setCityname_en(String cityname_en) {
        this.cityname_en = cityname_en;
    }

    public String getCityname_ar() {
        return cityname_ar;
    }

    public void setCityname_ar(String cityname_ar) {
        this.cityname_ar = cityname_ar;
    }
}
