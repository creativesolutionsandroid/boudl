package com.cs.boudl;

public class Constants {

//    static String local_url = "http://csadms.com/BoudlGroupAPI/";
//    static String test_url = "http://csadms.com/BoudlGroupAPItest/";

    static String local_url = "http://services.boudlgroup.com/";
    static String test_url = "http://services.boudlgroup.com/";

    public static String Offers_URL = test_url+"api/OfferAPI/NewGetOfferService";
    public static String GalleryURL = test_url+"api/GalleryAPI/NewGalleryService";
    public static String New_URL = local_url+"api/NewsAPI/NewNewsService";
    public static String Notifications_URL = local_url+"api/UserDeviceTokenAPI/NewPNMessageService";
    public static String Device_Token_URL = local_url+"api/UserDeviceTokenAPI/NewDevicetoken";
    public static String Booking_URL = local_url+"api/UserDeviceTokenAPI/DashBoardService";
    public static String Booking_Detail_URL = local_url+"api/UserDeviceTokenAPI/BookingDetailService";


    public static String IMAGE_URL = "http://controlpanel.boudlgroup.com/Uploads/";
//    public static String IMAGE_URL = "http://csadms.com/BoudlGrouptest/Banners/";
}
