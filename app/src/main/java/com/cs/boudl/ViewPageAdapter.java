package com.cs.boudl;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import static com.cs.boudl.activity.PhotoGalleryFragment.viewPager;

public class ViewPageAdapter extends PagerAdapter {

    //    private static ArrayList<Gallery> images;
    ArrayList<String> img = new ArrayList<>();
    ArrayList<String> mediaid = new ArrayList<>();


    private LayoutInflater inflater;
    private static Context context;
    Activity parent;
    int pos, pos1 = 0;
    String language;
    private LinearLayout youtubeLayout;
    public static ImageView myImage;
    public static FrameLayout imageframe;
    public static RelativeLayout layout;
    public static WebView youtubeFragment;
    private static String TAG = "sudheer";
    private int currentPos = 0;

    public ViewPageAdapter(Context context, ArrayList<String> mediaid, ArrayList<String> img, int pos, Activity gallery, String language) {
        this.context = context;
        this.mediaid = mediaid;
        this.img = img;
        this.pos = pos;
        this.parent = gallery;
        this.language = language;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        if(youtubeFragment != null){
////            youtubeFragment.loadUrl("");
//            youtubeFragment.stopLoading();
////            youtubeFragment.destroy();
//        }
        container.removeView((ViewGroup) object);
    }

//    public void destroyItem(ViewGroup container, int position, Object object) {
//        destroyItem((View) container, position, object);
//    }

    public void destroyItem(View container, int position, Object object) {
        try {
            throw new UnsupportedOperationException("Required method destroyItem was not overridden");
        } catch (Exception e) {

        }
        destroyItem((ViewGroup) container, position, object);
    }

    @Override
    public int getCount() {
        Log.d("TAG", "getCount: " + img.size());
        return img.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        Log.d("TAG", "instantiateItem: ");
        View myImageLayout = inflater.inflate(R.layout.slider, view, false);
        myImage = (ImageView) myImageLayout
                .findViewById(R.id.image);
        youtubeLayout = (LinearLayout) myImageLayout.findViewById(R.id.youtube_layout);
        imageframe = myImageLayout.findViewById(R.id.image_frame);
        layout = myImageLayout.findViewById(R.id.layout);
        currentPos = position;
//        YouTubePlayerSupportFragment youtubeFragment = (YouTubePlayerSupportFragment)
//                ((AppCompatActivity)context).getSupportFragmentManager().findFragmentById(R.id.youtube_view);

        if (language.equalsIgnoreCase("Ar")) {
            layout.setRotationY(180);
        }

//        showViewContent(position);

        if (mediaid.get(position).equals("1")) {

            youtubeLayout.setVisibility(View.GONE);
            imageframe.setVisibility(View.VISIBLE);


            Glide.with(context).load(Constants.IMAGE_URL + img.get(position)).into(myImage);

        } else {

            youtubeFragment = myImageLayout.findViewById(R.id.youtube_view);
            youtubeLayout.setVisibility(View.VISIBLE);
            imageframe.setVisibility(View.GONE);
//
            youtubeFragment.getSettings().setJavaScriptEnabled(true);
            youtubeFragment.getSettings().setPluginState(WebSettings.PluginState.ON);
            youtubeFragment.setWebChromeClient(new WebChromeClient());
//            youtubeFragment.getSettings().setMediaPlaybackRequiresUserGesture(false);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int changedPos, float positionOffset, int positionOffsetPixels) {
                    Log.d(TAG, "onPageScrolled: " + mediaid.get(changedPos));
                    if (mediaid.get(changedPos).equals("1")) {
//                        if (youtubeFragment != null) {
                        Log.d(TAG, "onPageScrolled: ");
                        youtubeFragment.onPause();
//                        }
                    } else {
                        if (youtubeFragment != null) {
                            youtubeFragment.onResume();
                        }
                    }
                }

                @Override
                public void onPageSelected(int pos) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


            viewPager.addOnAdapterChangeListener(new ViewPager.OnAdapterChangeListener() {
                @Override
                public void onAdapterChanged(@NonNull ViewPager viewPager, @Nullable PagerAdapter oldAdapter, @Nullable PagerAdapter newAdapter) {
                    Log.d("sudheer", "adapter changed");
                    if (youtubeFragment != null) {
                        youtubeFragment.onPause();
                    }
                }
            });

//            if(currentPos == position) {
            String frameVideo = "<html><body style='margin:0px;padding:0px;'>\n" +
                    "        <script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>\n" +
                    "                var player;\n" +
                    "        function onYouTubeIframeAPIReady()\n" +
                    "        {player=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}\n" +
                    "        function onPlayerReady(event){player.playVideo();}\n" +
                    "        </script>\n" +
                    "        <iframe id='playerId' type='text/html' width='100%' height='250'\n" +
                    "        src='https://www.youtube.com/embed/"+img.get(position)+"?enablejsapi=1&autoplay=0' frameborder='0'>\n" +
                    "        </body></html>";
            youtubeFragment.loadDataWithBaseURL("http://www.youtube.com", frameVideo, "text/html", "utf-8", null);
//            }


//
//            ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction().replace(R.id.youtube_view, myFragment).commit();

//            String playVideo = "<html><body style='margin:0px;padding:0px;'><script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>function onYouTubeIframeAPIReady(){ytplayer=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}function onPlayerReady(a){a.target.playVideo();}</script><iframe id='playerId' type='text/html' width='\(self.view.bounds.size.width-16)' height='250' src='http://www.youtube.com/embed/\(videoStr)' frameborder='0'></body></html>";

        }

//            String playVideo= "<html><body> <br> <iframe class=\"youtube-player\" type=\"text/html\"width=\"100%\" height=\"250\" src=\"http://www.youtube.com/embed/"+images.get(position).getImagepath()+"?enablejsapi=1&rel=0&playsinline=1&autoplay=1\" frameborder=\"0\"></body></html>";

//            youtubeFragment.loadData(playVideo, "text/html", "utf-8");

//            final int finalPosition = position;
//
//            youtubeFragment.initialize(PlayerConfig.Api_key,
//                    new YouTubePlayer.OnInitializedListener() {
//                        @Override
//                        public void onInitializationSuccess(YouTubePlayer.Provider provider,
//                                                            YouTubePlayer youTubePlayer, boolean b) {
//                            // do any work here to cue video, play video, etc.
//                            youTubePlayer.cueVideo(images.get(position).getImagepath());
//                        }
//                        @Override
//                        public void onInitializationFailure(YouTubePlayer.Provider provider,
//                                                            YouTubeInitializationResult youTubeInitializationResult) {
//
//                        }
//                    });

//        }
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public static void showViewContent(int position) {


//        position = pos

    }
}
