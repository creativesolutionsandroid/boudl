package com.cs.boudl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.boudl.activity.BrandsFragment;
import com.cs.boudl.activity.ContactUsFragment;
import com.cs.boudl.activity.MembershipFragment;
import com.cs.boudl.activity.NewsFragment;
import com.cs.boudl.activity.NotificationFragment;
import com.cs.boudl.activity.OffersFragment;
import com.cs.boudl.activity.PhotoGalleryFragment;
import com.cs.boudl.activity.ProfileFragment;
import com.cs.boudl.activity.SearchActivity;
import com.cs.boudl.activity.SplashScreenActivity;
import com.cs.boudl.activity.WebViewActivity;
import com.cs.boudl.fragments.BookingFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MainActivity extends AppCompatActivity {

    SharedPreferences userPrefs;
    SharedPreferences.Editor  userPrefEditor;
    TextView msidemenu_name, login ,language1 ;
    String mLoginStatus;
    int selectedPos = 1;
    private static final int LOGIN_REQUEST = 1;

    String response;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    DrawerListAdapter adapter;
    private String[] mSidemenuTitles,mlanguage;
    private int[] unselected_icons;
    private RelativeLayout mDrawerLinear;
    private ListView mDrawerList;
//    boolean doubleBackToExitPressedOnce = false;
    FragmentManager fragmentManager = getSupportFragmentManager();

    String  language;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    TextView langAr,langEng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS",Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_main);
        }else {
            setContentView(R.layout.activity_main_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        mLoginStatus = userPrefs.getString("login_status", "");

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerLinear = (RelativeLayout) findViewById(R.id.left_drawer);
        msidemenu_name= (TextView) findViewById(R.id.sidemenu_name);
        language1=findViewById(R.id.language);

        if (language.equalsIgnoreCase("En")) {

//            if (mLoginStatus.equals("")) {
                mSidemenuTitles = new String[]{"", "Booking", "Brands", "Notification", "Photo Gallery",
                        "Membership", "News", "Offers", "Contact us", "Chat", "Language"};

                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_booking,
                        R.drawable.menu_brands,  R.drawable.menu_notification,
                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
                        R.drawable.menu_contactus, R.drawable.menu_chat, R.drawable.menu_language};

                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "", "","English"};

//            } else {
//                mSidemenuTitles = new String[]{"", "Profile", "Booking", "Brands", "Notification", "Photo Gallery",
//                        "Membership", "News", "Offers", "Reward Points", "Contact us", "Language"};
//
//
//                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_profile, R.drawable.menu_booking,
//                        R.drawable.menu_brands, R.drawable.menu_notification,
//                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
//                        R.drawable.menu_membership, R.drawable.menu_contactus ,R.drawable.menu_language};
//
//                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "","English"};
//            }
        }else if (language.equalsIgnoreCase("Ar")){
            
//            if (mLoginStatus.equals("")) {
                mSidemenuTitles = new String[]{"", "حجز", "العلامات التجارية",  "اشعارات", "معرض الصور",
                        "برنامج العضوية", "أخبار", "اخر العروض", "إتصل بنا", "تحدث معنا", "اللغة"};

                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_booking,
                        R.drawable.menu_brands,  R.drawable.menu_notification,
                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
                        R.drawable.menu_contactus , R.drawable.menu_chat, R.drawable.menu_language  };

                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "","","العربية"};
//            } else {
//                mSidemenuTitles = new String[]{"", "ملف شخصى ", "حجز", "العلامات التجارية",  "اشعارات", "معرض الصور",
//                        "برنامج العضوية", "إخبار", "أخر العروض", "نقاط المكافآت", "إتصل بنا" ,"اللغة"};
//
//
//                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_profile, R.drawable.menu_booking,
//                        R.drawable.menu_brands,  R.drawable.menu_notification,
//                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
//                        R.drawable.menu_membership, R.drawable.menu_contactus ,R.drawable.menu_language};
//
//                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "","العربية"};
//            }

        }

        if (language.equalsIgnoreCase("En")) {
            adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                    mSidemenuTitles, unselected_icons, selectedPos,language);
        }else if (language.equalsIgnoreCase("Ar")) {
            adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                    mSidemenuTitles, unselected_icons, selectedPos,language);
        }
        mDrawerList.setAdapter(adapter);

        configureToolbar();
        configureDrawer();

        Fragment fragment = new BookingFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        try {
            if(SplashScreenActivity.regId != null) {

                JSONObject mainObj = new JSONObject();
                mainObj.put("DeviceToken", SplashScreenActivity.regId);
                mainObj.put("Language", language);

                Log.i("TAG", mainObj.toString());

                new GetDeviceToken().execute(mainObj.toString());
            }
            else{
                Log.i("TAG","device token null");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void configureToolbar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mainToolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
//        mTitleTV.setText(mSidemenuTitles1[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);

                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    public void menuClick(){
        if (language.equalsIgnoreCase("En")) {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);

            } else {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        }else {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
                mDrawerLayout.closeDrawer(GravityCompat.END);

            } else {
                mDrawerLayout.openDrawer(GravityCompat.END);
            }
        }
    }

    public void configureDrawer() {
        // Configure drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_closed) {

            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
//                mTitleTV.setText(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
//                mTitleTV.setText(mAppTitle);
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            view.setSelected(true);
            selectItem(position);
        }
    }

    public void selectItem(int position) {

        // update the main content by replacing fragments
        switch (position) {
            case 1:

                Intent a=new Intent(MainActivity.this,SearchActivity.class);
                startActivity(a);
                selectedPos = 1;
                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mDrawerLayout.closeDrawer(mDrawerLinear);

//                mLoginStatus = userPrefs.getString("login_status", "");
//                if (mLoginStatus.equals("")){
//
//                    mSidemenuTitles = new String[]{"", "Profile", "Booking", "Brands", "Notification", "Photo Gallery",
//                            "Membership", "News", "Offers", "Contact us", "Language"};
//
//                    unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_profile, R.drawable.menu_booking,
//                            R.drawable.menu_brands,  R.drawable.menu_notification,
//                            R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
//                            R.drawable.menu_contactus, R.drawable.menu_language};
//
//                    Intent i = new Intent(MainActivity.this, SignInActivity.class);
//                    startActivityForResult(i, LOGIN_REQUEST);
//                }else {
//
//                    mSidemenuTitles = new String[]{"", "ملف شخصى ", "حجز", "العلامات التجارية",  "اشعارات", "معرض الصور",
//                            "برنامج العضوية", "إخبار", "أخر العروض", "نقاط المكافآت", "إتصل بنا" ,"اللغة"};
//
//
//                    unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_profile, R.drawable.menu_booking,
//                            R.drawable.menu_brands,  R.drawable.menu_notification,
//                            R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
//                            R.drawable.menu_membership, R.drawable.menu_contactus ,R.drawable.menu_language};
//
//                    Intent j=new Intent(MainActivity.this,ProfileFragment.class);
//                    startActivity(j);
//                }
//                mDrawerList.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;
            case 2:

                Intent b=new Intent(MainActivity.this,BrandsFragment.class);
                startActivity(b);
//                selectedPos = 3;
                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mDrawerLayout.closeDrawer(mDrawerLinear);

//                Intent a=new Intent(MainActivity.this,SearchActivity.class);
//                startActivity(a);
//                selectedPos = 2;
//                if (language.equalsIgnoreCase("En")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }else if (language.equalsIgnoreCase("Ar")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }
//                mDrawerList.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;
            case 3:
                Intent d=new Intent(MainActivity.this,NotificationFragment.class);
                startActivity(d);
//                selectedPos = 4;
                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mDrawerLayout.closeDrawer(mDrawerLinear);

//                Intent b=new Intent(MainActivity.this,BrandsFragment.class);
//                startActivity(b);
////                selectedPos = 3;
//                if (language.equalsIgnoreCase("En")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }else if (language.equalsIgnoreCase("Ar")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }
//                mDrawerList.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 4:
                Intent e=new Intent(MainActivity.this,PhotoGalleryFragment.class);
                startActivity(e);
//                selectedPos = 5;
                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mDrawerLayout.closeDrawer(mDrawerLinear);

//                Intent d=new Intent(MainActivity.this,NotificationFragment.class);
//                startActivity(d);
////                selectedPos = 4;
//                if (language.equalsIgnoreCase("En")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }else if (language.equalsIgnoreCase("Ar")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }
//                mDrawerList.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 5:
                Intent f=new Intent(MainActivity.this,MembershipFragment.class);
                startActivity(f);
//                selectedPos = 6;
                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mDrawerLayout.closeDrawer(mDrawerLinear);

//                Intent e=new Intent(MainActivity.this,PhotoGalleryFragment.class);
//                startActivity(e);
////                selectedPos = 5;
//                if (language.equalsIgnoreCase("En")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }else if (language.equalsIgnoreCase("Ar")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }
//                mDrawerList.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 6:
                Intent g=new Intent(MainActivity.this,NewsFragment.class);
                startActivity(g);
//                selectedPos = 7;
                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mDrawerLayout.closeDrawer(mDrawerLinear);

//                Intent f=new Intent(MainActivity.this,MembershipFragment.class);
//                startActivity(f);
////                selectedPos = 6;
//                if (language.equalsIgnoreCase("En")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }else if (language.equalsIgnoreCase("Ar")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }
//                mDrawerList.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 7:
                Intent h=new Intent(MainActivity.this,OffersFragment.class);
                startActivity(h);
//                selectedPos = 8;
                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mDrawerLayout.closeDrawer(mDrawerLinear);

//                Intent g=new Intent(MainActivity.this,NewsFragment.class);
//                startActivity(g);
////                selectedPos = 7;
//                if (language.equalsIgnoreCase("En")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }else if (language.equalsIgnoreCase("Ar")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }
//                mDrawerList.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;
            case 8:
                Intent j=new Intent(MainActivity.this,ContactUsFragment.class);
                startActivity(j);
//                    selectedPos = 10;
                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mDrawerLayout.closeDrawer(mDrawerLinear);

//                Intent h=new Intent(MainActivity.this,OffersFragment.class);
//                startActivity(h);
////                selectedPos = 8;
//                if (language.equalsIgnoreCase("En")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }else if (language.equalsIgnoreCase("Ar")) {
//                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                            mSidemenuTitles, unselected_icons, selectedPos,language);
//                }
//                mDrawerList.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 9:

                Intent chat =new Intent(MainActivity.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    chat.putExtra("title", "Chat");
                }else if (language.equalsIgnoreCase("Ar")){
                    chat.putExtra("title","تحدث معنا");
                }
                chat.putExtra("booking",true);
                chat.putExtra("url","https://tawk.to/chat/5ba8ce68b156da713cb3be4c/default");
                startActivity(chat);


                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos,language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mDrawerLayout.closeDrawer(mDrawerLinear);

                break;
            case 10:

                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos, language);
                } else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos, language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
//                    mDrawerLayout.closeDrawer(mDrawerLinear);

//                if (mSidemenuTitles[9].contains("Reward Points") || mSidemenuTitles[9].contains("نقاط المكافآت")) {
//                    Intent i=new Intent(MainActivity.this,LoyaltyFragment.class);
//                    startActivity(i);
////                    selectedPos = 10;
//                    if (language.equalsIgnoreCase("En")) {
//                        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                                mSidemenuTitles, unselected_icons, selectedPos,language);
//                    }else if (language.equalsIgnoreCase("Ar")) {
//                        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                                mSidemenuTitles, unselected_icons, selectedPos,language);
//                    }
//                    mDrawerList.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
//                    mDrawerLayout.closeDrawer(mDrawerLinear);
//                }
//                else {
//                    Intent j=new Intent(MainActivity.this,ContactUsFragment.class);
//                    startActivity(j);
////                    selectedPos = 10;
//                    if (language.equalsIgnoreCase("En")) {
//                        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                                mSidemenuTitles, unselected_icons, selectedPos,language);
//                    }else if (language.equalsIgnoreCase("Ar")) {
//                        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                                mSidemenuTitles, unselected_icons, selectedPos,language);
//                    }
//                    mDrawerList.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
//                    mDrawerLayout.closeDrawer(mDrawerLinear);
//                }
                break;

            case 11:


//                if (mSidemenuTitles[10].contains("Contact us") || mSidemenuTitles[10].contains("إتصل بنا")) {
//
//                    Intent k = new Intent(MainActivity.this, ContactUsFragment.class);
//                    startActivity(k);
////                selectedPos = 11;
//                    if (language.equalsIgnoreCase("En")) {
//                        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                                mSidemenuTitles, unselected_icons, selectedPos, language);
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                                mSidemenuTitles, unselected_icons, selectedPos, language);
//                    }
//                    mDrawerList.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
//                    mDrawerLayout.closeDrawer(mDrawerLinear);
//                }else {
//
//                    if (language.equalsIgnoreCase("En")) {
//                        languagePrefsEditor.putString("language", "Ar");
//                        languagePrefsEditor.commit();
//                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
//                        startActivity(intent);
//                        finish();
//                    }else {
//                        languagePrefsEditor.putString("language", "En");
//                        languagePrefsEditor.commit();
//                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
//                        startActivity(intent);
//                        finish();
//                    }
//                    if (language.equalsIgnoreCase("En")) {
//                        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                                mSidemenuTitles, unselected_icons, selectedPos, language);
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
//                                mSidemenuTitles, unselected_icons, selectedPos, language);
//                    }
//                    mDrawerList.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
////                    mDrawerLayout.closeDrawer(mDrawerLinear);
//                }
                break;

              case 12:

                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                if (language.equalsIgnoreCase("En")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                            mSidemenuTitles, unselected_icons, selectedPos, language);
                } else if (language.equalsIgnoreCase("Ar")) {
                    adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                            mSidemenuTitles, unselected_icons, selectedPos, language);
                }
                mDrawerList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
//                mDrawerLayout.closeDrawer(mDrawerLinear);
        break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOGIN_REQUEST && resultCode == RESULT_OK) {
//            if(selectedPos==10){
//                selectedPos = selectedPos + 1;
//            }

            Intent a=new Intent(MainActivity.this,ProfileFragment.class);
            startActivity(a);

//            if (language.equalsIgnoreCase("En")) {
                mSidemenuTitles = new String[]{"", "Booking", "Brands",  "Notification", "Photo Gallery",
                        "Membership", "News", "Offers", "Reward Points", "Contact us", "Chat", "Language"};

                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_booking,
                        R.drawable.menu_brands,  R.drawable.menu_notification,
                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
                        R.drawable.menu_membership, R.drawable.menu_contactus ,R.drawable.menu_chat, R.drawable.menu_language};

                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "", "","English"};

//            }else if (language.equalsIgnoreCase("Ar")) {
//
//                mSidemenuTitles = new String[]{"", "ملف شخصى", "حجز", "العلامات التجارية",  "اشعارات", "معرض الصور",
//                        "برنامج العضوية", "إخبار", "أخر العروض", "نقاط المكافآت", "إتصل بنا" ,"اللغة"};
//
//                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_profile, R.drawable.menu_booking,
//                        R.drawable.menu_brands,  R.drawable.menu_notification,
//                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
//                        R.drawable.menu_membership, R.drawable.menu_contactus ,R.drawable.menu_language};
//
//                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "","العربية"};
//            }

            if (language.equalsIgnoreCase("En")) {
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                        mSidemenuTitles, unselected_icons, selectedPos,language);
            }else if (language.equalsIgnoreCase("Ar")) {
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_arabic,
                        mSidemenuTitles, unselected_icons, selectedPos,language);
            }
            mDrawerList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {

        if (language.equalsIgnoreCase("En")) {

//            if (mLoginStatus.equals("")) {
                mSidemenuTitles = new String[]{"", "Booking", "Brands", "Notification", "Photo Gallery",
                        "Membership", "News", "Offers", "Contact us", "Chat", "Language"};

                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_booking,
                        R.drawable.menu_brands,  R.drawable.menu_notification,
                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
                        R.drawable.menu_contactus, R.drawable.menu_chat, R.drawable.menu_language};

                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "","","English"};

//            } else {
//                mSidemenuTitles = new String[]{"", "Profile", "Booking", "Brands", "Notification", "Photo Gallery",
//                        "Membership", "News", "Offers", "Reward Points", "Contact us", "Language"};
//
//
//                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_profile, R.drawable.menu_booking,
//                        R.drawable.menu_brands, R.drawable.menu_notification,
//                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
//                        R.drawable.menu_membership, R.drawable.menu_contactus ,R.drawable.menu_language};
//
//                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "","English"};
//            }
        }else if (language.equalsIgnoreCase("Ar")){

//            if (mLoginStatus.equals("")) {
                mSidemenuTitles = new String[]{"", "حجز", "العلامات التجارية",  "اشعارات", "معرض الصور",
                        "برنامج العضوية", "أخبار", "اخر العروض", "إتصل بنا", "تحدث معنا", "اللغة"};

                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_booking,
                        R.drawable.menu_brands,  R.drawable.menu_notification,
                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
                        R.drawable.menu_contactus , R.drawable.menu_chat, R.drawable.menu_language  };

                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "","","العربية"};
//            } else {
//                mSidemenuTitles = new String[]{"", "ملف شخصى ", "حجز", "العلامات التجارية",  "اشعارات", "معرض الصور",
//                        "برنامج العضوية", "إخبار", "أخر العروض", "نقاط المكافآت", "إتصل بنا" ,"اللغة"};
//
//
//                unselected_icons = new int[]{R.drawable.menu_logout, R.drawable.menu_profile, R.drawable.menu_booking,
//                        R.drawable.menu_brands,  R.drawable.menu_notification,
//                        R.drawable.menu_photography, R.drawable.menu_membership, R.drawable.menu_news, R.drawable.menu_offer,
//                        R.drawable.menu_membership, R.drawable.menu_contactus ,R.drawable.menu_language};
//
//                mlanguage =new String[]{"", "", "", "", "", "", "", "", "", "","العربية"};
//            }

        }

        super.onResume();
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
//        else if (!doubleBackToExitPressedOnce) {
//            this.doubleBackToExitPressedOnce = true;
//            Toast.makeText(this,"Press BACK again to exit.", Toast.LENGTH_SHORT).show();
//
//            new Handler().postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//                    doubleBackToExitPressedOnce = false;
//                }
//            }, 2000);
//        }
        else {

        }

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));

        if (language.equalsIgnoreCase("En")) {
            alertDialogBuilder.setMessage("Do you really want to exit ?");
        }
        else {
            alertDialogBuilder.setMessage(" هل ترغب حقاً بالخروج ؟ ");
        }
            alertDialogBuilder.setCancelable(false);
        if (language.equalsIgnoreCase("En")) {

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
//                    onBackPressed();
                    finish();

                }
            });
        }else {

            alertDialogBuilder.setPositiveButton("صحيح", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
//                    onBackPressed();
                    finish();

                }
            });
        }

            if (language.equalsIgnoreCase("En")) {

                alertDialogBuilder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });
            }else {

                alertDialogBuilder.setNegativeButton("الغاء", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });
            }

            AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
            alertDialog.show();
        }

    public class GetDeviceToken extends AsyncTask<String, Integer, String> {

        ProgressDialog pDialog;
        String networkStatus;
//        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
//            dialog = ProgressDialog.show(MainActivity.this, "",
//                    "Device Token...");
        }

        @Override
        protected String doInBackground(String... params) {

            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.Device_Token_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
//                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            Log.i("TAG", "result " + result);
                            JSONObject jo = new JSONObject(result);

                            String message = jo.getString("message");
//                            Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
    }