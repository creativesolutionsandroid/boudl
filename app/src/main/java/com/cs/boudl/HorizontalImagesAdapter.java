package com.cs.boudl;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class HorizontalImagesAdapter extends RecyclerView.Adapter<HorizontalImagesAdapter.MyViewHolder> {

    private ArrayList<String> horizontalList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView recyclerimageView;

        public MyViewHolder(View view) {
            super(view);
            recyclerimageView = (ImageView) view.findViewById(R.id.image);
        }
    }


    public HorizontalImagesAdapter(Context context, ArrayList<String> horizontalList) {
        this.horizontalList = horizontalList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_horizaontal_images, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Glide.with(context).load(Constants.IMAGE_URL+horizontalList.get(position)).into(holder.recyclerimageView);
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}
