package com.cs.boudl;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cs.boudl.Model.News;

import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class NewsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    String[] titles;
    String[] titlesAr;
    int[] images;
    Typeface boldTypeface;
    String langugae;
    ArrayList<News> newsArrayList = new ArrayList<>();

    public NewsAdapter(Context context, ArrayList<News> newsArrayList, String langugae) {
        this.context = context;
        this.newsArrayList = newsArrayList;
        this.langugae = langugae;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return newsArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvTitle;
        ImageView icon;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.list_news, null);

            holder.tvTitle = (TextView) convertView.findViewById(R.id.text);
            holder.icon = (ImageView) convertView.findViewById(R.id.image);

            if (langugae.equalsIgnoreCase("En")) {

                boldTypeface = Typeface.createFromAsset(context.getAssets(),
                        "OpenSans-Regular.ttf");
            }else {
                boldTypeface = Typeface.createFromAsset(context.getAssets(),
                        "Cairo-Regular.ttf");
            }

            ((TextView) convertView.findViewById(R.id.text)).setTypeface(boldTypeface);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Log.i("TAG","name "+newsArrayList.get(position).getNameEn());
        Log.i("TAG","nameAr "+newsArrayList.get(position).getNameAr());

        if(langugae.equalsIgnoreCase("En")) {
            holder.tvTitle.setText(newsArrayList.get(position).getNameEn());
        }
        else{
            holder.tvTitle.setText(newsArrayList.get(position).getNameAr());
        }
        Glide.with(context).load(Constants.IMAGE_URL + newsArrayList.get(position).getImagePath())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE).dontTransform().into(holder.icon);
//        holder.icon.setImageResource(images[position]);
        return convertView;
    }
}