package com.cs.boudl.Firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cs.boudl.R;
import com.cs.boudl.activity.NewsFragment;
import com.cs.boudl.activity.NotificationFragment;
import com.cs.boudl.activity.OffersFragment;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService implements com.cs.boudl.RemoteMessage {
    private static final String TAG = "TAG";
    String title, icon;
      Intent intent;
    Context mContext;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "onMessageReceived: got message");
            Intent intent = new Intent(this, NotificationFragment.class);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

            Notification noti = new NotificationCompat.Builder(MyFirebaseMessagingService.this, "1")
                    .setContentTitle(title)
                    .setContentText(remoteMessage.getNotification().getBody())
                    .setSmallIcon(R.drawable.ic_notification_icon)
                    .setColor(Color.parseColor("#000000"))
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(remoteMessage.getNotification().getBody()))
                    .setContentIntent(resultPendingIntent).build();

//            Notification noti = new Notification.Builder(MyFirebaseMessagingService.this)
//                    .setContentTitle(title)
//                    .setContentText(remoteMessage.getNotification().getBody())
//                    .setSmallIcon(R.drawable.ic_notification_icon)
//                    .setColor(Color.parseColor("#000000"))
//                    .setPriority(Notification.PRIORITY_HIGH)
//                    .setDefaults(NotificationCompat.DEFAULT_ALL)
//                    .setContentIntent(resultPendingIntent).build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // hide the notification after its selected
            noti.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify((int)System.currentTimeMillis(), noti);
        }

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Data Payload: " + remoteMessage.getData().toString());
            String message = "";
            int pnType = 1;
//            try {
//                JSONObject resultObj = new JSONObject(remoteMessage.getData().toString());
//                message = resultObj.getString("text");
//                pnType = resultObj.getInt("PnType");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

            Map<String, String> notificationMap = remoteMessage.getData();
            for (Map.Entry<String, String> e : notificationMap.entrySet()) {
                if(e.getKey().equals("text")){
                    message = e.getValue();
                }
                if(e.getKey().equals("PnType")){
                    pnType = Integer.parseInt(e.getValue());
                }
            }

            Intent intent = null;
            if(pnType == 1) {
                intent = new Intent(this, NotificationFragment.class);
            }
            else if(pnType == 2) {
                intent = new Intent(this, NewsFragment.class);
            }
            else if(pnType == 3) {
                intent = new Intent(this, OffersFragment.class);
            }

            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

            Notification noti = new Notification.Builder(MyFirebaseMessagingService.this)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_notification_icon)
                    .setColor(Color.parseColor("#000000"))
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setContentIntent(resultPendingIntent).build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // hide the notification after its selected
            noti.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify((int)System.currentTimeMillis(), noti);
        }

//    private void handleNotification(String message) {
//        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
//
//            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
//            pushNotification.putExtra("message", message);
//            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//
//
//            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//            notificationUtils.playNotificationSound();
//        }
//    }
//
//    private void handleDataMessage(JSONObject json) {
//        Log.e(TAG, "push json: " + json.toString());
//
//        try {
//            JSONObject data = json.getJSONObject("data");
//
//            String title = data.getString("title");
//            String message = data.getString("message");
//            boolean isBackground = data.getBoolean("is_background");
//            String imageUrl = data.getString("image");
//            String timestamp = data.getString("timestamp");
//            JSONObject payload = data.getJSONObject("payload");
//
//            Log.e(TAG, "title: " + title);
//            Log.e(TAG, "message: " + message);
//            Log.e(TAG, "isBackground: " + isBackground);
//            Log.e(TAG, "payload: " + payload.toString());
//            Log.e(TAG, "imageUrl: " + imageUrl);
//            Log.e(TAG, "timestamp: " + timestamp);
//
//
//            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
//
//                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
//                pushNotification.putExtra("message", message);
//                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//
//
//                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//                notificationUtils.playNotificationSound();
//            }else {
//
//                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
//                resultIntent.putExtra("message", message);
//
//                Intent pendingIntentYes=new Intent(getApplicationContext(), Receiver.class);
//                pendingIntentYes.putExtra("message",message);
//                if (TextUtils.isEmpty(imageUrl)) {
//                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
////                } else {
//
//                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
//                }
//            }
////        } catch (JSONException e) {
//            Log.e(TAG, "Json Exception: " + e.getMessage());
//        } catch (Exception e) {
//            Log.e(TAG, "Exception: " + e.getMessage());
//        }
//    }

//     private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
//        notificationUtils = new NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
//    }

//    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
//        notificationUtils = new NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
//    }
    }

    @Override
    public void remoteMessagevalue(RemoteMessage remoteMessages) {

        RemoteMessage remoteMessage = null;

        remoteMessages = remoteMessage;

        Log.i(TAG, "remoteMessagevalue: " + remoteMessages);

    }

}
