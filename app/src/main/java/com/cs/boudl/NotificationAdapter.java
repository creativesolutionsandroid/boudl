package com.cs.boudl;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.boudl.Model.Notification;
import com.cs.boudl.activity.WebViewActivity;

import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class NotificationAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    Typeface boldTypeface;
    String langugae;
    ArrayList<Notification> newsArrayList = new ArrayList<>();

    public NotificationAdapter(Context context, ArrayList<Notification> newsArrayList, String langugae) {
        this.context = context;
        this.newsArrayList = newsArrayList;
        this.langugae = langugae;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return newsArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvTitle;
        ImageView icon;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(langugae.equals("En")) {
                convertView = inflater.inflate(R.layout.list_notification, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_notification_ar, null);
            }

            holder.tvTitle = (TextView) convertView.findViewById(R.id.notificationMsg);

            if (langugae.equalsIgnoreCase("En")) {

                boldTypeface = Typeface.createFromAsset(context.getAssets(),
                        "OpenSans-Regular.ttf");
            }else {
                boldTypeface = Typeface.createFromAsset(context.getAssets(),
                        "Cairo-Regular.ttf");
            }

            ((TextView) convertView.findViewById(R.id.notificationMsg)).setTypeface(boldTypeface);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvTitle.setText(newsArrayList.get(position).getMessage());
        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newsArrayList.get(position).getRedirectURL().length()>0){
                    Intent fb=new Intent(context, WebViewActivity.class);
                    fb.putExtra("title","Boudl Group");
                    fb.putExtra("url",newsArrayList.get(position).getRedirectURL());
                    fb.putExtra("booking",false);
                    context.startActivity(fb);
                }
                else{

                }
            }
        });
        return convertView;
    }
}