package com.cs.boudl;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.cs.boudl.Model.Gallery;
import com.cs.boudl.Model.Hotalname;
import com.cs.boudl.activity.PhotoGalleryFragment;

import java.util.ArrayList;
import java.util.List;

import static com.cs.boudl.activity.PhotoGalleryFragment.galleryRecyclerAdapter;
import static com.cs.boudl.activity.PhotoGalleryFragment.selectedHotel1;

public class GalleryFilterAdapter extends RecyclerView.Adapter<GalleryFilterAdapter.ViewHolder> {


    ArrayList<Gallery> galleryArrayList = new ArrayList<>();
    ArrayList<String> city_name_en = new ArrayList<>();
    ArrayList<String> city_name_ar = new ArrayList<>();

    String TAG = "TAG";

    ArrayList<Hotalname> hotalnames = new ArrayList<>();
    ArrayList<String> city = new ArrayList<>();
//    public static String value = "null";


    private LayoutInflater mInflater;
    String language;
    public static ImageView itemImage;
    Activity parentActivity;
    public Context context;
    ViewPager viewpager;
    ViewPageAdapter viewPageAdapter;
    boolean dropdown = false;
    private static RecyclerViewClickListener itemListener;
    List<TextView> cardViewList = new ArrayList<>();
    int value;


    public GalleryFilterAdapter(Context context, RecyclerViewClickListener itemListener, int value, ArrayList<Hotalname> hotelnames, ArrayList<Gallery> galleryArrayList, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.hotalnames = hotelnames;
//        this.hotelname_Ar = hotelname_Ar;
        this.galleryArrayList = galleryArrayList;
        this.itemListener = itemListener;
        this.parentActivity = parentActivity;
        this.language = language;
        this.context = context;
        this.city = city;
        this.value = value;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.i("TAG", "curst size1 " + orderList.size());
        View view = null;
//        if (language.equalsIgnoreCase("En")) {
        view = mInflater.inflate(R.layout.gallery_footer, parent, false);
//        } else if (language.equalsIgnoreCase("Ar")) {
//            view = mInflater.inflate(R.layout.curst_list, parent, false);
//        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Log.i(TAG, "cityname: " + hotalnames.get(value).getCitynames().get(position).getCityname_en());

        if (language.equalsIgnoreCase("En")) {
            holder.title.setText(hotalnames.get(value).getCitynames().get(position).getCityname_en());

        } else {
            holder.layout.setRotationY(180);
            holder.title.setText(hotalnames.get(value).getCitynames().get(position).getCityname_ar());
        }

//        if (hotalnames.get(position).getHotalname_en().equalsIgnoreCase("Aber Hotel") || hotalnames.get(position).getHotalname_en().equalsIgnoreCase("Narcissus")) {
//
//            holder.spinner.setMinimumWidth(100);
//
//        } else {
//
//            holder.spinner.setMinimumWidth(70);
//
//        }
        cardViewList.add(holder.title);
        if (selectedHotel1.equalsIgnoreCase(hotalnames.get(value).getCitynames().get(position).getCityname_en())) {
            holder.title.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_selected_bg));
            holder.title.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.title.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_bg));
            holder.title.setTextColor(context.getResources().getColor(R.color.black));
        }

        if (language.equalsIgnoreCase("En")) {
//            if (position != 0) {
            final ArrayList<String> branchs = new ArrayList<>();
            for (int i = 0; i < hotalnames.get(value).getCitynames().get(position).getBrandnames().size(); i++) {
                branchs.add(hotalnames.get(value).getCitynames().get(position).getBrandnames().get(i).getBrandname_en());
            }


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.list_spinner_citys, branchs) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(0);
                    ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.LEFT);
                    ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.black));


                    return v;
                }
            };

            holder.spinner.setAdapter(adapter);
            holder.spinner.setSelection(0, false);
//            }

            holder.spinner.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectedHotel1 = hotalnames.get(value).getCitynames().get(position).getCityname_en();
                    PhotoGalleryFragment.img.clear();
                    PhotoGalleryFragment.mediaid.clear();
//                    if (selectedHotel1.equalsIgnoreCase(hotalnames.get(value).getCitynames().get(position).getCityname_en())) {
//                        for (int i = 1; i < hotalnames.get(value).getCitynames().size(); i++) {
//                            for (int j = 0; j < hotalnames.get(value).getCitynames().get(i).getBrandnames().size(); j++) {
//                                PhotoGalleryFragment.img.addAll(hotalnames.get(value).getCitynames().get(i).getBrandnames().get(j).getImagePath());
//                                PhotoGalleryFragment.mediaid.addAll(hotalnames.get(value).getCitynames().get(i).getBrandnames().get(j).getMediaTypeId());
//                            }
//                            break;
//                        }
//                    } else {

                    for (int h = 0; h < hotalnames.size(); h++) {
                        for (int i = 0; i < hotalnames.get(value).getCitynames().size(); i++) {
                            if (hotalnames.get(value).getCitynames().get(i).getCityname_en().equalsIgnoreCase(selectedHotel1)) {
                                PhotoGalleryFragment.img.addAll(hotalnames.get(value).getCitynames().get(i).getImagePath());
                                PhotoGalleryFragment.mediaid.addAll(hotalnames.get(value).getCitynames().get(i).getMediaTypeId());
                                Log.i(TAG, "img: " + PhotoGalleryFragment.img.size());

                            }
                        }
                        break;
                    }
                    Log.i(TAG, "selectedhotel: " + selectedHotel1);
                    Log.i(TAG, "img: " + PhotoGalleryFragment.img.size());
                    galleryRecyclerAdapter = new GalleryRecyclerAdapter(context, PhotoGalleryFragment.mediaid, PhotoGalleryFragment.img, language);
                    PhotoGalleryFragment.Gallery_view.setAdapter(galleryRecyclerAdapter);

                    for (TextView cardView : cardViewList) {
                        cardView.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_bg));
                        cardView.setTextColor(context.getResources().getColor(R.color.black));
                    }
                    holder.title.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_selected_bg));
                    holder.title.setTextColor(context.getResources().getColor(R.color.white));
                    holder.spinner.performClick();
//                        notifyDataSetChanged();
                    return true;
                }
            });
            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                    PhotoGalleryFragment.img.clear();
                    PhotoGalleryFragment.mediaid.clear();

                    PhotoGalleryFragment.img = hotalnames.get(value).getCitynames().get(position).getBrandnames().get(pos).getImagePath();
                    PhotoGalleryFragment.mediaid = hotalnames.get(value).getCitynames().get(position).getBrandnames().get(pos).getMediaTypeId();

                    Log.d("whw", "spinner city selected: " + PhotoGalleryFragment.img.size());
                    galleryRecyclerAdapter = new GalleryRecyclerAdapter(context, PhotoGalleryFragment.mediaid, PhotoGalleryFragment.img, language);
                    PhotoGalleryFragment.Gallery_view.setAdapter(galleryRecyclerAdapter);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        } else if (language.equalsIgnoreCase("Ar")) {

//            if (position != 0) {
                final ArrayList<String> branchs_ar = new ArrayList<>();
                for (int i = 0; i < hotalnames.get(value).getCitynames().get(position).getBrandnames().size(); i++) {
                    branchs_ar.add(hotalnames.get(value).getCitynames().get(position).getBrandnames().get(i).getBrandname_ar());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.list_spinner_citys, branchs_ar) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View v = super.getView(position, convertView, parent);

                        ((TextView) v).setTextSize(0);
                        ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.white));

                        return v;
                    }

                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
                        View v = super.getDropDownView(position, convertView, parent);
                        v.setBackgroundResource(R.color.white);
                        ((TextView) v).setTextSize(15);
                        ((TextView) v).setGravity(Gravity.LEFT);
                        ((TextView) v).setTextColor(context.getResources().getColorStateList(R.color.black));

                        return v;
                    }
                };

                holder.spinner.setAdapter(adapter);
                holder.spinner.setSelection(0, true);
//            }

            holder.spinner.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectedHotel1 = hotalnames.get(value).getCitynames().get(position).getCityname_en();
                    PhotoGalleryFragment.img.clear();
                    PhotoGalleryFragment.mediaid.clear();
//                    if (selectedHotel1.equalsIgnoreCase(hotalnames.get(value).getCitynames().get(position).getCityname_en())) {
//                        for (int i = 1; i < hotalnames.get(value).getCitynames().size(); i++) {
//                            for (int j = 0; j < hotalnames.get(value).getCitynames().get(i).getBrandnames().size(); j++) {
//                                PhotoGalleryFragment.img.addAll(hotalnames.get(value).getCitynames().get(i).getBrandnames().get(j).getImagePath());
//                                PhotoGalleryFragment.mediaid.addAll(hotalnames.get(value).getCitynames().get(i).getBrandnames().get(j).getMediaTypeId());
//                            }
//                            break;
//                        }
//                    } else {

                    for (int h = 0; h < hotalnames.size(); h++) {
                        for (int i = 0; i < hotalnames.get(value).getCitynames().size(); i++) {
                            if (hotalnames.get(value).getCitynames().get(i).getCityname_en().equalsIgnoreCase(selectedHotel1)) {
                                PhotoGalleryFragment.img.addAll(hotalnames.get(value).getCitynames().get(i).getImagePath());
                                PhotoGalleryFragment.mediaid.addAll(hotalnames.get(value).getCitynames().get(i).getMediaTypeId());
                                Log.i(TAG, "img: " + PhotoGalleryFragment.img.size());

                            }
                        }
                        break;
                    }
                    Log.i(TAG, "selectedhotel: " + selectedHotel1);
                    Log.i(TAG, "img: " + PhotoGalleryFragment.img.size());
                    galleryRecyclerAdapter = new GalleryRecyclerAdapter(context, PhotoGalleryFragment.mediaid, PhotoGalleryFragment.img, language);
                    PhotoGalleryFragment.Gallery_view.setAdapter(galleryRecyclerAdapter);

                    for (TextView cardView : cardViewList) {
                        cardView.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_bg));
                        cardView.setTextColor(context.getResources().getColor(R.color.black));
                    }
                    holder.title.setBackground(context.getResources().getDrawable(R.drawable.gallery_footer_selected_bg));
                    holder.title.setTextColor(context.getResources().getColor(R.color.white));
                    holder.spinner.performClick();
//                        notifyDataSetChanged();
                    return true;
                }
            });
            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                    PhotoGalleryFragment.img.clear();
                    PhotoGalleryFragment.mediaid.clear();

                    PhotoGalleryFragment.img = hotalnames.get(value).getCitynames().get(position).getBrandnames().get(pos).getImagePath();
                    PhotoGalleryFragment.mediaid = hotalnames.get(value).getCitynames().get(position).getBrandnames().get(pos).getMediaTypeId();

                    Log.d("whw", "spinner city selected: " + PhotoGalleryFragment.img.size());
                    galleryRecyclerAdapter = new GalleryRecyclerAdapter(context, PhotoGalleryFragment.mediaid, PhotoGalleryFragment.img, language);
                    PhotoGalleryFragment.Gallery_view.setAdapter(galleryRecyclerAdapter);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }

    }


    @Override
    public int getItemCount() {
        return hotalnames.get(value).getCitynames().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout layout;
        TextView title;
        Spinner spinner;

//        ImageView curstImage;

        public ViewHolder(View itemView) {
            super(itemView);
//            Log.i("TAG", "curst size4 " + orderList.size());

            title = itemView.findViewById(R.id.gallery_footer_txt);
            itemImage = itemView.findViewById(R.id.image1);
            viewpager = itemView.findViewById(R.id.pager);
            layout = itemView.findViewById(R.id.layout);
            spinner = itemView.findViewById(R.id.city_spinner);
//            curstImage = itemView.findViewById(R.id.curst_img_layout);

//            layout.setOnClickListener(this);
        }

//        @Override
//        public void onClick(View v) {
//
//
//
//            int pos = getAdapterPosition();
//
//            for (int i = 0; i < galleryArrayList.size(); i++) {
//                if (galleryArrayList.get(i).getHotalnameEn().contains(hotelname_En.get(pos))) {
//
//                    if (!city_name_en.contains(galleryArrayList.get(i).getCityNameEn())) {
//                        city_name_en.add(galleryArrayList.get(i).getCityNameEn());
//                    }
//
//                }
//            }
//
//            Log.d("TAG", "adapterpos: " + pos);
//
//            if (hotelname_En.get(pos).contains(hotelname_En.get(pos))) {
//
//                spinner.setBackgroundResource(R.drawable.gallery_footer_selected_bg);
//
//            }
//
//
//        }
    }
}

