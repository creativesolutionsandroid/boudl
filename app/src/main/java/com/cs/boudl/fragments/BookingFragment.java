package com.cs.boudl.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.boudl.Constants;
import com.cs.boudl.MainActivity;
import com.cs.boudl.Model.Branch;
import com.cs.boudl.Model.BranchList;
import com.cs.boudl.Model.Brand;
import com.cs.boudl.Model.City;
import com.cs.boudl.Model.CityList;
import com.cs.boudl.Model.HotelList;
import com.cs.boudl.NetworkUtil;
import com.cs.boudl.R;
import com.cs.boudl.activity.SearchActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by CS on 20-01-2017.
 */
public class BookingFragment extends Fragment {

    TextView searchHotel;
    ImageView menu;
    ImageView narcissus, aber, boudl, braira;
    View rootView;
    String  language;
    SharedPreferences.Editor languagePrefsEditor ;
    SharedPreferences languagePrefs;
    Typeface semiBoldTypeface;
    TextView dlanguage;
    int selectedPOs = 0;
//    RecyclerView mRecyclerView;
//    HorizontalImagesAdapter mAdapter;

    ArrayList<CityList> allCitiesList = new ArrayList<>();
    ArrayList<HotelList> allHotelsList = new ArrayList<>();
    ArrayList<BranchList> allBranchesList = new ArrayList<>();

    ArrayList<Brand> brandArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.fragment_booking, container, false);
        }else {
            rootView = inflater.inflate(R.layout.fragment_booking_arabic, container, false);
        }

        if (language.equalsIgnoreCase("En")) {
            semiBoldTypeface = Typeface.createFromAsset(getActivity().getAssets(),
                    "OpenSans-Semibold.ttf");
        }else {
            semiBoldTypeface = Typeface.createFromAsset(getActivity().getAssets(),
                    "Cairo-Semibold.ttf");
        }


        ((TextView) rootView.findViewById(R.id.searchHotel)).setTypeface(semiBoldTypeface);

        searchHotel = (TextView) rootView.findViewById(R.id.searchHotel);
        menu = (ImageView) rootView.findViewById(R.id.menu);
        narcissus = (ImageView) rootView.findViewById(R.id.narcissus);
        aber = (ImageView) rootView.findViewById(R.id.aber);
        boudl = (ImageView) rootView.findViewById(R.id.boudl);
        braira = (ImageView) rootView.findViewById(R.id.braira);
        dlanguage = rootView.findViewById(R.id.language);

//        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.horizontal_recycler_view);

//        new GetBooking().execute();

        dlanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }

            }
        });

        if (language.equalsIgnoreCase("En")) {
            searchHotel.setText("Book now");
        }else if (language.equalsIgnoreCase("Ar")) {
            searchHotel.setText("احجز الان");
        }

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        searchHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SearchActivity.class));
            }
        });

        narcissus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPOs = 3;
                if(brandArrayList.size()>0){
                    checkBrands();
                }
                else {
                    new GetBooking().execute();
                }
            }
        });

        aber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPOs = 2;
                if(brandArrayList.size()>0){
                    checkBrands();
                }
                else {
                    new GetBooking().execute();
                }
            }
        });

        boudl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPOs = 0;
                if(brandArrayList.size()>0){
                    checkBrands();
                }
                else {
                    new GetBooking().execute();
                }
            }
        });

        braira.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPOs = 1;
                if(brandArrayList.size()>0){
                    checkBrands();
                }
                else {
                    new GetBooking().execute();
                }
            }
        });
        return rootView;
    }

    public class GetBooking extends AsyncTask<String,Integer,String > {

        String networkStatus;
        ProgressDialog dialog;
        String response;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Please wait...");
            dialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_LIGHT);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Please wait");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.Booking_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity("", "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            brandArrayList.clear();
                            JSONArray jsonArray = new JSONArray(result);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            JSONArray cityArray = jsonObject.getJSONArray("cityList");
                            for (int i = 0; i < cityArray.length(); i++) {
                                JSONObject cityObj = cityArray.getJSONObject(i);
                                CityList cityList = new CityList();
                                cityList.setId(cityObj.getString("Id"));
                                cityList.setCountryId(cityObj.getString("CountryId"));
                                cityList.setName_en(cityObj.getString("NameEn"));
                                cityList.setName_ar(cityObj.getString("NameAr"));
                                allCitiesList.add(cityList);
                            }

                            JSONArray hotelsArray = jsonObject.getJSONArray("HotelList");
                            for (int i = 0; i < hotelsArray.length(); i++) {
                                JSONObject cityObj = hotelsArray.getJSONObject(i);
                                HotelList cityList = new HotelList();
                                cityList.setId(cityObj.getString("Id"));
                                cityList.setName_en(cityObj.getString("NameEn"));
                                cityList.setName_ar(cityObj.getString("NameAr"));
                                cityList.setLogo_en(cityObj.getString("LogoEn"));
                                cityList.setLogo_ar(cityObj.getString("LogoAr"));
                                allHotelsList.add(cityList);
                            }

                            JSONArray branchArray = jsonObject.getJSONArray("BranchList");
                            for (int i = 0; i < branchArray.length(); i++) {
                                JSONObject cityObj = branchArray.getJSONObject(i);
                                BranchList cityList = new BranchList();
                                cityList.setId(cityObj.getString("Id"));
                                cityList.setHotelid(cityObj.getString("HotelId"));
                                cityList.setCityid(cityObj.getString("cityId"));
                                cityList.setBookingurl(cityObj.getString("BookingUrl"));
                                cityList.setName_en(cityObj.getString("NameEn"));
                                cityList.setName_ar(cityObj.getString("NameAr"));
                                allBranchesList.add(cityList);
                            }

                            for (int i = 0; i < allHotelsList.size(); i++){
                                Brand brand = new Brand();
                                brand.setId(allHotelsList.get(i).getId());
                                brand.setBrandName(allHotelsList.get(i).getName_en());
                                brand.setBrandNameAr(allHotelsList.get(i).getName_ar());
                                brand.setLogoEn(allHotelsList.get(i).getLogo_en());
                                brand.setLogoAr(allHotelsList.get(i).getLogo_ar());

                                ArrayList<String> dummyCityList = new ArrayList<>();
                                ArrayList<City> cityList = new ArrayList<>();
                                for (int j = 0; j < allBranchesList.size(); j++){
                                    if(!dummyCityList.contains(allBranchesList.get(j).getCityid())){
                                        if(allHotelsList.get(i).getId().equals(allBranchesList.get(j).getHotelid())) {
                                            dummyCityList.add(allBranchesList.get(j).getCityid());
                                            City city = new City();
                                            for (CityList cityList1 : allCitiesList) {
                                                if (allBranchesList.get(j).getCityid().equals(cityList1.getId())) {
                                                    city.setCityName(cityList1.getName_en());
                                                    city.setCityNameAr(cityList1.getName_ar());

                                                    ArrayList<Branch> branchArrayList = new ArrayList<>();
                                                    for (int k = 0; k < allBranchesList.size(); k++) {
                                                        if(allHotelsList.get(i).getId().equals(allBranchesList.get(k).getHotelid())) {
                                                            if (allBranchesList.get(k).getCityid().equals(cityList1.getId())) {
                                                                Branch branch = new Branch();
                                                                branch.setBranchName(allBranchesList.get(k).getName_en());
                                                                branch.setBranchNameAr(allBranchesList.get(k).getName_ar());
                                                                branch.setLink(allBranchesList.get(k).getBookingurl());
                                                                branchArrayList.add(branch);
                                                            }
                                                        }
                                                    }
                                                    city.setBranchArrayList(branchArrayList);
                                                    break;
                                                }
                                            }
                                            cityList.add(city);
                                        }
                                    }
                                }
                                brand.setCityArrayList(cityList);
                                brandArrayList.add(brand);
                            }

                            checkBrands();
//                            loadImages();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void checkBrands(){
        Log.i("TAG","selectedPOs "+selectedPOs);
        Log.i("TAG","brandArrayList "+brandArrayList.size());
        if(selectedPOs >= brandArrayList.size()){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));
//            if(language.equalsIgnoreCase("En")) {
                // set title
                alertDialogBuilder.setTitle("Boudl");

                // set dialog message
                alertDialogBuilder
                        .setMessage("No data found.")
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.dismiss();
                            }
                        });
//            }else if(language.equalsIgnoreCase("Ar")){
//                // set title
//                alertDialogBuilder.setTitle("اوريجانو");
//
//                // set dialog message
//                alertDialogBuilder
//                        .setMessage("من فضلك إختر منتج على الأقل لإستخدام تلك العملية")
//                        .setCancelable(false)
//                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                        });
//            }
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
        else{
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            intent.putExtra("pos",selectedPOs);
            startActivity(intent);
        }
    }
//    public void loadImages(){
//        ArrayList<String> imagesArray = new ArrayList<>();
//        for (Brand brand : brandArrayList){
//            if(language.equalsIgnoreCase("En")) {
//                imagesArray.add(brand.getLogoEn());
//            }
//            else{
//                imagesArray.add(brand.getLogoAr());
//            }
//        }
//        mAdapter = new HorizontalImagesAdapter(getContext(),imagesArray);
//
//        if(language.equalsIgnoreCase("En")) {
//            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
//        }
//        else{
//            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
//        }
//        mRecyclerView.setAdapter(mAdapter);
//    }
}
