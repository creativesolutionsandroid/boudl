package com.cs.boudl.activity;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.boudl.Constants;
import com.cs.boudl.MainActivity;
import com.cs.boudl.Model.Offers;
import com.cs.boudl.NetworkUtil;
import com.cs.boudl.OffersAdapter;
import com.cs.boudl.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by CS on 11-12-2017.
 */

public class OffersFragment extends AppCompatActivity {

    ImageView menu;
    ListView listView;
    OffersAdapter newsAdapter;
    Typeface lightTypeface, semiBoldTypeface, boldTypeface;
//    int[] images = {R.drawable.boudloffer1, R.drawable.boudloffer2, R.drawable.boudloffer3, R.drawable.boudloffer4, R.drawable.boudloffer5,
//            R.drawable.boudloffer6, R.drawable.boudloffer7, R.drawable.boudloffer8, R.drawable.boudloffer9, R.drawable.boudloffer10, R.drawable.boudloffer11
//            , R.drawable.brairaoffer1, R.drawable.brairaoffer2, R.drawable.brairaoffer3, R.drawable.brairaoffer4, R.drawable.brairaoffer5};
    View rootView;

    String response;

    ArrayList<Offers> offersArray = new ArrayList<>();

    String language;
    SharedPreferences languagePrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.fragement_news);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.fragement_news_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        } else {
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);
        if (language.equalsIgnoreCase("En")) {
            ((TextView) findViewById(R.id.title)).setText("Offers");
        } else if (language.equalsIgnoreCase("Ar")) {
            ((TextView) findViewById(R.id.title)).setText("اخر العروض");
        }

        menu = (ImageView) findViewById(R.id.menu);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent AboutIntent = new Intent(OffersFragment.this, WebViewActivity.class);
//                if(language.equalsIgnoreCase("En")) {
//                    AboutIntent.putExtra("title", offersArray.get(position).getNameEn());
//                }
//                else{
//                    AboutIntent.putExtra("title", offersArray.get(position).getNameAr());
//                }
//                AboutIntent.putExtra("url", offersArray.get(position).getUrl());
//                startActivity(AboutIntent);

                Intent intent = new Intent(OffersFragment.this, OffersDescriptionActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    intent.putExtra("title", offersArray.get(position).getNameEn());
                    intent.putExtra("image", offersArray.get(position).getImageEn());
                    intent.putExtra("desc", offersArray.get(position).getDescriptionEn());
                    intent.putExtra("hotalnameEn", offersArray.get(position).getHotelNameEn());
                } else if (language.equalsIgnoreCase("Ar")) {
                    intent.putExtra("title1", offersArray.get(position).getNameAr());
                    intent.putExtra("image", offersArray.get(position).getImageAr());
                    intent.putExtra("desc1", offersArray.get(position).getDescriptionAr());
                    intent.putExtra("hotalnameAr", offersArray.get(position).getHotelNameAr());
                }
                intent.putExtra("link", offersArray.get(position).getOfferBookingUrl());
                intent.putExtra("mode", offersArray.get(position).getMode());
                intent.putExtra("price", offersArray.get(position).getOfferPrice());
                intent.putExtra("amenities_name_En", offersArray.get(position).getAmenitiesListEn());
                intent.putExtra("amenities_name_Ar",offersArray.get(position).getAmenitiesListAr());
                intent.putExtra("anenities_icon",offersArray.get(position).getAmenitiesUniCode());
                intent.putExtra("offers_array",offersArray);
                startActivity(intent);
            }
        });

            new GetOffers().execute();
    }

    public class GetOffers extends AsyncTask<String, Integer, String> {

        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            offersArray.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(OffersFragment.this);
//            dialog = ProgressDialog.show(OffersFragment.this, "",
//                    "Please wait..");
            dialog = new ProgressDialog(OffersFragment.this, ProgressDialog.THEME_HOLO_LIGHT);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Please wait");
            dialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.Offers_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
//                        StringEntity se = new StringEntity(params[0], "UTF-8");
//
//                        // 6. set httpPost Entity
//                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OffersFragment.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(OffersFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            Log.i("TAG", "user response:" + result);
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {
                                Offers offers = new Offers();

                                JSONObject jo = ja.getJSONObject(i);

                                offers.setId(jo.getString("Id"));
                                offers.setHotelId(jo.getString("HotelId"));
                                offers.setHotelNameAr(jo.getString("HotelNameAr"));
                                offers.setHotelNameEn(jo.getString("HotelNameEn"));
                                offers.setMode(jo.getString("Mode"));
                                offers.setOfferBookingUrl(jo.getString("OfferBookingUrl"));
                                offers.setNameEn(jo.getString("NameEn"));
                                offers.setNameAr(jo.getString("NameAr"));
                                offers.setDescriptionEn(jo.getString("DescriptionEn"));
                                offers.setDescriptionAr(jo.getString("DescriptionAr"));
                                offers.setImageEn(jo.getString("ImageEn"));
                                offers.setImageAr(jo.getString("ImageAr"));
                                offers.setOfferPrice(jo.getString("OfferPrice"));
                                offers.setStartDate(jo.getString("StartDate"));
                                offers.setEndDate(jo.getString("EndDate"));
                                offers.setBookingRedirection(jo.getString("BookingRedirection"));
                                offers.setSortIndex(jo.getString("SortIndex"));
                                offers.setIsActive(jo.getString("IsActive"));
                                offers.setAmenitiesListEn(jo.getString("AmenitiesListEn"));
                                offers.setAmenitiesListAr(jo.getString("AmenitiesListAr"));
                                offers.setAmenityIds(jo.getString("AmenityIds"));
                                offers.setAmenitiesUniCode(jo.getString("AmenitiesUniCode"));

                                offersArray.add(offers);
                            }

                            newsAdapter = new OffersAdapter(OffersFragment.this, offersArray, language);
                            listView.setAdapter(newsAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(OffersFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public void onBackPressed() {
        ActivityManager mngr = (ActivityManager) getSystemService( ACTIVITY_SERVICE );

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if(taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            startActivity(new Intent(OffersFragment.this, MainActivity.class));
            finish();
        }
        else{
            super.onBackPressed();
        }
    }
}
