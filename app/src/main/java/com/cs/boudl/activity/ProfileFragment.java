package com.cs.boudl.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.boudl.R;
import com.gdacciaro.iOSDialog.iOSDialog;

/**
 * Created by CS on 20-01-2017.
 */
public class ProfileFragment extends AppCompatActivity{

    ImageView menu;
    RelativeLayout update;
    View rootView;
    String  language;
    SharedPreferences languagePrefs;
    TextView plogout;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String mLoginStatus;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        mLoginStatus = userPrefs.getString("login_status", "");

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.fragement_profile);
        }else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.fragement_profile_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");

            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }else {
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");

            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);
        ((TextView) findViewById(R.id.signupBtn)).setTypeface(semiBoldTypeface);

        plogout=findViewById(R.id.logout);

        menu = (ImageView) findViewById(R.id.menu);

        plogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPrefEditor.clear();
                userPrefEditor.commit();
                plogout.setText("Login");
                onBackPressed();
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        update = (RelativeLayout) findViewById(R.id.updatebtn);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final iOSDialog iOSDialog = new iOSDialog(ProfileFragment.this);
                if (language.equalsIgnoreCase("En")) {
                    iOSDialog.setTitle(getResources().getString(R.string.app_name));
                }else {
                    iOSDialog.setTitle("بودل");
                }
                if (language.equalsIgnoreCase("En")) {
                    iOSDialog.setSubtitle("Profile is successfully updated");
                }else if (language.equalsIgnoreCase("Ar")){
                    iOSDialog.setSubtitle("تم تحديث الملف الشخصى بنجاح");
                }
                if (language.equalsIgnoreCase("En")){
                    iOSDialog.setPositiveLabel("ok");
                }else {
                    iOSDialog.setPositiveLabel("صحيح");
                }
                iOSDialog.setBoldPositiveLabel(false);
                iOSDialog.setPositiveListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        iOSDialog.dismiss();
                    }
                });
                iOSDialog.show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
