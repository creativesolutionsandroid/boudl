package com.cs.boudl.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.boudl.Constants;
import com.cs.boudl.Model.Branch;
import com.cs.boudl.Model.BranchList;
import com.cs.boudl.Model.Brand;
import com.cs.boudl.Model.City;
import com.cs.boudl.Model.CityList;
import com.cs.boudl.Model.HotelList;
import com.cs.boudl.NetworkUtil;
import com.cs.boudl.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by CS on 12-12-2017.
 */

public class SearchActivity extends AppCompatActivity {

    TextView numNights;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface,regularTypeface;
    TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek;
    TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
    ImageView back_btn, logo;
    TextView search;
    int hotelPos = 0, brandPos = 0, cityPos = 0, selectedPos = -1;
    String link = "", hotelName, imageName = "";
    EditText hotelSelect;
    TextView etRoom, etAdult, etChild;
    EditText etBrand, etCity;
    TextView nightsText, roomText, adlutText, childText;
    RelativeLayout checkInLayout, checkOutLayout;
    private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    Date mCheckInDate = null, mCheckOutDate = null;
    String mCheckInDateStr = null, mCheckOutDateStr = null;
    Spinner roomsSpinner, adultSpinner, childSpinner, hotelSpinner, brandSpinner, citySpinner;
    ArrayAdapter<String> roomsAdapter;
    ArrayAdapter<String> hotelAdapter;
    ArrayAdapter<String> adultsAdapter, adultsAdapter1, adultsAdapter2, adultsAdapter3;
    ArrayAdapter<String> childrenAdapter, childrenAdapter1, childrenAdapter2, childrenAdapter3;
    ArrayAdapter<String> brandAdapter;
    ArrayAdapter<String> brandAdapterAr;
    ArrayAdapter<String> cityAdapter1, cityAdapter2, cityAdapter3, cityAdapter4;
    ArrayAdapter<String> cityAdapter1Ar, cityAdapter2Ar, cityAdapter3Ar, cityAdapter4Ar;
    ArrayAdapter<String> hotelNarcissusAdapter, hotelAberAdapter, hotelBrairaAdapter1, hotelBrairaAdapter2, hotelBrairaAdapter3;
    ArrayAdapter<String> hotelNarcissusAdapterAr, hotelAberAdapterAr, hotelBrairaAdapter1Ar, hotelBrairaAdapter2Ar, hotelBrairaAdapter3Ar;
    ArrayAdapter<String> hotelBoudlAdapter1Ar, hotelBoudlAdapter2Ar, hotelBoudlAdapter3Ar, hotelBoudlAdapter4Ar, hotelBoudlAdapter5Ar,
            hotelBoudlAdapter6Ar, hotelBoudlAdapter7Ar, hotelBoudlAdapter8Ar, hotelBoudlAdapter9Ar, hotelBoudlAdapter10Ar;
    ArrayAdapter<String> hotelBoudlAdapter1, hotelBoudlAdapter2, hotelBoudlAdapter3, hotelBoudlAdapter4, hotelBoudlAdapter5,
            hotelBoudlAdapter6, hotelBoudlAdapter7, hotelBoudlAdapter8, hotelBoudlAdapter9, hotelBoudlAdapter10;
    String[] hotels = {"Aber 101", "Aber 102"};
    String[] rooms = {"01", "02", "03", "04", "05"};
    String[] adultCount1 = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
    String[] adultCount2 = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
    String[] adultCount3 = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
    String[] adultCount4 = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
    String[] childCount1 = {"00", "01", "02", "03", "04", "05"};
    String[] childCount2 = {"00", "01", "02", "03", "04", "05"};
    String[] childCount3 = {"00", "01", "02", "03", "04", "05"};
    String[] childCount4 = {"00", "01", "02", "03", "04", "05"};
    String[] brands = {"Boudl", "Narcissus", "Braira", "Aber"};
    String[] brands_ar = {" بودل للشقق الفندقية", "نارسس", "بريرا", "عابر"};
    String[] city1 = {"Riyadh"};
    String[] city1_ar = {"الرياض"};
    String[] city2 = {"Riyadh"};
    String[] city2_ar = {"الرياض"};
    String[] city3 = {"Riyadh", "Dammam", "Khobar", "Jeddah", "Al Taif", "Qassim", "Hail", "Abha", "Hafr AL batin", "Kuwait City"};
    String[] city3_ar = {"الرياض", "الدمام", "الخبر", "جدة", "الطائف", "القصيم", "حائل", "أبها", "حفر الباطن","الكويت"};
    String[] city4 = {"Riyadh", "Al Khobar", "Al Dammam"};
    String[] city4_ar = {"الرياض", "الخبر","الدمام"};
    String[] hotelBoudlRiyadh = {"Al Worod", "Al Masif", "Al Morouf", "Al Matar", "Al Malaz", "Gaber", "Al Fayhaa", "Al Qasr", "Al Majmaa", "Khurais"};
    String[] hotelBoudlRiyadh_ar = {"الورود", "المصيف", "المروج", "المطار", "الملز", "جابر", "الفيحاء", "القصر", "المجمعة", "خريص"};
    String[] hotelBoudlDamma = {"Al Shahtea", "Al Corniche"};
    String[] hotelBoudlDamma_ar = {"الشاطئ", "الكورنيش"};
    String[] hotelBoudlKhobar = {"Gardenia", "Al Khobar"};
    String[] hotelBoudlKhobar_ar = {"جاردينيا", "الخبر"};
    String[] hotelBoudlJeddah = {"Heera", "Al Tahlia", "Palestine"};
    String[] hotelBoudlJeddah_ar = {"حراء", "التحلية", "فلسطين"};
    String[] hotelBoudlAlTaif = {"Al Taif"};
    String[] hotelBoudlAlTaif_ar = {"الطائف"};
    String[] hotelBoudlQassim = {"Buraidah", "Al Nakhil", "Al Fakhria", "Al Rass"};
    String[] hotelBoudlQassim_ar = {"بريدة", "النخيل", "الفاخرية", "الرس"};
    String[] hotelBoudlHail = {"Al Bondoqia"};
    String[] hotelBoudlHail_ar = {"البندقية"};
    String[] hotelBoudlAbha = {"Abha", "Mahayel"};
    String[] hotelBoudlAbha_ar = {"أبها", "محايل"};
    String[] hotelBoudlHafr = {"AL Maydan", " City Center"};
    String[] hotelBoudlHafr_ar = {"الميدان", "سيتى سنتر"};
    String[] hotelBoudlKuwait = {"Al Salemya", "Al Fahahil"};
    String[] hotelBoudlKuwait_ar = {"السالمية", "بودل فحيحيل"};
    String[] hotelBrairaRiyadh = {"Olaya", "Al Wizarat", "Qortobah", "Hitin"};
    String[] hotelBrairaRiyadh_ar = {"العليا", "الوزارات", "قرطبة", "حطين"};
    String[] hotelBrairaKhobar = {"Al Azizya"};
    String[] hotelBrairaKhobar_ar = {"العزيزيه"};
    String[] hotelBrairaDammam = {"Al Dammam"};
    String[] hotelBrairaDammam_ar = {"الدمام"};
    String[] hotelsNarcissus = {"Narcissus Riyadh"};
    String[] hotelsNarcissus_ar = {"نارسس الرياض"};
    String[] hotelsAber = {"Aber 101", "Aber 102"};
    String[] hotelsAber_ar = {"عابر 101", "عابر 102"};
    String[] linksNarcissus = {"https://reservations.travelclick.com/98314"};
    String[] linksAber = {"http://aber101.boudl.com/V8Client/Inquiry.aspx", "http://aber102.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlRiyadh = {"http://worood.boudl.com/V8Client/Inquiry.aspx", "http://maseef.boudl.com/V8Client/Inquiry.aspx", "http://morooj.boudl.com/V8Client/Inquiry.aspx",
            "http://matar.boudl.com/V8Client/Inquiry.aspx", "http://malaz.boudl.com/V8Client/Inquiry.aspx", "http://jaber.boudl.com/V8Client/Inquiry.aspx", "http://fayhaa.boudl.com/V8Client/Inquiry.aspx", "http://qasr.boudl.com/V8Client/Inquiry.aspx", "http://majmaa.boudl.com/V8Client/Inquiry.aspx", "http://khorais.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlDamma = {"http://shatea.boudl.com/V8Client/Inquiry.aspx", "http://cornich.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlKhobar = {"http://gardenia.boudl.com/V8Client/Inquiry.aspx", "http://khobar.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlJeddah = {"http://hera.boudl.com/V8Client/Inquiry.aspx","http://tahliya.boudl.com/V8Client/Inquiry.aspx","http://palestine.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlAlTaif = {"http://taif.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlQassim = {"http://buraida.boudl.com/V8Client/Inquiry.aspx","http://nakheel.boudl.com/V8Client/Inquiry.aspx","http://fakhriya.boudl.com/V8Client/Inquiry.aspx","http://rass.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlHail = {"http://bondoqia.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlAbha = {"http://abha.boudl.com/V8Client/Inquiry.aspx","http://asir.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlHafr = {"http://maydan.boudl.com/V8Client/Inquiry.aspx","http://citycenter.boudl.com/V8Client/Inquiry.aspx"};
    String[] linksBoudlKuwait = {"http://salmiya.boudl.com/V8Client/Inquiry.aspx","http://fehehel.boudl.com/V8Client/Inquiry.aspx"};

    String[] linkBrairaRiyadh ={"http://olaya.boudl.com/V8Client/Inquiry.aspx","http://wizarat.boudl.com/V8Client/Inquiry.aspx","http://qurtuba.boudl.com/V8Client/Inquiry.aspx","http://hattien.boudl.com/V8Client/Inquiry.aspx"};
    String[] linkBrairaKhobar ={"http://azizia.boudl.com/V8Client/Inquiry.aspx"};
    String[] linkBrairaDammam ={"http://dammam.boudl.com/V8Client/Inquiry.aspx"};
    int roomPos, adultPos, childPos;
    boolean isFirstTime = false;
    boolean isCheckOutShown = false, isCheckInSelected = false;

    ArrayList<CityList> allCitiesList = new ArrayList<>();
    ArrayList<HotelList> allHotelsList = new ArrayList<>();
    ArrayList<BranchList> allBranchesList = new ArrayList<>();

    ArrayList<Brand> brandArrayList = new ArrayList<>();
    ArrayList<City> cityArrayList = new ArrayList<>();
    ArrayList<Branch> branchArrayList = new ArrayList<>();

    ArrayList<String> finalBrands = new ArrayList<>();
    ArrayList<String> finalBrandsAr = new ArrayList<>();
    ArrayList<String> finalBrandsid = new ArrayList<>();
    ArrayList<String> finalCities = new ArrayList<>();
    ArrayList<String> finalCitiesAr = new ArrayList<>();
    ArrayList<String> finalbranches = new ArrayList<>();
    ArrayList<String> finalbranchesAr = new ArrayList<>();
    ArrayList<String> finallinks = new ArrayList<>();
    ArrayList<String> finallinks_en = new ArrayList<>();
    ArrayList<String> finallinks_ar = new ArrayList<>();


    String language;
    String response;
    SharedPreferences languagePrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_search);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_search_arabic);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Light.ttf");
            regularTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }
        else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Light.ttf");
            regularTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }


        selectedPos = getIntent().getIntExtra("pos", -1);
        Log.i("TAG", "selectedPos " + selectedPos);

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);
        ((TextView) findViewById(R.id.loyalty)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.hotel_text)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.etHotelName)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.location_text)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.etLocation)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.hotel_text_select)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.etHotelNameSelect)).setTypeface(semiBoldTypeface);

//        ((TextView) findViewById(R.id.checkInText)).setTypeface(lightTypeface);
//        ((TextView) findViewById(R.id.checkInDate)).setTypeface(regularTypeface);
//        ((TextView) findViewById(R.id.checkInMonth)).setTypeface(regularTypeface);
//        ((TextView) findViewById(R.id.checkInWeek)).setTypeface(regularTypeface);
//
//        ((TextView) findViewById(R.id.checkOutText)).setTypeface(lightTypeface);
//        ((TextView) findViewById(R.id.checkOutDate)).setTypeface(regularTypeface);
//        ((TextView) findViewById(R.id.checkOutMonth)).setTypeface(regularTypeface);
//        ((TextView) findViewById(R.id.checkOutWeek)).setTypeface(regularTypeface);
//
//        ((TextView) findViewById(R.id.nightsCount)).setTypeface(regularTypeface);
//        ((TextView) findViewById(R.id.nightsText)).setTypeface(lightTypeface);
//
//        ((TextView) findViewById(R.id.etRooms)).setTypeface(semiBoldTypeface);
//        ((TextView) findViewById(R.id.roomText)).setTypeface(lightTypeface);
//
//        ((TextView) findViewById(R.id.etAdults)).setTypeface(semiBoldTypeface);
//        ((TextView) findViewById(R.id.adlutText)).setTypeface(lightTypeface);
//
//        ((TextView) findViewById(R.id.etChild)).setTypeface(semiBoldTypeface);
//        ((TextView) findViewById(R.id.childText)).setTypeface(lightTypeface);

        ((TextView) findViewById(R.id.searchHotel)).setTypeface(semiBoldTypeface);

//        tvCheckInDate = (TextView) findViewById(R.id.checkInDate);
//        tvCheckInMonth = (TextView) findViewById(R.id.checkInMonth);
//        tvCheckInWeek = (TextView) findViewById(R.id.checkInWeek);
        hotelSelect = (EditText) findViewById(R.id.etHotelNameSelect);
        etBrand = (EditText) findViewById(R.id.etHotelName);
        etCity = (EditText) findViewById(R.id.etLocation);

//        tvCheckOutDate = (TextView) findViewById(R.id.checkOutDate);
//        tvCheckOutMonth = (TextView) findViewById(R.id.checkOutMonth);
//        tvCheckOutWeek = (TextView) findViewById(R.id.checkOutWeek);

        search = (TextView) findViewById(R.id.searchHotel);

//        numNights = (TextView) findViewById(R.id.nightsCount);

//        etRoom = (TextView) findViewById(R.id.etRooms);
//        etAdult = (TextView) findViewById(R.id.etAdults);
//        etChild = (TextView) findViewById(R.id.etChild);

//        nightsText = (TextView) findViewById(R.id.nightsText);
//        adlutText = (TextView) findViewById(R.id.adlutText);
//        childText = (TextView) findViewById(R.id.childText);
//        roomText = (TextView) findViewById(R.id.roomText);

        logo = (ImageView) findViewById(R.id.logo);
        back_btn = (ImageView) findViewById(R.id.back_btn);
//        checkOutLayout = (RelativeLayout) findViewById(R.id.checkoutLayout);
//        checkInLayout = (RelativeLayout) findViewById(R.id.checkinLayout);

//        roomsSpinner = (Spinner) findViewById(R.id.resort_spinner);
//        adultSpinner = (Spinner) findViewById(R.id.adult_spinner);
//        childSpinner = (Spinner) findViewById(R.id.child_spinner);
        hotelSpinner = (Spinner) findViewById(R.id.hotel_spinner);
        brandSpinner = (Spinner) findViewById(R.id.brand_spinner);
        citySpinner = (Spinner) findViewById(R.id.city_spinner);

        new GetBooking().execute();

//        roomsAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, rooms) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//
//                ((TextView) v).setTextSize(1);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setTextSize(15);
//                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                return v;
//            }
//        };
//
//        adultsAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, adultCount1) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//
//                ((TextView) v).setTextSize(1);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setTextSize(15);
//                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                return v;
//            }
//        };
//
//        adultsAdapter1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, adultCount2) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//
//                ((TextView) v).setTextSize(1);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setTextSize(15);
//                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                return v;
//            }
//        };
//
//        adultsAdapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, adultCount3) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//
//                ((TextView) v).setTextSize(1);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setTextSize(15);
//                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                return v;
//            }
//        };
//
//        adultsAdapter3 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, adultCount4) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//
//                ((TextView) v).setTextSize(1);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setTextSize(15);
//                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                return v;
//            }
//        };
//
//        childrenAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, childCount1) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//
//                ((TextView) v).setTextSize(1);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setTextSize(15);
//                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                return v;
//            }
//        };
//
//        childrenAdapter1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, childCount2) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//
//                ((TextView) v).setTextSize(1);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setTextSize(15);
//                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                return v;
//            }
//        };
//
//        childrenAdapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, childCount3) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//
//                ((TextView) v).setTextSize(1);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setTextSize(15);
//                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                return v;
//            }
//        };

//        childrenAdapter3 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, childCount4) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//
//                ((TextView) v).setTextSize(1);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setTextSize(15);
//                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
//                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                return v;
//            }
//        };

//        roomsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                etRoom.setText(rooms[i]);
//                roomPos = i;
//                if (i == 0) {
//                    adultSpinner.setAdapter(adultsAdapter);
//                    childSpinner.setAdapter(childrenAdapter);
////                    adultSpinner.setSelection(1);
////                    childSpinner.setSelection(2);
////                    etAdult.setText("02");
////                    etChild.setText("02");
//                } else if (i == 1) {
//                    adultSpinner.setAdapter(adultsAdapter1);
//                    childSpinner.setAdapter(childrenAdapter1);
////                    adultSpinner.setSelection(3);
////                    childSpinner.setSelection(4);
////                    etAdult.setText("04");
////                    etChild.setText("04");
//                } else if (i == 2) {
//                    adultSpinner.setAdapter(adultsAdapter2);
//                    childSpinner.setAdapter(childrenAdapter2);
////                    adultSpinner.setSelection(5);
////                    childSpinner.setSelection(6);
////                    etAdult.setText("06");
////                    etChild.setText("06");
//                } else if (i == 3) {
//                    adultSpinner.setAdapter(adultsAdapter3);
//                    childSpinner.setAdapter(childrenAdapter3);
////                    adultSpinner.setSelection(7);
////                    childSpinner.setSelection(8);
////                    etAdult.setText("08");
////                    etChild.setText("08");
//                }
//
//                if (i == 0) {
//                    if (language.equalsIgnoreCase("En")) {
//                        roomText.setText("Room");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        roomText.setText("غرفة");
//                    }
//                } else {
//                    if (language.equalsIgnoreCase("En")) {
//                        roomText.setText("Rooms");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        roomText.setText("غرف");
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//        adultSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                adultPos = i;
//                etAdult.setText(adultCount1[i]);
////                if (roomPos == 0) {
////                    etAdult.setText(adultCount1[i]);
////                } else if (roomPos == 1) {
////                    etAdult.setText(adultCount2[i]);
////                } else if (roomPos == 2) {
////                    etAdult.setText(adultCount3[i]);
////                } else if (roomPos == 3) {
////                    etAdult.setText(adultCount4[i]);
////                }
//                if (i == 0) {
//
//                    if (language.equalsIgnoreCase("En")) {
//                        adlutText.setText("Adult");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        adlutText.setText("بالغ");
//                    }
//
//                } else {
//
//                    if (language.equalsIgnoreCase("En")) {
//                        adlutText.setText("Adults");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        adlutText.setText("بالغون");
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//        childSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                childPos = i;
//                etChild.setText(childCount1[i]);
//
////                if (roomPos == 0) {
////                    etChild.setText(childCount1[i]);
////                } else if (roomPos == 1) {
////                    etChild.setText(childCount2[i]);
////                } else if (roomPos == 2) {
////                    etChild.setText(childCount3[i]);
////                } else if (roomPos == 3) {
////                    etChild.setText(childCount4[i]);
////                }
//
//                if (i == 1) {
//
//                    if (language.equalsIgnoreCase("En")) {
//                        childText.setText("Child");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        childText.setText("طفل");
//                    }
//
//                } else {
//                    if (language.equalsIgnoreCase("En")) {
//                        childText.setText("Children");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        childText.setText("أطفال");
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (!link.equalsIgnoreCase("")) {
                if(finalbranches.size() > 0 || finalbranchesAr.size() > 0){
                    Intent AboutIntent = new Intent(SearchActivity.this, WebViewActivity.class);
                    if(language.equalsIgnoreCase("En")) {
                        AboutIntent.putExtra("title", finalbranches.get(hotelPos));
                        AboutIntent.putExtra("url", finallinks_en.get(hotelPos));
                    }
                    else{
                        AboutIntent.putExtra("title", finalbranchesAr.get(hotelPos));
                        AboutIntent.putExtra("url", finallinks_ar.get(hotelPos));
                    }
                    AboutIntent.putExtra("booking",true);
                    startActivity(AboutIntent);

//                    Intent AboutIntent = new Intent(SearchActivity.this, BookingDetails.class);
//                    if(language.equalsIgnoreCase("En")) {
//                        AboutIntent.putExtra("title", finalbranches.get(hotelPos));
//                        AboutIntent.putExtra("brandname", finalBrands.get(brandPos));
//                    }
//                    else{
//                        AboutIntent.putExtra("title", finalbranchesAr.get(hotelPos));
//                        AboutIntent.putExtra("brandname", finalBrandsAr.get(brandPos));
//                        AboutIntent.putExtra("title_en", finalbranches.get(hotelPos));
//                    }
//                    AboutIntent.putExtra("brandid",finalBrandsid.get(brandPos));
//                    Log.i("TAG", "brandid1 " + finalBrandsid.get(brandPos));
//                    startActivity(AboutIntent);
                }
            }
        });

        brandSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
                brandPos = i;
                cityPos = 0;
                hotelPos = 0;
                setAdapters(false, true, true);
                if (language.equalsIgnoreCase("En")) {
                    etBrand.setText(finalBrands.get(i));
                } else {
                    etBrand.setText(finalBrandsAr.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                cityPos = i;
                hotelPos = 0;
                setAdapters(false, false, true);
                if (language.equalsIgnoreCase("En")) {
                    etCity.setText(finalCities.get(i));
                } else {
                    etCity.setText(finalCitiesAr.get(i));
                }
//                setAdapters();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        hotelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                hotelPos = i;
//                    setAdapters(false, false, false);
                if (language.equalsIgnoreCase("En")) {
                    hotelSelect.setText(finalbranches.get(i));
                } else {
                    hotelSelect.setText(finalbranchesAr.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        roomsSpinner.setAdapter(roomsAdapter);
//        adultSpinner.setAdapter(adultsAdapter);
//        childSpinner.setAdapter(childrenAdapter);

        final Calendar c = Calendar.getInstance();
        mCheckInYear = c.get(Calendar.YEAR);
        mCheckInMonth = c.get(Calendar.MONTH);
        mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

//        if ((c.get(Calendar.DATE)) < 10) {
//            tvCheckInDate.setText("0" + c.get(Calendar.DATE));
//        } else {
//            tvCheckInDate.setText("" + c.get(Calendar.DATE));
//        }
//        tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
//        tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK) - 1)]);

        c.add(Calendar.DATE, 1);
        mCheckOutYear = c.get(Calendar.YEAR);
        mCheckOutMonth = c.get(Calendar.MONTH);
        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

//        if ((c.get(Calendar.DATE)) < 10) {
//            tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
//        } else {
//            tvCheckOutDate.setText("" + c.get(Calendar.DATE));
//        }
//        tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
//        tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK) - 1)]);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        checkInLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                checkInDatePicker();
//            }
//        });
//
//        checkOutLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                isCheckOutShown = false;
//                checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth + 1));
//            }
//        });
    }

    public void checkInDatePicker() {
        final Calendar c = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(SearchActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if(view.isShown()) {
                            mCheckInYear = year;
                            mCheckInDay = dayOfMonth;
                            mCheckInMonth = monthOfYear;

                            if (mCheckInDay < 10) {
                                tvCheckInDate.setText("0" + mCheckInDay);
                            } else {
                                tvCheckInDate.setText("" + mCheckInDay);
                            }
                            tvCheckInMonth.setText(MONTHS[mCheckInMonth]);
                            isCheckOutShown = false;
                            isCheckInSelected = true;
                            checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth + 1));
                        }
                    }
                }, mCheckInYear, mCheckInMonth, mCheckInDay);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + max);
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
            });
        datePickerDialog.setCancelable(false);
        if (language.equalsIgnoreCase("En")) {
            datePickerDialog.setMessage("Select Check In Date");
        } else {
            datePickerDialog.setMessage("إختر تاريخ الدخول");
        }
        datePickerDialog.show();
    }

    public void checkOutDatePicker(final int year, final int date, final int month) {
        final Calendar c = Calendar.getInstance();
        long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
        String givenDateString = year + "-" + (month) + "-" + date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(givenDateString);
            mCheckInDateStr = sdf.format(mCheckInDate);
            selectedMillis = mCheckInDate.getTime();
            c.setTime(mCheckInDate); // yourdate is an object of type Date
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            tvCheckInWeek.setText(WEEKS[(dayOfWeek - 1)]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        c.add(Calendar.DATE, 1);
        selectedMillis = c.getTimeInMillis();
        mCheckOutYear = c.get(Calendar.YEAR);
        mCheckOutMonth = c.get(Calendar.MONTH);
        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(SearchActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckOutYear = year;
                        mCheckOutDay = dayOfMonth;
                        mCheckOutMonth = monthOfYear;

                        if (mCheckOutDay < 10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay);
                        } else {
                            tvCheckOutDate.setText("" + mCheckOutDay);
                        }

                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);
                        isCheckInSelected = false;

                        String givenDateString = mCheckOutYear + "-" + (mCheckOutMonth + 1) + "-" + mCheckOutDay;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDate = sdf.parse(givenDateString);
                            mCheckOutDateStr = sdf.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek - 1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        CalculateNumOfDays();

                    }
                }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
        datePickerDialog.getDatePicker().setMinDate(selectedMillis);
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(selectedMillis + max);
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    // Do Stuff
                    if(isCheckInSelected){
                        String givenDateString = year + "-" + (month) + "-" + date;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckInDate = sdf.parse(givenDateString);
                            mCheckInDateStr = sdf.format(mCheckInDate);
//                            selectedMillis = mCheckInDate.getTime();
                            c.setTime(mCheckInDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckInWeek.setText(WEEKS[(dayOfWeek - 1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        c.add(Calendar.DATE, 1);
//                        selectedMillis = c.getTimeInMillis();
                        mCheckOutYear = c.get(Calendar.YEAR);
                        mCheckOutMonth = c.get(Calendar.MONTH);
                        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

                        if (mCheckOutDay < 10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay);
                        } else {
                            tvCheckOutDate.setText("" + mCheckOutDay);
                        }

                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);
                        isCheckInSelected = false;

                        String givenDateString1 = mCheckOutYear + "-" + (mCheckOutMonth + 1) + "-" + mCheckOutDay;
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDate = sdf1.parse(givenDateString1);
                            mCheckOutDateStr = sdf1.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek - 1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        CalculateNumOfDays();
                    }
                    datePickerDialog.dismiss();
                }
            }
        });
        datePickerDialog.setInverseBackgroundForced(true);
        if (language.equalsIgnoreCase("En")) {
            datePickerDialog.setMessage("Select Check Out Date");
        } else {
            datePickerDialog.setMessage(" إختر تاريخ المغادرة");
        }
        if(!isCheckOutShown) {
            datePickerDialog.show();
            isCheckOutShown = true;
        }
    }

    public void CalculateNumOfDays() {
        long diff = mCheckOutDate.getTime() - mCheckInDate.getTime();
        long NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        numNights.setText("" + NumofDaysStr);
        if (NumofDaysStr == 1) {

//            if (language.equalsIgnoreCase("En")) {
//                nightsText.setText("Night");
//            } else if (language.equalsIgnoreCase("Ar")) {
//                nightsText.setText("ليلة");
//            }
//
//        } else {
//            if (language.equalsIgnoreCase("En")) {
//
//                nightsText.setText("Nights");
//
//            } else if (language.equalsIgnoreCase("Ar")) {
//
//                nightsText.setText("عدد الليالى");
//
//            }
        }
    }

    public class GetBooking extends AsyncTask<String,Integer,String >{

        String networkStatus;
        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(SearchActivity.this);
//            dialog = ProgressDialog.show(SearchActivity.this, "",
//                    "Please wait..");
            dialog = new ProgressDialog(SearchActivity.this, ProgressDialog.THEME_HOLO_LIGHT);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Please wait");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.Booking_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
//                        StringEntity se = new StringEntity("", "UTF-8");
//
//                        // 6. set httpPost Entity
//                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(SearchActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(SearchActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {

                            JSONArray jsonArray = new JSONArray(result);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            JSONArray cityArray = jsonObject.getJSONArray("cityList");
                            for (int i = 0; i < cityArray.length(); i++) {
                                JSONObject cityObj = cityArray.getJSONObject(i);
                                CityList cityList = new CityList();
                                cityList.setId(cityObj.getString("Id"));
                                cityList.setCountryId(cityObj.getString("CountryId"));
                                cityList.setName_en(cityObj.getString("NameEn"));
                                cityList.setName_ar(cityObj.getString("NameAr"));
                                allCitiesList.add(cityList);
                            }

                            JSONArray hotelsArray = jsonObject.getJSONArray("HotelList");
                            for (int i = 0; i < hotelsArray.length(); i++) {
                                JSONObject cityObj = hotelsArray.getJSONObject(i);
                                HotelList cityList = new HotelList();
                                cityList.setId(cityObj.getString("Id"));
                                cityList.setName_en(cityObj.getString("NameEn"));
                                cityList.setName_ar(cityObj.getString("NameAr"));
                                cityList.setLogo_en(cityObj.getString("LogoEn"));
                                cityList.setLogo_ar(cityObj.getString("LogoAr"));
                                allHotelsList.add(cityList);
                            }

                            JSONArray branchArray = jsonObject.getJSONArray("BranchList");
                            for (int i = 0; i < branchArray.length(); i++) {
                                JSONObject cityObj = branchArray.getJSONObject(i);
                                BranchList cityList = new BranchList();
                                cityList.setId(cityObj.getString("Id"));
                                cityList.setHotelid(cityObj.getString("HotelId"));
                                cityList.setCityid(cityObj.getString("cityId"));
                                cityList.setBookingurl(cityObj.getString("BookingUrl"));
                                cityList.setName_en(cityObj.getString("NameEn"));
                                cityList.setName_ar(cityObj.getString("NameAr"));
                                cityList.setUrlEn(cityObj.getString("UrlEn"));
                                cityList.setUrlAr(cityObj.getString("UrlAr"));
                                allBranchesList.add(cityList);
                            }

//                            Collections.sort(allBranchesList, citySort);


                            for (int i = 0; i < allHotelsList.size(); i++){
                                Brand brand = new Brand();
                                brand.setId(allHotelsList.get(i).getId());
                                brand.setBrandName(allHotelsList.get(i).getName_en());
                                brand.setBrandNameAr(allHotelsList.get(i).getName_ar());
                                brand.setLogoEn(allHotelsList.get(i).getLogo_en());
                                brand.setLogoAr(allHotelsList.get(i).getLogo_ar());

                                ArrayList<String> dummyCityList = new ArrayList<>();
                                ArrayList<City> cityList = new ArrayList<>();
                                for (int j = 0; j < allBranchesList.size(); j++){
                                    if(!dummyCityList.contains(allBranchesList.get(j).getCityid())){
                                        if(allHotelsList.get(i).getId().equals(allBranchesList.get(j).getHotelid())) {
                                            dummyCityList.add(allBranchesList.get(j).getCityid());
                                            City city = new City();
                                            for (CityList cityList1 : allCitiesList) {
                                                if (allBranchesList.get(j).getCityid().equals(cityList1.getId())) {
                                                    city.setCityName(cityList1.getName_en());
                                                    city.setCityNameAr(cityList1.getName_ar());
                                                    city.setId(cityList1.getId());

                                                    ArrayList<Branch> branchArrayList = new ArrayList<>();
                                                    for (int k = 0; k < allBranchesList.size(); k++) {
                                                        if(allHotelsList.get(i).getId().equals(allBranchesList.get(k).getHotelid())) {
                                                            if (allBranchesList.get(k).getCityid().equals(cityList1.getId())) {
                                                                Branch branch = new Branch();

                                                                branch.setBranchName(allBranchesList.get(k).getName_en());
                                                                branch.setBranchNameAr(allBranchesList.get(k).getName_ar());
                                                                branch.setLink(allBranchesList.get(k).getBookingurl());
                                                                branch.setLink_en(allBranchesList.get(k).getUrlEn());
                                                                branch.setLink_ar(allBranchesList.get(k).getUrlAr());
                                                                branchArrayList.add(branch);
                                                            }
                                                        }
                                                    }
                                                    city.setBranchArrayList(branchArrayList);
                                                    break;
                                                }
                                            }
                                            cityList.add(city);
                                        }
                                    }
                                }
                                brand.setCityArrayList(cityList);
                                brandArrayList.add(brand);
                            }

                            setAdapters(true, true, true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(SearchActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public void setAdapters(boolean brandRefresh, boolean cityrefresh, boolean hotelRefresh){
        finalBrandsid.clear();
        finalBrands.clear();
        finalBrandsAr.clear();
        finalCities.clear();
        finalCitiesAr.clear();
        finalbranches.clear();
        finalbranchesAr.clear();
        finallinks.clear();
        finallinks_en.clear();
        finallinks_ar.clear();

        if (selectedPos != -1) {
            brandPos = selectedPos;
            selectedPos = -1;
        }

        for (Brand brand : brandArrayList){
            finalBrands.add(brand.getBrandName());
            finalBrandsAr.add(brand.getBrandNameAr());
            finalBrandsid.add(brand.getId());
            Log.i("TAG", "brandid" + finalBrandsid);
        }

        if(language.equalsIgnoreCase("En")){
            imageName = brandArrayList.get(brandPos).getLogoEn();
        }
        else{
            imageName = brandArrayList.get(brandPos).getLogoAr();
        }

        for (int i = 0; i < brandArrayList.get(brandPos).getCityArrayList().size(); i++){
            Log.i("TAG", "finalCities "+ brandArrayList.get(brandPos).getCityArrayList().get(i).getId());
            Log.i("TAG", "finalCities "+ brandArrayList.get(brandPos).getCityArrayList().get(i).getCityName());
            finalCities.add(brandArrayList.get(brandPos).getCityArrayList().get(i).getCityName());
            finalCitiesAr.add(brandArrayList.get(brandPos).getCityArrayList().get(i).getCityNameAr());
        }

        for (int i = 0; i < brandArrayList.get(brandPos).getCityArrayList().get(cityPos).getBranchArrayList().size(); i++){
            finalbranches.add(brandArrayList.get(brandPos).getCityArrayList().get(cityPos).getBranchArrayList().get(i).getBranchName());
            finalbranchesAr.add(brandArrayList.get(brandPos).getCityArrayList().get(cityPos).getBranchArrayList().get(i).getBranchNameAr());
            finallinks.add(brandArrayList.get(brandPos).getCityArrayList().get(cityPos).getBranchArrayList().get(i).getLink());
            finallinks_en.add(brandArrayList.get(brandPos).getCityArrayList().get(cityPos).getBranchArrayList().get(i).getLink_en());
            finallinks_ar.add(brandArrayList.get(brandPos).getCityArrayList().get(cityPos).getBranchArrayList().get(i).getLink_ar());
        }

        if(brandRefresh) {
            brandAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, finalBrands) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.LEFT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            brandAdapterAr = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, finalBrandsAr) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.RIGHT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            if (language.equalsIgnoreCase("En")) {
                brandSpinner.setAdapter(brandAdapter);
            } else {
                brandSpinner.setAdapter(brandAdapterAr);
            }

            brandSpinner.setSelection(brandPos);
        }

        if(cityrefresh) {
            cityAdapter1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, finalCities) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.LEFT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            cityAdapter1Ar = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, finalCitiesAr) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.RIGHT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            if (language.equalsIgnoreCase("En")) {
                citySpinner.setAdapter(cityAdapter1);
            } else {
                citySpinner.setAdapter(cityAdapter1Ar);
            }
        }

        if(hotelRefresh) {
            hotelNarcissusAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, finalbranches) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.LEFT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            hotelNarcissusAdapterAr = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, finalbranchesAr) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.RIGHT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            Glide.with(SearchActivity.this).load(Constants.IMAGE_URL+imageName).into(logo);
            if (language.equalsIgnoreCase("En")) {
                hotelSpinner.setAdapter(hotelNarcissusAdapter);
            } else {
                hotelSpinner.setAdapter(hotelNarcissusAdapterAr);
                }
        }
    }

    public static Comparator<BranchList> citySort = new Comparator<BranchList>() {

        public int compare(BranchList s1, BranchList s2) {

            float price1 = Integer.parseInt(s1.getCityid());
            float price2 = Integer.parseInt(s2.getCityid());

            /*For ascending order*/
            return Float.compare(price1,price2);

            /*For descending order*/
//            return price2-price1;
        }};
}
