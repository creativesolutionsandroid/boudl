package com.cs.boudl.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cs.boudl.AmenitiesAdapter;
import com.cs.boudl.Constants;
import com.cs.boudl.CustomGridView;
import com.cs.boudl.Model.Offers;
import com.cs.boudl.R;
import com.cs.boudl.TextViewEx;

import java.util.ArrayList;

/**
 * Created by CS on 11-12-2017.
 */

public class OffersDescriptionActivity extends AppCompatActivity {

    ImageView back_btn, icon;
    //    JustifyTextView text7;
    TextView title, price_hotal;
    String titleStr, descStr, title_ar, desc_ar, imageStr;
    TextView header_title, websiteButton;
    TextViewEx text8;
    WebView webView;

    CustomGridView mamenities;
    AmenitiesAdapter amenitiesAdapter;


    String language, link, mode, price, amenities_name_en, amenities_name_ar, amenities_icon, hotalnameEn, hotalnameAr;
    SharedPreferences languagePrefs;
    Typeface lightTypeface, semiBoldTypeface, boldTypeface;

    ArrayList<Offers> offers = new ArrayList<>();

    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    String bookingno;



    @SuppressLint("JavascriptInterface")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_news);
        } else {
            setContentView(R.layout.activity_new_arabic);
        }

        titleStr = getIntent().getStringExtra("title");
        title_ar = getIntent().getStringExtra("title1");
        descStr = getIntent().getStringExtra("desc");
        desc_ar = getIntent().getStringExtra("desc1");
        imageStr = getIntent().getStringExtra("image");
        link = getIntent().getStringExtra("link");
        mode = getIntent().getStringExtra("mode");
        price = getIntent().getStringExtra("price");
        amenities_name_en = getIntent().getStringExtra("amenities_name_En");
        amenities_name_ar = getIntent().getStringExtra("amenities_name_Ar");
        amenities_icon = getIntent().getStringExtra("anenities_icon");

        Log.d("TAG", "amenities " + amenities_icon);
        offers = (ArrayList<Offers>) getIntent().getSerializableExtra("offers_array");
        hotalnameAr = getIntent().getStringExtra("hotalnameAr");
        hotalnameEn = getIntent().getStringExtra("hotalnameEn");

        Log.i("TAG", "onCreate: " + desc_ar);

        header_title = (TextView) findViewById(R.id.title);
        websiteButton = (TextView) findViewById(R.id.offerWebsite);

//        if (language.equalsIgnoreCase("En")) {
            mamenities = (CustomGridView) findViewById(R.id.amenities);
//        }

//        if (language.equalsIgnoreCase("En")) {

            price_hotal = findViewById(R.id.price);

            price_hotal.setVisibility(View.VISIBLE);
//        }
        websiteButton.setVisibility(View.VISIBLE);

        if (mode.equalsIgnoreCase("2")) {

            if (link.equalsIgnoreCase("")) {
                websiteButton.setVisibility(View.GONE);
            } else {
                websiteButton.setVisibility(View.VISIBLE);
                websiteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent AboutIntent = new Intent(OffersDescriptionActivity.this, WebViewActivity.class);
                        if (language.equalsIgnoreCase("En")) {
                            AboutIntent.putExtra("title", "Offers");
                        } else {
                            AboutIntent.putExtra("title", "اخر العروض");
                        }
                        AboutIntent.putExtra("url", link);
                        AboutIntent.putExtra("booking",false);
                        startActivity(AboutIntent);
                    }
                });
            }
            if (language.equalsIgnoreCase("En")) {

                price_hotal.setText(price + " SAR per night");
            }
            else {

                price_hotal.setText(  price +" ريال سعودي -  لليلة واحدة ");
            }


        } else if (mode.equalsIgnoreCase("1")) {

            if (language.equalsIgnoreCase("En")) {
                price_hotal.setText(price + " SAR per person");
            }
            else {
                price_hotal.setText( price +" ريال سعودي - للفرد " );
            }
            websiteButton.setVisibility(View.VISIBLE);
            websiteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (language.equalsIgnoreCase("En")) {
                        if (hotalnameEn.equalsIgnoreCase("Boudl")) {

                            bookingno = "0096692000666";

                        } else if (hotalnameEn.equalsIgnoreCase("Braira")) {

                            bookingno = "0096692000555";

                        } else if (hotalnameEn.equalsIgnoreCase("Narcissus")) {

                            bookingno = "00966920000777";

                        } else if (hotalnameEn.equalsIgnoreCase("Aber Hotel")) {

                            bookingno = "0096692000666";

                        }
                    }else {


                        if (hotalnameAr.equalsIgnoreCase("بودل")) {

                            bookingno = "0096692000666";

                        } else if (hotalnameAr.equalsIgnoreCase("بریرا")) {

                            bookingno = "0096692000555";

                        } else if (hotalnameAr.equalsIgnoreCase("نارسس")) {

                            bookingno = "00966920000777";

                        } else if (hotalnameAr.equalsIgnoreCase("عابر")) {

                            bookingno = "0096692000666";

                        }
                    }

                    int currentapiVersion = Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {
                        if (!canAccessPhonecalls()) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                            }
                        } else {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + bookingno));
                            if (ActivityCompat.checkSelfPermission(OffersDescriptionActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(intent);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + bookingno));
                        startActivity(intent);
                    }

                }
            });
        }

        if (language.equalsIgnoreCase("En")) {

            header_title.setText(" Offers ");

        } else if (language.equalsIgnoreCase("Ar")) {

            header_title.setText(" اخر العروض ");

        }

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Regular.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        } else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Regular.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }


        webView = (WebView) findViewById(R.id.webView);

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);
        ((TextView) findViewById(R.id.title1)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.offerWebsite)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.price)).setTypeface(lightTypeface);

        if (language.equalsIgnoreCase("En")) {
//            ((TextView) findViewById(R.id.desc)).setTypeface(lightTypeface);
        } else if (language.equalsIgnoreCase("Ar")) {
//            ((TextView) findViewById(R.id.desc2)).setTypeface(lightTypeface);
        }

        icon = (ImageView) findViewById(R.id.image);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        title = (TextView) findViewById(R.id.title1);

        if (language.equalsIgnoreCase("En")) {
//            text7 = (JustifyTextView) findViewById(R.id.desc);
        } else if (language.equalsIgnoreCase("Ar")) {
            text8 = findViewById(R.id.desc2);
        }

        if (language.equalsIgnoreCase("En")) {
            title.setText(titleStr);
        } else if (language.equalsIgnoreCase("Ar")) {
            title.setText(title_ar);
        }



        webView.setVisibility(View.VISIBLE);
        if (language.equalsIgnoreCase("En")) {
            webView.loadDataWithBaseURL(null, descStr, "text/html", "utf-8", null);
        } else {
//            webView.setVisibility(View.GONE);
//            String webview = webView.loadDataWithBaseURL(null, desc_ar, "text/html", "utf-8", null);;

            text8.setVisibility(View.GONE);
            text8.setTypeface(lightTypeface);
//
            String mHtmlString = "<html dir=\"rtl\" lang=\"\"><body>" + desc_ar + "</body></html>";
            text8.setText(String.valueOf(Html.fromHtml(Html.fromHtml(mHtmlString).toString())),true);
//            String justifyTag = "<html><body style='text-align:justify;'>%s</body></html>";

            String des_ar ;
            

//            String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/font/Cairo-Regular.ttf\")}body {font-family: MyFont;font-size: medium;text-align: justify;}</style></head><body>";
//            String pas = "</body></html>";
//            String myHtmlString = pish + desc_ar + pas;

//            String dataString = String.format(Locale.US, justifyTag, desc_ar);

            Log.d("TAG", "onCreate:1 "+desc_ar);

            desc_ar =  text8.getText().toString();

            Log.d("TAG", "onCreate: " +desc_ar);

            des_ar =  "<html> <body style= font-family: Cairo-Regular.ttf>" + mHtmlString + "</body></html>";

            webView.loadDataWithBaseURL("file:///android_assets/demo.html", des_ar , "text/html", "UTF-8", null);



//            webView.loadDataWithBaseURL(null, myHtmlString, "text/html", "utf-8", null);
        }

        Glide.with(OffersDescriptionActivity.this).load(Constants.IMAGE_URL + imageStr)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE).dontTransform().into(icon);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        String[] animeties_name_en = amenities_name_en.split(",");
        String[] animeties_name_ar = amenities_name_ar.split(",");
        String[] animeties_icon = amenities_icon.split(",");

//        if (language.equalsIgnoreCase("En")) {

            if (mode.equalsIgnoreCase("2")) {

                mamenities.setVisibility(View.VISIBLE);
                amenitiesAdapter = new AmenitiesAdapter(OffersDescriptionActivity.this, animeties_name_en, animeties_name_ar, animeties_icon, language);
                mamenities.setAdapter(amenitiesAdapter);

            } else {

                mamenities.setVisibility(View.GONE);

            }
//        }
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OffersDescriptionActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + bookingno));
                    if (ActivityCompat.checkSelfPermission(OffersDescriptionActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(OffersDescriptionActivity.this, "Call phone permission denied, unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
