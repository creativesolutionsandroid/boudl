package com.cs.boudl.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.cs.boudl.Firebase.Config;
import com.cs.boudl.MainActivity;
import com.cs.boudl.R;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by CS on 28-04-2017.
 */

public class SplashScreenActivity extends AppCompatActivity {

    public static String regId = "";
    BroadcastReceiver mRegistrationBroadcastReceiver;
    String response;
    private static final String TAG = "TAG";
    String title, icon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    regId = pref.getString("regId", null);

                    Log.i("TAG", "Firebase reg id: " + regId);
                }
            }

        };

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);

        Log.i("TAG", "Firebase reg id: " + regId);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, 1000);

    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

//    @Override
//    public void onPointerCaptureChanged(boolean hasCapture) {
//
//    }
//
//    @Override
//    public void remoteMessagevalue(RemoteMessage remoteMessages) {
//
//        Log.e(TAG, "From: " + remoteMessages.getFrom());
//
//        if (remoteMessages == null)
//            return;
//
//        if (remoteMessages.getNotification() != null) {
//            Log.d(TAG, "onMessageReceived: got message");
//            Intent intent = new Intent(this, NotificationFragment.class);
//            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
//
//            Notification noti = new Notification.Builder(SplashScreenActivity.this)
//                    .setContentTitle(title)
//                    .setContentText(remoteMessages.getNotification().getBody())
//                    .setSmallIcon(R.drawable.ic_notification_icon)
//                    .setColor(Color.parseColor("#000000"))
//                    .setPriority(Notification.PRIORITY_HIGH)
//                    .setDefaults(NotificationCompat.DEFAULT_ALL)
//                    .setContentIntent(resultPendingIntent).build();
//            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//            // hide the notification after its selected
//            noti.flags |= Notification.FLAG_AUTO_CANCEL;
//
//            notificationManager.notify((int) System.currentTimeMillis(), noti);
//        }
//
//        if (remoteMessages.getData().size() > 0) {
//            Log.d(TAG, "Data Payload: " + remoteMessages.getData().toString());
//            String message = "";
//            int pnType = 1;
////            try {
////                JSONObject resultObj = new JSONObject(remoteMessage.getData().toString());
////                message = resultObj.getString("text");
////                pnType = resultObj.getInt("PnType");
////            } catch (JSONException e) {
////                e.printStackTrace();
////            }
//
//            Map<String, String> notificationMap = remoteMessages.getData();
//            for (Map.Entry<String, String> e : notificationMap.entrySet()) {
//                if (e.getKey().equals("text")) {
//                    message = e.getValue();
//                }
//                if (e.getKey().equals("PnType")) {
//                    pnType = Integer.parseInt(e.getValue());
//                }
//            }
//
//            Intent intent = null;
//            if (pnType == 1) {
//                intent = new Intent(this, NotificationFragment.class);
//            } else if (pnType == 2) {
//                intent = new Intent(this, NewsFragment.class);
//            } else if (pnType == 3) {
//                intent = new Intent(this, OffersFragment.class);
//            }
//
//            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
//
//            Notification noti = new Notification.Builder(SplashScreenActivity.this)
//                    .setContentTitle(title)
//                    .setContentText(message)
//                    .setSmallIcon(R.drawable.ic_notification_icon)
//                    .setColor(Color.parseColor("#000000"))
//                    .setPriority(Notification.PRIORITY_HIGH)
//                    .setDefaults(NotificationCompat.DEFAULT_ALL)
//                    .setContentIntent(resultPendingIntent).build();
//            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//            // hide the notification after its selected
//            noti.flags |= Notification.FLAG_AUTO_CANCEL;
//
//            notificationManager.notify((int) System.currentTimeMillis(), noti);
//
//            Log.i(TAG, "remoteMessagevalue: " + remoteMessages);
//        }
//    }
}