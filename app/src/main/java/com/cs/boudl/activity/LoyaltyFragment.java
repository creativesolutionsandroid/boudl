package com.cs.boudl.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.boudl.R;

/**
 * Created by CS on 11-12-2017.
 */

public class LoyaltyFragment extends AppCompatActivity {

    ImageView menu, mainImage;
    TextView loyalty, title;
    View rootview;
    SharedPreferences languagePrefs;
    String language;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.fragment_loyalty);
        }else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.fragment_loyalty_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Light.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Light.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);
        ((TextView) findViewById(R.id.loyalty)).setTypeface(lightTypeface);

        menu = (ImageView) findViewById(R.id.menu);
//        mainImage = (ImageView) findViewById(R.id.mainImage);

        loyalty = (TextView) findViewById(R.id.loyalty);
        title = (TextView) findViewById(R.id.title);

//        if(language.equalsIgnoreCase("En")){
//            mainImage.setImageResource(R.drawable.loyalty);
//        }
//        else{
//            mainImage.setImageResource(R.drawable.loyalty_ar);
//            loyalty.setText("نقاط المكافآت");
//            title.setText("مجموعة بودل");
//        }

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
}
