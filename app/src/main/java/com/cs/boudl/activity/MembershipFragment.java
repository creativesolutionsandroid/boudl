package com.cs.boudl.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.boudl.R;

/**
 * Created by CS on 20-01-2017.
 */
public class MembershipFragment extends AppCompatActivity {

    ImageView menu;
    View rootView;
    SharedPreferences languagePrefs;
    String language;
    TextView header_title,mplatinum,mgolden,msilver;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface;
    WebView desc_web, desc_web1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
        setContentView(R.layout.fragement_membership);
        }else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.fragement_membership_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);
//        ((TextView) findViewById(R.id.platinumText)).setTypeface(semiBoldTypeface);
//        ((TextView) findViewById(R.id.goldenText)).setTypeface(semiBoldTypeface);
//        ((TextView) findViewById(R.id.silverText)).setTypeface(semiBoldTypeface);

        header_title = findViewById(R.id.title);
//        mplatinum = findViewById(R.id.platinumText);
//        mgolden = findViewById(R.id.goldenText);
//        msilver = findViewById(R.id.silverText);

        menu = (ImageView) findViewById(R.id.menu);

//        if (language.equalsIgnoreCase("En")) {
//            header_title.setText("Membership");
//        }else if (language.equalsIgnoreCase("Ar")) {
//            header_title.setText("برنامج العضوية");
//        }
//
//        if (language.equalsIgnoreCase("En")) {
//            mplatinum.setText("Platinum Card");
//        }else if (language.equalsIgnoreCase("Ar")) {
//            mplatinum.setText("بطاقة بلاتينية");
//        }
//
//        if (language.equalsIgnoreCase("En")) {
//            mgolden.setText("Golden Card");
//        }else if (language.equalsIgnoreCase("Ar")) {
//            mgolden.setText("بطاقة ذهبية");
//        }
//
//        if (language.equalsIgnoreCase("En")) {
//            msilver.setText("Silver Card");
//        }else if (language.equalsIgnoreCase("Ar")) {
//            msilver.setText("بطاقة فضية");
//        }


        if (language.equalsIgnoreCase("En")) {
            desc_web = findViewById(R.id.desc);
            desc_web1 = findViewById(R.id.desc1);


            String desc = "Welcome to ( Boudl Club ), we wish you a pleasant stay with us.As a courtesy to our customers and those who spent time with us, and for the sake of continuity between us and our honored guests whom through their trust we were able to achieve this wide spread across the kingdom and the Gulf, through a wide chain of full readiness at any time, For all of that we are pleased to offer you this special program of Boudl Club membership.";
            String desc1 = "It is to enjoy the privileges granted to you through having the membership card, that gives you one point for every Riyal or equivalent in other currencies. and when you collect 3,000 points, you can start enjoying the privileges provided by this program.\n" +
                    "To Join Boudl Club please dial 92 0000 666.";

            String mdesc = "<html > <body style=\"text-align:justify;color:black;\">" + desc + "</body></html>";
            String mdesc1 = "<html > <body style=\"text-align:justify;color:black;\">" + desc1 + "</body></html>";

            desc_web.loadDataWithBaseURL(null, mdesc, "text/html", "utf-8", null);

            desc_web1.loadDataWithBaseURL(null, mdesc1, "text/html", "utf-8", null);
        }

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
