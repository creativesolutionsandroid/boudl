package com.cs.boudl.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.boudl.AmenitiesAdapter1;
import com.cs.boudl.Constants;
import com.cs.boudl.CustomGridView;
import com.cs.boudl.Model.AmenitiesBooking;
import com.cs.boudl.Model.Banners;
import com.cs.boudl.Model.BranchDetails;
import com.cs.boudl.NetworkUtil;
import com.cs.boudl.R;
import com.cs.boudl.ViewPageAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class BookingDetails extends AppCompatActivity {

    Typeface lightTypeface, semiBoldTypeface, boldTypeface, regularTypeface;
    TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek;
    TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
    ImageView back_btn;
    TextView etAdult, header_title, search_hotal;
    TextView adlutText;
    RelativeLayout checkInLayout, checkOutLayout;
    private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    String[] adultCount1 = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
    String[] adultCount2 = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
    String[] adultCount3 = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
    String[] adultCount4 = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
    Date mCheckInDate = null, mCheckOutDate = null;
    String mCheckInDateStr = null, mCheckOutDateStr = null;
    Spinner adultSpinner;

    String checkin, adultcount, booking_url, booking_url_ar;

    ArrayList<Banners> BannersList = new ArrayList<>();
    ArrayList<BranchDetails> BranchList = new ArrayList<>();
    ArrayList<AmenitiesBooking> AnenitiesList = new ArrayList<>();

    public static ArrayList<String> img = new ArrayList<>();
    public static ArrayList<String> mediaid = new ArrayList<>();
    public static ArrayList<String> amenities_name_en = new ArrayList<>();
    public static ArrayList<String> amenities_name_ar = new ArrayList<>();
    public static ArrayList<String> amenities_icon = new ArrayList<>();


    ArrayAdapter<String> adultsAdapter, adultsAdapter1, adultsAdapter2, adultsAdapter3;
    boolean isCheckOutShown = false, isCheckInSelected = false;
    int adultPos;
    ViewPageAdapter viewPageAdapter;
    public static ViewPager viewPager;

    CustomGridView mamenities;
    AmenitiesAdapter1 amenitiesAdapter;

    String brandname, branchname, branchname_en, brandname_ar, branchname_ar, brandid;

    long NumofDaysStr = 1;
    String language;
    SharedPreferences languagePrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.booking_details);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.booking_details_ar);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Light.ttf");
            regularTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        } else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Light.ttf");
            regularTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        ((TextView) findViewById(R.id.checkInText)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.checkInDate)).setTypeface(regularTypeface);
        ((TextView) findViewById(R.id.checkInMonth)).setTypeface(regularTypeface);
        ((TextView) findViewById(R.id.checkInWeek)).setTypeface(regularTypeface);

        ((TextView) findViewById(R.id.checkOutText)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.checkOutDate)).setTypeface(regularTypeface);
        ((TextView) findViewById(R.id.checkOutMonth)).setTypeface(regularTypeface);
        ((TextView) findViewById(R.id.checkOutWeek)).setTypeface(regularTypeface);

        ((TextView) findViewById(R.id.etAdults)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.adlutText)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);

        tvCheckInDate = (TextView) findViewById(R.id.checkInDate);
        tvCheckInMonth = (TextView) findViewById(R.id.checkInMonth);
        tvCheckInWeek = (TextView) findViewById(R.id.checkInWeek);

        tvCheckOutDate = (TextView) findViewById(R.id.checkOutDate);
        tvCheckOutMonth = (TextView) findViewById(R.id.checkOutMonth);
        tvCheckOutWeek = (TextView) findViewById(R.id.checkOutWeek);

        header_title = (TextView) findViewById(R.id.title);

        search_hotal = (TextView) findViewById(R.id.searchHotel);

        etAdult = (TextView) findViewById(R.id.etAdults);
        adlutText = (TextView) findViewById(R.id.adlutText);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        checkOutLayout = (RelativeLayout) findViewById(R.id.checkoutLayout);
        checkInLayout = (RelativeLayout) findViewById(R.id.checkinLayout);

        adultSpinner = (Spinner) findViewById(R.id.adult_spinner);

        viewPager = findViewById(R.id.pager);
        mamenities = (CustomGridView) findViewById(R.id.amenities);

        if (language.equalsIgnoreCase("En")) {
            branchname = getIntent().getStringExtra("title");
            brandname = getIntent().getStringExtra("brandname");
        } else if (language.equalsIgnoreCase("Ar")) {
            branchname_ar = getIntent().getStringExtra("title");
            branchname_en = getIntent().getStringExtra("title_en");
            brandname_ar = getIntent().getStringExtra("brandname");
        }
        brandid = getIntent().getStringExtra("brandid");

        Log.i("TAG", "onCreate: " + brandid);
        Log.i("TAG", "onCreate1: " + brandname);
        Log.i("TAG", "onCreate2: " + branchname_en);

        if (language.equalsIgnoreCase("En")) {
            header_title.setText("" + branchname);
        } else {
            header_title.setText("" + branchname_ar);
        }


        int id = Integer.parseInt(brandid);
        Log.i("TAG", " " + id);

        try {

            JSONObject mainObj = new JSONObject();
            mainObj.put("Id", id);
            if (language.equalsIgnoreCase("En")) {
                mainObj.put("NameEn", branchname);
            }else {
                mainObj.put("NameEn", branchname_en);
            }


            Log.i("TAG", mainObj.toString());

            new GetBookingDetails().execute(mainObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        adultsAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, adultCount1) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        adultsAdapter1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, adultCount2) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        adultsAdapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, adultCount3) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        adultsAdapter3 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, adultCount4) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        adultSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                adultPos = i;
                etAdult.setText(adultCount1[i]);

                adultcount = adultCount1[i];

                Log.i("TAG", "onItemSelected: " + adultcount);
//                if (roomPos == 0) {
//                    etAdult.setText(adultCount1[i]);
//                } else if (roomPos == 1) {
//                    etAdult.setText(adultCount2[i]);
//                } else if (roomPos == 2) {
//                    etAdult.setText(adultCount3[i]);
//                } else if (roomPos == 3) {
//                    etAdult.setText(adultCount4[i]);
//                }
                if (i == 0) {

                    if (language.equalsIgnoreCase("En")) {
                        adlutText.setText("Adult");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        adlutText.setText("بالغ");
                    }

                } else {

                    if (language.equalsIgnoreCase("En")) {
                        adlutText.setText("Adults");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        adlutText.setText("بالغون");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        adultSpinner.setAdapter(adultsAdapter);

        final Calendar c = Calendar.getInstance();
        mCheckInYear = c.get(Calendar.YEAR);
        mCheckInMonth = c.get(Calendar.MONTH);
        mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

        if ((c.get(Calendar.DATE)) < 10) {
            tvCheckInDate.setText("0" + c.get(Calendar.DATE));
        } else {
            tvCheckInDate.setText("" + c.get(Calendar.DATE));
        }
        tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK) - 1)]);

        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.US);


        checkin = df.format(c.getTime());

        Log.i("TAG", "onCreate: " + checkin);

        c.add(Calendar.DATE, 1);
        mCheckOutYear = c.get(Calendar.YEAR);
        mCheckOutMonth = c.get(Calendar.MONTH);
        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

        if ((c.get(Calendar.DATE)) < 10) {
            tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
        } else {
            tvCheckOutDate.setText("" + c.get(Calendar.DATE));
        }
        tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK) - 1)]);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        checkInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInDatePicker();
            }
        });

        checkOutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCheckOutShown = false;
                checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth + 1));
            }
        });


        search_hotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("TAG", "onItemSelected: " + adultcount);

                Log.i("TAG", "CalculateNumOfDays: " + NumofDaysStr);

                Log.i("TAG", "onCreate: " + checkin);

                for (int i = 0; i < BranchList.size(); i++) {

                    if (language.equalsIgnoreCase("En")) {
                        booking_url = BranchList.get(i).getBookingUrl() + "?culture=en-gb&date=" + checkin + "&nights=" + NumofDaysStr + "&adults=" + adultcount;
                    } else if (language.equalsIgnoreCase("Ar")) {
                        booking_url_ar = BranchList.get(i).getBookingUrl() + "?culture=en-us&date=" + checkin + "&nights=" + NumofDaysStr + "&adults=" + adultcount;
                    }
                }

                Log.i("TAG", "onClick: " + booking_url_ar);

                Intent AboutIntent = new Intent(BookingDetails.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    AboutIntent.putExtra("title", branchname);
                    AboutIntent.putExtra("booking",false);
                } else {
                    AboutIntent.putExtra("title", branchname_ar);
                    AboutIntent.putExtra("booking",false);
                }
                if (language.equalsIgnoreCase("En")) {
                    AboutIntent.putExtra("url", booking_url);
                } else {
                    AboutIntent.putExtra("url", booking_url_ar);
                }

                startActivity(AboutIntent);


            }
        });

    }


    public class GetBookingDetails extends AsyncTask<String, Integer, String> {

        String networkStatus;
        ProgressDialog dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(BookingDetails.this);
//            dialog = ProgressDialog.show(BookingDetails.this, "",
//                    "Please wait..");
            dialog = new ProgressDialog(BookingDetails.this, ProgressDialog.THEME_HOLO_LIGHT);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Please wait");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.Booking_Detail_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");
//
//                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response1:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(BookingDetails.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(BookingDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {

                            JSONObject jo = new JSONObject(result);
//                            if (jo.has("MediaTypId")) {
                            JSONArray ja = jo.getJSONArray("Banners");
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo1 = ja.getJSONObject(i);
                                Banners banners = new Banners();
                                banners.setMediaTypId(jo1.optString("MediaTypId"));
                                banners.setSourcePath(jo1.optString("SourcePath"));
                                BannersList.add(banners);
                                Log.i("TAG", "onPostExecute: " + BannersList.size());
                            }
//                            }

//                            if (jo.has("Id")) {
                            JSONArray ja1 = jo.getJSONArray("BranchDetails");
                            for (int i = 0; i < ja1.length(); i++) {
                                JSONObject jo2 = ja1.getJSONObject(i);
                                BranchDetails branchDetails = new BranchDetails();
                                branchDetails.setId(jo2.getString("Id"));
                                branchDetails.setUserId(jo2.getString("UserId"));
                                branchDetails.setHotelId(jo2.getString("HotelId"));
                                branchDetails.setHotelNameEn(jo2.getString("HotelNameEn"));
                                branchDetails.setCountryId(jo2.getString("CountryId"));
                                branchDetails.setCityId(jo2.getString("CityId"));
                                branchDetails.setDistrictId(jo2.getString("DistrictId"));
                                branchDetails.setNameEn(jo2.getString("NameEn"));
                                branchDetails.setNameAr(jo2.getString("NameAr"));
                                branchDetails.setAboutEn(jo2.getString("AboutEn"));
                                branchDetails.setAboutAr(jo2.getString("AboutAr"));
                                branchDetails.setPhone(jo2.getString("Phone"));
                                branchDetails.setEmail(jo2.getString("Email"));
                                branchDetails.setLandline(jo2.getString("Landline"));
                                branchDetails.setFaxNo(jo2.getString("FaxNo"));
                                branchDetails.setAddressEn(jo2.getString("AddressEn"));
                                branchDetails.setAddressAr(jo2.getString("AddressAr"));
                                branchDetails.setLatitude(jo2.getString("Latitude"));
                                branchDetails.setLongitude(jo2.getString("Longitude"));
                                branchDetails.setBookingUrl(jo2.getString("BookingUrl"));
                                branchDetails.setImagePath(jo2.getString("ImagePath"));
                                branchDetails.setBranchRating(jo2.getString("BranchRating"));
                                branchDetails.setIsActive(jo2.getString("IsActive"));
                                branchDetails.setCreatedOn(jo2.getString("CreatedOn"));
                                branchDetails.setCreatedBy(jo2.getString("CreatedBy"));
                                branchDetails.setModifiedOn(jo2.getString("ModifiedOn"));
                                branchDetails.setModifiedBy(jo2.getString("ModifiedBy"));
                                branchDetails.setDeletedOn(jo2.getString("DeletedOn"));
                                branchDetails.setDeletedBy(jo2.getString("DeletedBy"));
                                branchDetails.setIsDeleted(jo2.getString("IsDeleted"));
                                branchDetails.setBranchList(jo2.getString("BranchList"));
                                branchDetails.setAmenityIds(jo2.getString("AmenityIds"));
                                branchDetails.setMessage(jo2.getString("message"));
                                branchDetails.setFlagId(jo2.getString("FlagId"));
                                BranchList.add(branchDetails);
                            }
//                            }


//                            if (jo.has("Id")) {
                            JSONArray ja2 = jo.getJSONArray("Amenities");
                            for (int i = 0; i < ja2.length(); i++) {
                                JSONObject jo3 = ja2.getJSONObject(i);
                                AmenitiesBooking amenitiesBooking = new AmenitiesBooking();
                                amenitiesBooking.setId(jo3.getString("Id"));
                                amenitiesBooking.setIconClass(jo3.getString("IconClass"));
                                amenitiesBooking.setUniCode(jo3.getString("UniCode"));
                                amenitiesBooking.setNameEn(jo3.getString("NameEn"));
                                amenitiesBooking.setNameAr(jo3.getString("NameAr"));

                                AnenitiesList.add(amenitiesBooking);
                            }
//                            }

                            int position = 0;

                            img.clear();
                            mediaid.clear();

                            for (int i = 0; i < BannersList.size(); i++) {
                                img.add(BannersList.get(i).getSourcePath());
                                mediaid.add(BannersList.get(i).getMediaTypId());
                            }

                            Log.i("TAG", "img   " + img.size());

                            viewPageAdapter = new ViewPageAdapter(BookingDetails.this, mediaid, img, position, BookingDetails.this, language);
                            viewPager.setAdapter(viewPageAdapter);

                            amenities_icon.clear();
                            amenities_name_ar.clear();
                            amenities_name_en.clear();

                            for (int i = 0; i < AnenitiesList.size(); i++) {

                                amenities_name_en.add(AnenitiesList.get(i).getNameEn());
                                amenities_name_ar.add(AnenitiesList.get(i).getNameAr());
                                amenities_icon.add(AnenitiesList.get(i).getUniCode());

                            }

                            amenitiesAdapter = new AmenitiesAdapter1(BookingDetails.this, amenities_name_en, amenities_name_ar, amenities_icon, language);
                            mamenities.setAdapter(amenitiesAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(BookingDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public void checkInDatePicker() {
        final Calendar c = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(BookingDetails.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (view.isShown()) {
                            mCheckInYear = year;
                            mCheckInDay = dayOfMonth;
                            mCheckInMonth = monthOfYear;

                            if (mCheckInDay < 10) {
                                tvCheckInDate.setText("0" + mCheckInDay);
                            } else {
                                tvCheckInDate.setText("" + mCheckInDay);
                            }
                            tvCheckInMonth.setText(MONTHS[mCheckInMonth]);
                            isCheckOutShown = false;
                            isCheckInSelected = true;
                            checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth + 1));

                            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.US);

                            String date = dayOfMonth + "." + (monthOfYear + 1) + "." + year;

                            Date date1 = null;

                            try {
                                date1 = df.parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            checkin = df.format(date1);

                            Log.i("TAG", "onDateSet: " + checkin);
                        }
                    }
                }, mCheckInYear, mCheckInMonth, mCheckInDay);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + max);
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        datePickerDialog.setCancelable(false);
        if (language.equalsIgnoreCase("En")) {
            datePickerDialog.setMessage("Select Check In Date");
        } else {
            datePickerDialog.setMessage("إختر تاريخ الدخول");
        }
        datePickerDialog.show();
    }

    public void checkOutDatePicker(final int year, final int date, final int month) {
        final Calendar c = Calendar.getInstance();
        long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
        String givenDateString = year + "-" + (month) + "-" + date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(givenDateString);
            mCheckInDateStr = sdf.format(mCheckInDate);
            selectedMillis = mCheckInDate.getTime();
            c.setTime(mCheckInDate); // yourdate is an object of type Date
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            tvCheckInWeek.setText(WEEKS[(dayOfWeek - 1)]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        c.add(Calendar.DATE, 1);
        selectedMillis = c.getTimeInMillis();
        mCheckOutYear = c.get(Calendar.YEAR);
        mCheckOutMonth = c.get(Calendar.MONTH);
        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(BookingDetails.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckOutYear = year;
                        mCheckOutDay = dayOfMonth;
                        mCheckOutMonth = monthOfYear;

                        if (mCheckOutDay < 10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay);
                        } else {
                            tvCheckOutDate.setText("" + mCheckOutDay);
                        }

                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);
                        isCheckInSelected = false;

                        String givenDateString = mCheckOutYear + "-" + (mCheckOutMonth + 1) + "-" + mCheckOutDay;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDate = sdf.parse(givenDateString);
                            mCheckOutDateStr = sdf.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek - 1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        CalculateNumOfDays();

                    }
                }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
        datePickerDialog.getDatePicker().setMinDate(selectedMillis);
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(selectedMillis + max);
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    // Do Stuff
                    if (isCheckInSelected) {
                        String givenDateString = year + "-" + (month) + "-" + date;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckInDate = sdf.parse(givenDateString);
                            mCheckInDateStr = sdf.format(mCheckInDate);
//                            selectedMillis = mCheckInDate.getTime();
                            c.setTime(mCheckInDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckInWeek.setText(WEEKS[(dayOfWeek - 1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        c.add(Calendar.DATE, 1);
//                        selectedMillis = c.getTimeInMillis();
                        mCheckOutYear = c.get(Calendar.YEAR);
                        mCheckOutMonth = c.get(Calendar.MONTH);
                        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

                        if (mCheckOutDay < 10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay);
                        } else {
                            tvCheckOutDate.setText("" + mCheckOutDay);
                        }

                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);
                        isCheckInSelected = false;

                        String givenDateString1 = mCheckOutYear + "-" + (mCheckOutMonth + 1) + "-" + mCheckOutDay;
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDate = sdf1.parse(givenDateString1);
                            mCheckOutDateStr = sdf1.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek - 1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        CalculateNumOfDays();
                    }
                    datePickerDialog.dismiss();
                }
            }
        });
        datePickerDialog.setInverseBackgroundForced(true);
        if (language.equalsIgnoreCase("En")) {
            datePickerDialog.setMessage("Select Check Out Date");
        } else {
            datePickerDialog.setMessage(" إختر تاريخ المغادرة");
        }
        if (!isCheckOutShown) {
            datePickerDialog.show();
            isCheckOutShown = true;
        }
    }

//    https:// [Hoteldomain.boudl.com/V8Client] /Inquiry.aspx?culture=en-KK&date=DD.MM.YYYY&nights=X&adults=Z
//            ?culture=en-KK&date=DD.MM.YYYY&nights=X&adults=Z
//    gb - for english
//    us - for arabic Cultures
//    date - Checkout date
//    nights - checkout date (number of days)

    public void CalculateNumOfDays() {
        long diff = mCheckOutDate.getTime() - mCheckInDate.getTime();
        NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
//        numNights.setText("" + NumofDaysStr);
        Log.i("TAG", "CalculateNumOfDays: " + NumofDaysStr);
    }

}
