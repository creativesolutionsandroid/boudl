package com.cs.boudl.activity;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.boudl.Constants;
import com.cs.boudl.MainActivity;
import com.cs.boudl.Model.News;
import com.cs.boudl.NetworkUtil;
import com.cs.boudl.NewsAdapter;
import com.cs.boudl.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by CS on 11-12-2017.
 */

public class NewsFragment extends AppCompatActivity {

    ImageView menu;
    ListView listView;
    NewsAdapter newsAdapter;
    Typeface lightTypeface, semiBoldTypeface, boldTypeface;

    String response;

    ArrayList<News> newsArrayList = new ArrayList<>();
    String[] titles = {"Luxury Hotel Award", "Braira Cordoba Branch is open", "Aber Hotel", "Narcissus Tower Riyadh", "Braira Al Nakhil ( Riyadh )", "Braira Hotel AL- Wezarat Branch Grand opening", "Braira Villas Grand Opening", "Narcissus Obhur Jeddah", "Braira Al Dammam ( 5 star ) is now open"};
    String[] desc = {"Narcissus Hotel & Residence Riyadh is the Winner of Luxury Hotel World Award in Middle East and Africa for the 4th year.\n",
            "Now Braira Hotel Cordoba branch is open to the public, Braira which is characterized by elegant design and modern furniture to suit all tastes, watch our amazing offers and discounts which are unbelievable  and enjoy the most beautiful nights in Braira Hotels and Resorts.\n",
            "The new branch of Aber hotels  in ( Sahafa) district isnow open to the  public. The hotels of Aber Brand are distinguished and simple in terms of design, concept and economical prices while ensuring high quality and professionalism in providing ser-vices to our guests. These concepts  are became very important  for most of travelers and for who are searching for new lifestyles. This series of hotels offers distinctive services at economical prices. The designs combine modernity with Arabic originality.\n",
            "We are pleased to announce very soon the official opening of the Narcissus Tower, adjacent to the Narcissus Hotel Riyadh on Tahlia Street. Narcissus has become a very common name in the world of hospitality, and for the past five years since its opening, it has proudly won four awards for international luxury. Hotel Narcissus is characterized by elegance and beauty in terms of exterior design, which inspires heritage and originality, in addition to the luxury of furnishings and facilities that dazzle the eyes of all who see it, in addition to many other services such as restaurants, gymnasiums, wedding halls, conferences halls, swimming pools and spa in the hotel and many other amenities.\n",
            "Braira Hotel & Resort will soon be opening in Al-Nakheel District in Riyadh. The most advantage of the hotels in Braira is the modern decoration, which tends to lux-ury and simplicity at the same time. The company has established many hotels and resorts throughout the Kingdom and is expanding to several major cities. Braira is the perfect place to spend a holiday with the family, where the guest will enjoy peace and luxury as well as complementary hotel services for unbeatable prices.\n",
            "Braira Hotel new branchis open to the  public in the ministries district in Riyadh, the hotel is located in a vital and distinctive location in Riyadh in the heart of the city. The hotel is the perfect choice for business and leisure trav-elers. The hotel is characterized by elegant and contemporary furniture that is very much to the luxury and has a beautiful color and comfortable design for the eyes. Also, the most outstanding of the hotels in Braira is the tranquility and good service with all the integrated services. You will certainly find everything you need for a wonderful stay .\n",
            "Braira Hotel & Villas is now open to the public in Hitin district- Riyadh City. This is the first time in the Kingdom that a hotel & Villa has been established, consisting of private villas comprising all services in an elegant style, furniture that is very luxurious and elegant. The resort is fully integrated in terms of services and entertainment. Each has a private swimming pool and glamour decorations.\n",
            "NarcissusObhurfive-star resort is now open to the public, which is located in the northern part of Jeddah. The resort combines luxury and relaxation with its world-class design for the apartment units. It is also the place for businessmen to meet in well-equipped meeting rooms.The diversity of restaurants is a combination of East and West charm, and the tranquility and beautiful sea views are a destination for visitors wishing to relax in luxury.\n",
            "Braira Al Dammam is now open to the public, it is the only hotel in braira hotels and resorts which is categorized as 5- Star Hotel. The hotel is located in a strategic location in Al Dammam city, it has the ideal frontage on the shore in Al Dammam, Also it is surrounded by the most important commercial centers and government facilities. Braira Al dammam Hotel provides their guests the most luxurious and comfortable stay, the hotel is known with its relaxing atmosphere and glamour decorations. it has many suites categories with legal setup provided specially for our special guests to make our guests pleased and relaxed.\n"};

//    int[] images = {R.drawable.news1, R.drawable.news2, R.drawable.news3, R.drawable.news4, R.drawable.news5, R.drawable.news6, R.drawable.news7, R.drawable.news8, R.drawable.news9};
    View rootView;

    String[] titles_Ar = {"جائزة الفخامة الفندقية العالمية", " إفتتاحفندقبريرافرعقرطبةبالرياض", " قريباً إفتتاح فندق عابر فرع الصحافة", "برج نارسيس الرياض", "بريرا النخيل\"  الرياض\"", "إفتتاح فندق بريرا حى الوزارات", " إفتتاح منتجع فيلات بريرا", " إفتتاح نارسيس أُبـــحر جدة", "إفتتاح فندق بريرا الدمام ( خمس نجوم )"};
    String[] desc_Ar = {"للسنة الرابعة على التوالى يحصد فندق نارسيس الرياض جائزة الفخامة الفندقية العالمية  لسنة ٢٠١٧ لكونه أفخم فندق من حيث التصميم المعمارى و الديكور الداخلى فى الشرق الأوسط و إفريقيا. \n",
            "تم إفتتاح فندق بريرا فرع قرطبة بالرياض, والذى يمتاز بالتصميم الأنيق والآثاث العصرى الذى يتناسب مع جميع الأذواق, ترقبوا عروض  وخصومات لا مثيل لها و إستمتعوا بأجمل الليالى فى فنادق ومنتجعات بريرا.\n",
            "تم إفتتاح الفرع الجديد لفندق عابر بحى الصحافة بالرياض , تمتاز فنادق عابر بطابع فخم وبسيط من حيث التصميموالمفهوم وباسعار اقتصادية مع ضمان الجودة العالية والأحترافية في تقديم الخدمات لضيوفنا الكرام . المفهوم الأقتصادي الذي نحرص على تقديمه في هذه المجموعة اصبح حاليا غاية ضرورية للعديد من المسافرين والباحثين عن التغير في نمط الحياة المعتاد . تتميز هذه السلسلة من الفنادق بتقديم خدمات مميزة بأسعار اقتصادية .حيث التصاميم تجمع بين الحداثة والأصالة العربية .\n",
            "يسعدنا أن نعلن قريباًً عن الأفتتاح الرسمى لبرج نارسيس و المجاور لفندق نارسيس الرياض بشارع التحلية, نارسيس أصبح أسم غنى عن التعريف فى عالم الضيافة, فطيلة خمس سنوات منذ الأفتتاح حصد وبكل فخر أربعة جوائز للفخامة العالمية. فندق نارسيس يمتاز بالرونق و الجمال من حيث التصميم الخارجى الذى يوحى بالعراقة و الأصالة , بالأضافة لفخامة المفروشات و التجهيزات الداخلية والتى تبهر أعين كل من يراها, بالأضافة لكثير من الخدمات الأخرى مثل المطاعم و الصالات الرياضية و قاعات الأفراح و المؤتمرات والمسابح و المنتجع الصحى داخل الفندق وغيرها الكثير من المزايا.\n",
            "قريباً سيتم إفتتاح فندق بريرا فرع حى النخيل بالرياض , من أكثر ما تمتاز به فنادق بريرا هو الديكور العصرى الذي يميل الى الفخامة والبساطة فى الوقت  ذاته, فقد أسست الشركة العديد من الفنادق والمنتجعات فى أنحاء المملكة وجارى التوسع لتشمل عدة مدن رئيسية. يُعد بريرا المكان الأمثل لقضاء عطلة مع العائلة  ففيه يسمتع النزيل بالهدوء والرفاهية بالأضافة الى خدمات فندقية متكاملة وبأسعار لا تقارن.\n",
            "تم إفتتاح فندق بريرا حى الوزارات بمدينة الرياض, فالفندق يقع فى موقع حيوي ومميز بالرياض فى قلب المدينة. يُعد الفندق هو الخيار الأمثل لرجال الأعمال و الباحثيين عن مكان للإقامة بجوار المؤسسات والهئيات. يمتاز الفندق بآثاث عصرى و أنيق يميل الى حد كبير الى الفخامة و يتمتع بتصاميم ذات ألوان خلابة و مريحة للأعين , أيضا من أكثر ما يميز فنادق بريرا هو الهدوء وحسن الخدمة الفندقية مع جميع الخدمات المتكاملة فهنالك بالتأكيد ستجد كل ما تحتاجه لقضاء إقامة أكثر من رائعة وبأسعار ممتازة.\n",
            "تم إفتتاح مجمع فيلات بريرا بالرياض , حى حطين , وهذه للمرة الأولى بالمملكة التى يتم فيها  إنشاء منتجع فندقى مكون من فيلات خاصة تشمل جميع الخدمات على طراز أنيق و مفروشات و آثاث غاية فى الفخامة والرونق, يُعد المنتجع متكامل من حيث الخدمات ووسائل الترفيه, فيشمل وحدات سكنية كل منها له حمام سباحة خاص.\n",
            "تم في ١ مايو افتتاح منتجع نارسس  في ابحر الشماليه  في مدينة جده والذي يصنف بأنه من فئة الخمس نجومجمع المنتجع بين الرفاهية والاسترخاء بتصميمه العالمي الفخم للوحدات السكينه كما انه المكان المناسب لرجال الاعمال لعقد اجتماعتهم في قاعة اجتماعات مجهزة ومنظمة .التنوع في المطاعم يشكل مزيج بين سحر الشرق والغرب كما أن الهدوء وإطلالة البحر الجميل تشكل وجهة للزائرين الراغبين في الاسترخاء برفاهية.\n",
            "تم إفتتاح فندق بريرا الدمام و هو أول فندق في مجموعة فنادق و منتجعات بريرا الذي يصنف بأنه فندق ٥ نجوم يقع فندق بريرا الدمام بموقع استراتيجي والمطل على الواجهة البحرية محاطاً بأهم مراكز التسوق التجارية والمراكز الحكومية ، يعد فندق بريرا الدمام هو الواجهة البحرية الأمثل لمدينة الدمام التي تعطيك جواً من الدفىء والراحة والهدوء ، يمتاز الفندق بغرف وأجنحة فاخرة مصممة بديكورات عصرية ساحرة وبإطلالات رائعة واكثر ما يميز الفندق الفخامة والهدوء لجعل إقامتك متكاملة بوجود جميع ما تحتاج لإقامة رائعة.\n"};

    SharedPreferences languagePrefs;
    String language;
    TextView header_title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.fragement_news);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.fragement_news_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        } else {
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }


        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);

        listView = (ListView) findViewById(R.id.listView);

        header_title = findViewById(R.id.title);

        if (language.equalsIgnoreCase("En")) {
            header_title.setText("News");
        } else if (language.equalsIgnoreCase("Ar")) {
            header_title.setText("أخبار");
        }
        menu = (ImageView) findViewById(R.id.menu);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(NewsFragment.this, NewsDescriptionActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    intent.putExtra("title", newsArrayList.get(i).getNameEn());
                } else if (language.equalsIgnoreCase("Ar")) {
                    intent.putExtra("title1", newsArrayList.get(i).getNameAr());
                }
                if (language.equalsIgnoreCase("En")) {
                    intent.putExtra("desc", newsArrayList.get(i).getDescEn());
                } else if (language.equalsIgnoreCase("Ar")) {
                    intent.putExtra("desc1", newsArrayList.get(i).getDescAr());
                }
                intent.putExtra("image", newsArrayList.get(i).getImagePath());
                startActivity(intent);
            }
        });

            new GetNews().execute();
    }

    public class GetNews extends AsyncTask<String, Integer, String> {

        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            newsArrayList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(NewsFragment.this);
//            dialog = ProgressDialog.show(NewsFragment.this, "",
//                    "Please wait..");
            dialog = new ProgressDialog(NewsFragment.this, ProgressDialog.THEME_HOLO_LIGHT);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Please wait");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.New_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
//                        StringEntity se = new StringEntity("", "UTF-8");
//
//                        // 6. set httpPost Entity
//                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(NewsFragment.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(NewsFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {
                                News news = new News();

                                JSONObject jo = ja.getJSONObject(i);

                                news.setId(jo.getString("Id"));
//                                news.setHotelId(jo.getString("HotelId"));
                                news.setSortIndex(jo.getString("SortIndex"));
                                news.setNameEn(jo.getString("NameEn"));
                                news.setNameAr(jo.getString("NameAr"));
                                news.setHotelNameEn(jo.getString("HotelNameEn"));
                                news.setHotelNameAr(jo.getString("HotelNameAr"));
                                news.setDescEn(jo.getString("DescEn")+"\n");
                                news.setDescAr(jo.getString("DescAr")+"\n");
                                news.setImagePath(jo.getString("ImagePath"));
                                news.setIsActive(jo.getString("isActive"));
                                news.setCreatedOn(jo.getString("CreatedOn"));
                                news.setCreatedBy(jo.getString("CreatedBy"));
                                news.setModifiedOn(jo.getString("ModifiedOn"));
                                news.setModifiedBy(jo.getString("ModifiedBy"));
                                news.setDeletedOn(jo.getString("DeletedOn"));
                                news.setDeletedBy(jo.getString("DeletedBy"));
                                news.setIsDeleted(jo.getString("isDeleted"));
                                news.setMessage(jo.getString("message"));
                                news.setFlagId(jo.getString("FlagId"));
                                news.setDatasetxml(jo.getString("datasetxml"));

                                newsArrayList.add(news);
                            }
                            Log.i("TAG", "size " + newsArrayList.size());

                            newsAdapter = new NewsAdapter(NewsFragment.this, newsArrayList, language);
                            listView.setAdapter(newsAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(NewsFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public void onBackPressed() {
        ActivityManager mngr = (ActivityManager) getSystemService( ACTIVITY_SERVICE );

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if(taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            startActivity(new Intent(NewsFragment.this, MainActivity.class));
            finish();
        }
        else{
            super.onBackPressed();
        }
    }

}
