package com.cs.boudl.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.boudl.R;

/**
 * Created by CS on 11-12-2017.
 */

public class SignUpActivity extends Activity {

    EditText etName, etEmail, etMobile, etPwd, etConfirmPwd;
    ImageView back_btn;
    RelativeLayout signIn;
    TextView signup;
    private static final int SIGNUP_REQUEST = 1;
    SharedPreferences languagePrefs;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_signup);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_signup_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        ((EditText) findViewById(R.id.etUserName)).setTypeface(lightTypeface);
        ((EditText) findViewById(R.id.etPassword)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.signupBtn)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.signinBtn)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.text1)).setTypeface(lightTypeface);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        signIn = (RelativeLayout) findViewById(R.id.signInbtn);
        signup = (TextView) findViewById(R.id.signinBtn);

        etName = (EditText) findViewById(R.id.etUserName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etPwd = (EditText) findViewById(R.id.etPassword);
        etConfirmPwd = (EditText) findViewById(R.id.etConfirmPassword);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mNameStr = etName.getText().toString();
                String mConfirm = etConfirmPwd.getText().toString();
                String mMobileStr = etMobile.getText().toString();
                String mEmailStr = etEmail.getText().toString();
                String mPasswordStr = etPwd.getText().toString();

                if(mNameStr.length() == 0){
                    if (language.equalsIgnoreCase("En")) {
                        etName.setError("Please enter Name");
                    }else if (language.equalsIgnoreCase("Ar")) {
                        etName.setError("من فضلك ارسل الاسم بالكامل");
                    }
                }
                else if(mEmailStr.length() == 0){
                    if (language.equalsIgnoreCase("En")) {
                        etEmail.setError("Please enter Email");
                    }else if (language.equalsIgnoreCase("Ar")) {
                        etEmail.setError("من فضلك ادخل البريد الالكتروني");
                    }
                }
                else if(!isValidEmail(mEmailStr)){
                    etEmail.setError("Please enter valid email");
                }
                else if(mMobileStr.length() == 0){
                    if (language.equalsIgnoreCase("En")) {
                        etMobile.setError("Please enter Mobile Number");
                    }else if (language.equalsIgnoreCase("Ar")) {
                        etMobile.setError("من فضلك أدخل رقم الجوال");
                    }
                }
                else if (mPasswordStr.length() == 0){
                    if (language.equalsIgnoreCase("En")) {
                        etPwd.setError("Please enter password");
                    }else if (language.equalsIgnoreCase("Ar")) {
                        etPwd.setError("من فضلك ادخل كلمة السر");
                    }
                }
                else if(mPasswordStr.length()<8){
                    etPwd.setError("Please enter valid password");
                }
                else if (mConfirm.length() == 0){
                    if (language.equalsIgnoreCase("En")) {
                        etConfirmPwd.setError("Please enter password");
                    }else if (language.equalsIgnoreCase("Ar")) {
                        etConfirmPwd.setError("من فضلك ادخل كلمة السر");
                    }
                }
                else if(!mConfirm.equals(mPasswordStr)){
                    if (language.equalsIgnoreCase("En")) {
                        etPwd.setError("Passwords not match, please retype");
                    }else  if (language.equalsIgnoreCase("Ar")) {
                        etPwd.setError("كلمة المرور غير صحيحة ، من فضلك أعد الإدخال");
                    }
                }
                else{
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
