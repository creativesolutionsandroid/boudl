package com.cs.boudl.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.boudl.Constants;
import com.cs.boudl.DropDownList;
import com.cs.boudl.GalleryFilterAdapter;
import com.cs.boudl.GalleryFooterAdapter;
import com.cs.boudl.GalleryRecyclerAdapter;
import com.cs.boudl.Model.BrandName;
import com.cs.boudl.Model.Cityname;
import com.cs.boudl.Model.Gallery;
import com.cs.boudl.Model.Hotalname;
import com.cs.boudl.NetworkUtil;
import com.cs.boudl.R;
import com.cs.boudl.RecyclerItemClickListener;
import com.cs.boudl.RecyclerViewClickListener;
import com.cs.boudl.ViewPageAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by CS on 20-01-2017.
 */
public class PhotoGalleryFragment extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    ImageView menu;
    View rootView;
    SharedPreferences languagePrefs;
    String language;
    TextView header_title;
    ImageView bigImage, dummyView1, dummyView2;
    ImageView image1, image2, image3, image4, image5, image6;
    RelativeLayout popup;
    Typeface lightTypeface, semiBoldTypeface, boldTypeface;
    ArrayList<Gallery> galleryArrayList = new ArrayList<>();
    ArrayList<Hotalname> hotalnames = new ArrayList<>();
    ArrayList<Gallery> galleryArrayList1 = new ArrayList<>();
    ArrayList<String> hotalname_en = new ArrayList<>();
    ArrayList<String> hotalname_ar = new ArrayList<>();
    public static String selectedHotel = "all";
    public static String selectedHotel1;

    public static RelativeLayout mlayout1;

    public static Boolean isHotelClicked = false;

    ArrayList<String> city_name_en = new ArrayList<>();
    ArrayList<String> city_name_ar = new ArrayList<>();
    ArrayList<String> brand_name_en1 = new ArrayList<>();
    ArrayList<String> city_name_ar1 = new ArrayList<>();
    ArrayList<String> city_name_en2 = new ArrayList<>();
    ArrayList<String> city_name_ar2 = new ArrayList<>();
    ArrayList<String> city_name_en3 = new ArrayList<>();
    ArrayList<String> city_name_ar3 = new ArrayList<>();
    ArrayList<String> city_name_en4 = new ArrayList<>();
    ArrayList<String> city_name_ar4 = new ArrayList<>();
    public static ArrayList<String> img = new ArrayList<>();
    ArrayList<String> img1 = new ArrayList<>();
    public static ArrayList<String> mediaid = new ArrayList<>();

    boolean boudl = false, barira = false, narcissus = false, aber = false;
    String response;
    public static GridView Gallery_view;
    //    Spinner mboudl_list, mbarira_list, mnarcissus_list, maber_list;
//    RelativeLayout mboudl_layout, mbarira_layout, mnarcissus_layout, maber_layout;
    DropDownList dropDownList;
    public static GalleryRecyclerAdapter galleryRecyclerAdapter;
    ViewPageAdapter viewPageAdapter;
    public static ViewPager viewPager;
    private LinearLayout mGallery;
    private LayoutInflater mInflater;
    private HorizontalScrollView horizontalScrollView;
    GalleryFooterAdapter galleryFooterAdapter;
    public static GalleryFilterAdapter galleryFilterAdapter;
    public static RecyclerView mfilter_view, mfilter_view_brand;
    ImageView myImage;
    FrameLayout imageframe;
    RelativeLayout layout;
    WebView youtubeFragment;
    RecyclerViewClickListener itemclick;
//    TextView mall, mboudl, mbarira, maber, mnarcissus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.fragement_gallery);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.fragement_gallery_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        } else {
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);

        selectedHotel = "all";
        selectedHotel1 = " ";
        hotalname_en.clear();
        hotalname_ar.clear();
        city_name_en.clear();
        city_name_ar.clear();
        brand_name_en1.clear();
        city_name_ar1.clear();
        city_name_en2.clear();
        city_name_ar2.clear();
        city_name_en3.clear();
        city_name_ar3.clear();
        city_name_en4.clear();
        city_name_ar4.clear();
        img.clear();
        mediaid.clear();

        header_title = findViewById(R.id.title);
        menu = (ImageView) findViewById(R.id.menu);


//        bigImage = (ImageView) findViewById(R.id.bigImage);
        dummyView1 = (ImageView) findViewById(R.id.dummyView1);
        dummyView2 = (ImageView) findViewById(R.id.dummyView2);
        popup = (RelativeLayout) findViewById(R.id.imagePopup);

        mlayout1 = findViewById(R.id.layout1);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);


        mfilter_view = (RecyclerView) findViewById(R.id.filter_view);
        mfilter_view_brand = (RecyclerView) findViewById(R.id.filter_view_brand);
//        mspinner_view = findViewById(R.id.city_spinner);
//        mspinner_view.setLayoutManager(layoutManager);
        mfilter_view.setLayoutManager(layoutManager1);
        mfilter_view_brand.setLayoutManager(layoutManager);
//
//        image1 = (ImageView) findViewById(R.id.image1);
//        image2 = (ImageView) findViewById(R.id.image2);
//        image3 = (ImageView) findViewById(R.id.image3);
//        image4 = (ImageView) findViewById(R.id.image4);
//        image5 = (ImageView) findViewById(R.id.image5);
//        image6 = (ImageView) findViewById(R.id.image6);

        Gallery_view = findViewById(R.id.gallery_view);
        viewPager = findViewById(R.id.pager);

//        LinearLayoutManager layoutManager= new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
//        gallery_footer = (RecyclerView) findViewById(R.id.gallery_footer);
//        gallery_footer.setLayoutManager(layoutManager);

        if (language.equalsIgnoreCase("En")) {
            header_title.setText("Photo Gallery");
        } else if (language.equalsIgnoreCase("Ar")) {
            header_title.setText("معرض الصور");
        }

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        dummyView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.setVisibility(View.GONE);
                viewPageAdapter = new ViewPageAdapter(PhotoGalleryFragment.this, mediaid, img, 0, PhotoGalleryFragment.this, language);
                viewPager.setAdapter(viewPageAdapter);
                viewPageAdapter.notifyDataSetChanged();
                viewPager.setVisibility(View.GONE);
            }
        });

        dummyView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.setVisibility(View.GONE);
                viewPageAdapter = new ViewPageAdapter(PhotoGalleryFragment.this, mediaid, img, 0, PhotoGalleryFragment.this, language);
                viewPager.setAdapter(viewPageAdapter);
                viewPageAdapter.notifyDataSetChanged();
                viewPager.setVisibility(View.GONE);
            }
        });


//
//        image1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bigImage.setImageResource(R.drawable.narcissus_image);
//                popup.setVisibility(View.VISIBLE);
//            }
//        });
//
//        image2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bigImage.setImageResource(R.drawable.braira_image);
//                popup.setVisibility(View.VISIBLE);
//            }
//        });
//
//        image3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bigImage.setImageResource(R.drawable.aber_image);
//                popup.setVisibility(View.VISIBLE);
//            }
//        });
//
//        image4.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bigImage.setImageResource(R.drawable.boudl_apart_image);
//                popup.setVisibility(View.VISIBLE);
//            }
//        });
//
//        image5.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bigImage.setImageResource(R.drawable.bur_image);
//                popup.setVisibility(View.VISIBLE);
//            }
//        });
//
//        image6.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bigImage.setImageResource(R.drawable.pampa_image);
//                popup.setVisibility(View.VISIBLE);
//            }
//        });

//        mInflater = LayoutInflater.from(this);

        Gallery_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view1, int position, long id) {

                viewPager.setVisibility(View.VISIBLE);
                popup.setVisibility(View.VISIBLE);

                viewPageAdapter = new ViewPageAdapter(PhotoGalleryFragment.this, mediaid, img, position, PhotoGalleryFragment.this, language);
                viewPager.setAdapter(viewPageAdapter);
                viewPager.setCurrentItem(position);
//                initView(position);

//                mGallery = (LinearLayout) findViewById(R.id.id_gallery);
//
//                for (int i = 0; i < galleryArrayList.size(); i++)
//                {
//                    View view = mInflater.inflate(R.layout.slider,
//                            mGallery, false);
//
//                    myImage = (ImageView) view
//                            .findViewById(R.id.image);
//                    imageframe = view.findViewById(R.id.image_frame);
//                    layout = view.findViewById(R.id.layout);
//                    youtubeFragment = view.findViewById(R.id.youtube_view);
//
//                    if (galleryArrayList.get(i).getMediatypeid().equals("1")) {
//
//                        youtubeFragment.setVisibility(View.GONE);
//                        imageframe.setVisibility(View.VISIBLE);
//
//                        DisplayMetrics displayMetrics = new DisplayMetrics();
//                        ((Activity) PhotoGalleryFragment.this).getWindowManager()
//                                .getDefaultDisplay()
//                                .getMetrics(displayMetrics);
//
//                        int height = displayMetrics.heightPixels;
//                        int width = displayMetrics.widthPixels;
//
//                        Log.d("TAG", "initView: "+width);
//
//                        myImage.setMinimumWidth(width);
//                        myImage.setMaxWidth(width);
//                        imageframe.setMinimumWidth(width);
//
//
//                        Glide.with(PhotoGalleryFragment.this).load(Constants.IMAGE_URL + galleryArrayList.get(i).getImagepath()).placeholder(R.drawable.boudl_logo).into(myImage);
//
//                    } else {
//
//                        DisplayMetrics displayMetrics = new DisplayMetrics();
//                        ((Activity) PhotoGalleryFragment.this).getWindowManager()
//                                .getDefaultDisplay()
//                                .getMetrics(displayMetrics);
//
//                        int height = displayMetrics.heightPixels;
//                        int width = displayMetrics.widthPixels;
//
//                        youtubeFragment.setVisibility(View.VISIBLE);
//                        imageframe.setVisibility(View.GONE);
//
//                        youtubeFragment.setMinimumWidth(width);
////
//                        youtubeFragment.getSettings().setJavaScriptEnabled(true);
//                        youtubeFragment.getSettings().setPluginState(WebSettings.PluginState.ON);
//                        youtubeFragment.setWebChromeClient(new WebChromeClient());
//                        youtubeFragment.getSettings().setMediaPlaybackRequiresUserGesture(false);
//
////            ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction().replace(R.id.youtube_view, myFragment).commit();
//
////            String playVideo = "<html><body style='margin:0px;padding:0px;'><script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>function onYouTubeIframeAPIReady(){ytplayer=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}function onPlayerReady(a){a.target.playVideo();}</script><iframe id='playerId' type='text/html' width='\(self.view.bounds.size.width-16)' height='250' src='http://www.youtube.com/embed/\(videoStr)' frameborder='0'></body></html>";
//
//
//                        String frameVideo = "<html><body style='margin:0px;padding:0px;'>\n" +
//                                "        <script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>\n" +
//                                "                var player;\n" +
//                                "        function onYouTubeIframeAPIReady()\n" +
//                                "        {player=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}\n" +
//                                "        function onPlayerReady(event){player.playVideo();}\n" +
//                                "        </script>\n" +
//                                "        <iframe id='playerId' type='text/html' width='100%' height='250'\n" +
//                                "        src='https://www.youtube.com/embed/" + galleryArrayList.get(i).getImagepath() + "?enablejsapi=1&autoplay=1' frameborder='0'>\n" +
//                                "        </body></html>";
//                        youtubeFragment.loadDataWithBaseURL("http://www.youtube.com", frameVideo, "text/html", "utf-8", null);
//                    }
//
//                    mGallery.addView(view);
//                }

            }
        });

        new GetGallery().execute();

    }

    private void initView(int position) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        Log.d("TAG", "onPageSelected: ");
//        ViewPageAdapter.showViewContent(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class GetGallery extends AsyncTask<String, Integer, String> {

        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            galleryArrayList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(PhotoGalleryFragment.this);
//            dialog = ProgressDialog.show(PhotoGalleryFragment.this, "",
//                    "Please wait..");
            dialog = new ProgressDialog(PhotoGalleryFragment.this, ProgressDialog.THEME_HOLO_LIGHT);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Please wait");
            dialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.GalleryURL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
//                        StringEntity se = new StringEntity(params[0], "UTF-8");
//
//                        // 6. set httpPost Entity
//                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(PhotoGalleryFragment.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(PhotoGalleryFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            img.clear();
                            mediaid.clear();
                            Log.i("TAG", "user response:" + result);
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {
                                Gallery gallery = new Gallery();

                                JSONObject jo = ja.getJSONObject(i);

                                gallery.setId(jo.getString("Id"));
                                gallery.setHotelid(jo.getString("HotelId"));
                                gallery.setBranchid(jo.getString("BranchId"));
                                gallery.setBranchnameEn(jo.getString("BranchNameEn"));
                                gallery.setBranchnameAr(jo.getString("BranchNameAr"));
                                gallery.setMediatypeid(jo.getString("MediaTypeId"));
                                gallery.setImagepath(jo.getString("ImagePath"));
                                gallery.setHotalnameEn(jo.getString("HotelNameEn"));
                                gallery.setHotalnameAr(jo.getString("HotelNameAr"));
                                gallery.setCityNameAr(jo.getString("CityNameAr"));
                                gallery.setCityNameEn(jo.getString("CityNameEn"));

                                galleryArrayList.add(gallery);
                            }

                            for (int i = 0; i < galleryArrayList.size(); i++) {

                                String all = null;
                                String all_ar = null;

                                all = "All";
                                all_ar = "الكل";
                                if (!hotalname_en.contains(all)) {
                                    hotalname_en.add(all);
                                    Hotalname hotalname = new Hotalname();
                                    hotalname.setHotalname_en(all);
                                    hotalname.setHotalname_ar(all_ar);

                                    hotalnames.add(hotalname);
                                }

                                if (!hotalname_en.contains(galleryArrayList.get(i).getHotalnameEn())) {
                                    Hotalname hotalname = new Hotalname();
                                    hotalname_en.add(galleryArrayList.get(i).getHotalnameEn());
                                    hotalname.setHotalname_en(galleryArrayList.get(i).getHotalnameEn());
                                    hotalname.setHotalname_ar(galleryArrayList.get(i).getHotalnameAr());

                                    ArrayList<Gallery> galleryAllList = new ArrayList<>();
                                    ArrayList<Cityname> citynames = new ArrayList<>();
                                    city_name_en.clear();
                                    galleryAllList.addAll(galleryArrayList);
                                    for (int j = 0; j < galleryAllList.size(); j++) {
                                        if (galleryArrayList.get(i).getHotalnameEn().contains(galleryAllList.get(j).getHotalnameEn())) {
                                            if (!city_name_en.contains(galleryAllList.get(j).getCityNameEn())) {
                                                Cityname cityname = new Cityname();
                                                city_name_en.add(galleryAllList.get(j).getCityNameEn());
                                                cityname.setCityname_en(galleryAllList.get(j).getCityNameEn());
                                                cityname.setCityname_ar(galleryAllList.get(j).getCityNameAr());

                                                ArrayList<Gallery> galleryAllList1 = new ArrayList<>();
                                                galleryAllList1.addAll(galleryArrayList);
                                                ArrayList<String> imagePaths = new ArrayList<>();
                                                ArrayList<String> mediaTypes = new ArrayList<>();
                                                for (int k = 0; k < galleryAllList1.size(); k++) {
                                                    if (galleryAllList.get(j).getHotalnameEn().equals(galleryAllList1.get(k).getHotalnameEn())) {
                                                        if (galleryAllList.get(j).getCityNameEn().equals(galleryAllList1.get(k).getCityNameEn())) {
                                                            if (!imagePaths.contains(galleryAllList1.get(k).getImagepath())) {
                                                                imagePaths.add(galleryAllList1.get(k).getImagepath());
                                                                mediaTypes.add(galleryAllList1.get(k).getMediatypeid());
                                                            }
                                                        }
                                                    }
                                                }

                                                ArrayList<Gallery> galleryAllList2 = new ArrayList<>();
                                                ArrayList<BrandName> brandNames = new ArrayList<>();
                                                brand_name_en1.clear();
                                                galleryAllList2.addAll(galleryArrayList);
                                                for (int l = 0; l < galleryAllList2.size(); l++) {
                                                    if (galleryAllList.get(j).getHotalnameEn().contains(galleryAllList2.get(l).getHotalnameEn())) {
                                                        if (galleryAllList.get(j).getCityNameEn().contains(galleryAllList2.get(l).getCityNameEn())) {
                                                            if (!brand_name_en1.contains(galleryAllList2.get(l).getBranchnameEn())) {
                                                                BrandName brandName = new BrandName();
                                                                brand_name_en1.add(galleryAllList2.get(l).getBranchnameEn());
                                                                brandName.setBrandname_en(galleryAllList2.get(l).getBranchnameEn());
                                                                brandName.setBrandname_ar(galleryAllList2.get(l).getBranchnameAr());

                                                                Log.i("TAG", "brand: " + brand_name_en1);

                                                                ArrayList<Gallery> galleryAllList3 = new ArrayList<>();
                                                                galleryAllList3.addAll(galleryArrayList);
                                                                ArrayList<String> imagePaths1 = new ArrayList<>();
                                                                ArrayList<String> mediaTypes1 = new ArrayList<>();
                                                                for (int m = 0; m < galleryAllList3.size(); m++) {
                                                                    if (galleryAllList2.get(l).getBranchnameEn().equals(galleryAllList3.get(m).getBranchnameEn())) {
                                                                        if (!imagePaths1.contains(galleryAllList3.get(m).getImagepath())) {
                                                                            imagePaths1.add(galleryAllList3.get(m).getImagepath());
                                                                            mediaTypes1.add(galleryAllList3.get(m).getMediatypeid());
                                                                        }
                                                                    }
                                                                }

                                                                Log.i("TAG", "image_branches " + imagePaths1.size());

                                                                brandName.setImagePath(imagePaths1);
                                                                brandName.setMediaTypeId(mediaTypes1);
                                                                brandNames.add(brandName);
                                                            }
                                                        }
                                                    }
                                                }

                                                cityname.setBrandnames(brandNames);
                                                cityname.setImagePath(imagePaths);
                                                cityname.setMediaTypeId(mediaTypes);
                                                citynames.add(cityname);
                                            }
                                        }
                                    }
                                    hotalname.setCitynames(citynames);
                                    hotalnames.add(hotalname);
                                }
                            }

                            galleryFooterAdapter = new GalleryFooterAdapter(PhotoGalleryFragment.this, itemclick, hotalnames, galleryArrayList, language);
                            mfilter_view.setAdapter(galleryFooterAdapter);

                            img.clear();
                            mediaid.clear();

                            for (int i = 0; i < galleryArrayList.size(); i++) {
                                img.add(galleryArrayList.get(i).getImagepath());
                                mediaid.add(galleryArrayList.get(i).getMediatypeid());
                            }

                            galleryRecyclerAdapter = new GalleryRecyclerAdapter(PhotoGalleryFragment.this, mediaid, img, language);
                            Gallery_view.setAdapter(galleryRecyclerAdapter);

                            mfilter_view.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {

                                    if (selectedHotel.equalsIgnoreCase("all")) {
                                        mlayout1.setVisibility(View.GONE);
                                    } else {
                                        mlayout1.setVisibility(View.VISIBLE);
                                        galleryFilterAdapter = new GalleryFilterAdapter(PhotoGalleryFragment.this, itemclick, position, hotalnames, galleryArrayList, language);
                                        mfilter_view_brand.setAdapter(galleryFilterAdapter);
                                    }

                                }
                            }));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else

            {
                Toast.makeText(PhotoGalleryFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null)

            {
                dialog.dismiss();
            }

            super.

                    onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public void onBackPressed() {
        if (viewPager.getVisibility() == View.VISIBLE) {

            viewPager.setVisibility(View.GONE);
            popup.setVisibility(View.GONE);

        } else {
            finish();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }
}
