package com.cs.boudl.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cs.boudl.R;

import im.delight.android.webview.AdvancedWebView;


/**
 * Created by CS on 06-07-2016.
 */
public class WebViewActivity extends AppCompatActivity implements AdvancedWebView.Listener {
    private ProgressBar mProgressBar;
    Toolbar toolbar;
    ImageView back_btn;
    TextView screenTitle;
    String title,title_Ar;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface;
    AdvancedWebView webview1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.webview);
        }else {
            setContentView(R.layout.webview_arabic);
        }

        screenTitle = (TextView) findViewById(R.id.title);


        if (language.equalsIgnoreCase("En")) {
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }else {
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);

        try {
            title = (getIntent().getExtras().getString("title"));
//            title_Ar=(getIntent().getExtras().getString("title_ar"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(title!=null && title.length()>0){
//            if (language.equalsIgnoreCase("En")) {
                screenTitle.setText(title);
//            }else {
//                screenTitle.setText(title_Ar);
//            }
        }

        back_btn = (ImageView) findViewById(R.id.back_btn);
        android.webkit.WebView wv = (android.webkit.WebView) findViewById(R.id.webView);
        webview1 =  findViewById(R.id.webView1);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
        mProgressBar.setVisibility(View.VISIBLE);

        if (getIntent().getBooleanExtra("booking",false) == true){

            wv.setVisibility(View.GONE);
            webview1.setVisibility(View.VISIBLE);
            webview1.setListener(WebViewActivity.this, (AdvancedWebView.Listener) WebViewActivity.this);
            webview1.loadUrl(getIntent().getExtras().getString("url"));
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    mProgressBar.setVisibility(View.GONE);
//                }
//            }, 4000);



        }else {

            wv.setVisibility(View.VISIBLE);
            webview1.setVisibility(View.GONE);

            wv.loadUrl(getIntent().getExtras().getString("url"));

            wv.setWebViewClient(new MyWebViewClient());
            wv.getSettings().setLoadsImagesAutomatically(true);
            wv.getSettings().setJavaScriptEnabled(true);
            wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        }


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageFinished(String url) {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {
            view.loadUrl(url);
            return true;
        }


        @Override
        public void onPageFinished(android.webkit.WebView view, String url) {
            mProgressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(android.webkit.WebView view, String url, Bitmap favicon) {
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }

}
