package com.cs.boudl.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.boudl.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

/**
 * Created by CS on 20-01-2017.
 */
public class ContactUsFragment extends AppCompatActivity implements OnMapReadyCallback {

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    private GoogleMap map;
    ImageView menu;
    View rootView;
    LinearLayout cfbtitle,ctwitter,cinstagram, mprivacy_policy;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface;

    String language;
    SharedPreferences languagePrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.more_activity);
        }else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.more_activity_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        cfbtitle=findViewById(R.id.more_fb);
        ctwitter=findViewById(R.id.more_twitter);
        cinstagram=findViewById(R.id.more_instagram);
        mprivacy_policy = findViewById(R.id.privacy_policy);

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);
        ((TextView) findViewById(R.id.fbtitle)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.twitterTitle)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.instagram_text)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.sel_address1)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.mobileText)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.emailText)).setTypeface(lightTypeface);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(ContactUsFragment.this);

        menu = (ImageView) findViewById(R.id.menu);


        mprivacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent fb=new Intent(ContactUsFragment.this, WebViewActivity.class);
                fb.putExtra("title","Privacy Policy");
                fb.putExtra("url", "http://www.boudlgroup.com/privacy.html");
                fb.putExtra("booking",false);
                startActivity(fb);

            }
        });

        cfbtitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent fb=new Intent(ContactUsFragment.this, WebViewActivity.class);
                fb.putExtra("title","Facebook");
                fb.putExtra("url", "https://www.facebook.com/Boudl-Group-197097914340728/");
                fb.putExtra("booking",false);
                startActivity(fb);

            }
        });

        ctwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent twitter=new Intent(ContactUsFragment.this, WebViewActivity.class);
                twitter.putExtra("title","Twitter");
                twitter.putExtra("url", "https://twitter.com/BoudlGroup");
                twitter.putExtra("booking",false);
                startActivity(twitter);

            }
        });

        cinstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent instagram=new Intent(ContactUsFragment.this, WebViewActivity.class);
                instagram.putExtra("title","Instagram");
                instagram.putExtra("url","https://www.instagram.com/boudlgroup/");
                instagram.putExtra("booking",false);
                startActivity(instagram);

            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((LinearLayout) findViewById(R.id.call)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: 920000555"));
                        if (ActivityCompat.checkSelfPermission(ContactUsFragment.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: 920000555"));
                    startActivity(intent);
                }
            }
        });

        ((LinearLayout) findViewById(R.id.email)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("plain/text");
                        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@boudl.com"});
                        i.putExtra(Intent.EXTRA_SUBJECT, "Boudl Feedback");
                        i.putExtra(Intent.EXTRA_TITLE  , "Boudl Feedback");
                        final PackageManager pm = getPackageManager();
                        final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                        String className = null;
                        for (final ResolveInfo info : matches) {
                            if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                                className = info.activityInfo.name;

                                if(className != null && !className.isEmpty()){
                                    break;
                                }
                            }
                        }
                        i.setClassName("com.google.android.gm", className);
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(ContactUsFragment.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(ContactUsFragment.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                    }
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ContactUsFragment.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +966920000555"));
                    if (ActivityCompat.checkSelfPermission(ContactUsFragment.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(ContactUsFragment.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng latLng = new LatLng(24.8106452,46.6452859);
        MarkerOptions markerOptions = new MarkerOptions();
        map.clear();
        // Show the current location in Google Map
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        // Zoom in the Google Map
        map.animateCamera(CameraUpdateFactory.zoomTo(12.0f));

        MarkerOptions marker = new MarkerOptions().position(new LatLng(24.8106452,46.6452859));

// Changing marker icon
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

// adding marker
        map.addMarker(marker);
    }
}
