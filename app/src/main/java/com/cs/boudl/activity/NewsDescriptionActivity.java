package com.cs.boudl.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cs.boudl.Constants;
import com.cs.boudl.R;
import com.cs.boudl.TextViewEx;

/**
 * Created by CS on 11-12-2017.
 */

public class NewsDescriptionActivity extends AppCompatActivity {

    ImageView back_btn, icon;
//    JustifyTextView text7;
    TextView title;
    String titleStr, descStr, title_ar, desc_ar, imageStr;
    TextView header_title;
    TextViewEx text8;
    WebView webView;

    String  language;
    SharedPreferences languagePrefs;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_news);
        }else{
            setContentView(R.layout.activity_new_arabic);
        }

        titleStr = getIntent().getStringExtra("title");
        title_ar = getIntent().getStringExtra("title1");
        descStr = getIntent().getStringExtra("desc");
        desc_ar = getIntent().getStringExtra("desc1");
        imageStr = getIntent().getStringExtra("image");

        Log.i("TAG", "onCreate: "+desc_ar);

        header_title = findViewById(R.id.title);


        if (language.equalsIgnoreCase("En")) {

            header_title.setText(" News ");

        }else if (language.equalsIgnoreCase("Ar")){

            header_title.setText(" أخبار ");

        }

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Regular.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Regular.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }


        webView = (WebView) findViewById(R.id.webView);

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);
        ((TextView) findViewById(R.id.title1)).setTypeface(lightTypeface);
        if (language.equalsIgnoreCase("En")) {
//            ((TextView) findViewById(R.id.desc)).setTypeface(lightTypeface);
        }else if (language.equalsIgnoreCase("Ar")) {
//            ((TextView) findViewById(R.id.desc2)).setTypeface(lightTypeface);
        }

        icon = (ImageView) findViewById(R.id.image);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        title = (TextView) findViewById(R.id.title1);

        if (language.equalsIgnoreCase("En")) {
//            text7 = (JustifyTextView) findViewById(R.id.desc);
        }else if (language.equalsIgnoreCase("Ar")) {
            text8 = findViewById(R.id.desc2);
        }

        if (language.equalsIgnoreCase("En")) {
            title.setText(titleStr);
        }else if (language.equalsIgnoreCase("Ar")) {
            title.setText(title_ar);
        }

        if(language.equalsIgnoreCase("En")) {
            webView.loadDataWithBaseURL(null, descStr, "text/html", "utf-8", null);
        }
        else{
            webView.loadDataWithBaseURL(null, desc_ar, "text/html", "utf-8", null);
        }

        Glide.with(NewsDescriptionActivity.this).load(Constants.IMAGE_URL + imageStr)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(icon);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
