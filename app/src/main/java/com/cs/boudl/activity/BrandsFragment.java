package com.cs.boudl.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.boudl.R;
import com.cs.boudl.TextViewEx;

import me.biubiubiu.justifytext.library.JustifyTextView;

/**
 * Created by CS on 20-01-2017.
 */
public class BrandsFragment extends AppCompatActivity {

    ImageView menu;
    JustifyTextView desc1, desc2, desc3, desc4, desc5, desc6;
    TextView website1, website2, website3, website4, website5, website6;
    TextViewEx desc_ar1, desc_ar2, desc_ar3, desc_ar4, desc_ar5, desc_ar6;

    Typeface lightTypeface, semiBoldTypeface, boldTypeface;
    String language;
    SharedPreferences languagePrefs;

    static Point size;
    static float density;

    //    String[] titles = {"Narcissus", "Braira", "Aber", "Boudl Apart", "Bur Bakery", "Pampa Grill"};
//    int[] logos = {R.drawable.narcissus_logo, R.drawable.braira_logo, R.drawable.aber_logo, R.drawable.boudl_apart_logo,
//            R.drawable.bur_logo, R.drawable.pampa_logo};
//    int[] images = {R.drawable.narcissus_image, R.drawable.braira_image, R.drawable.aber_image, R.drawable.boudl_apart_image,
//            R.drawable.bur_image, R.drawable.pampa_image};
    String[] desc = {"Narcissus Hotels were launched to meet the brilliant vision and history of “Boudl Group”. Narcissus Hotels reflect a great picture that combines International architecture and modern luxury. Narcissus hotels group consists of 3 hotels and one resort,all ranked 5 stars.\n" +
            "The future plan for Narcissus is to establish hotels at Makkah, Madinah, Khobar, and Aseer; all representing a cultural and majestic heritage.\n" +
            "Narcissus existing hotels are: “Narcissus” Riyadh, “Classic Tower” Riyadh will be open soon, “Narcissus Jeddah”, and “Narcissus” Abhor. Each room of Narcissus hotels has five star specifications to meet all the needs of the hotel guests whether, families, individual, or VIP guests.\n",
            "Braira Hotels & Resorts are regarded as among the most luxurious and innovative in the Kingdom of Saudi Arabia and have won The Excellence tourism award in 2018 for many considerations:\n" +
                    "- The luxury felt by guests in all hotel parts.\n" +
                    "- The enormous luxury that guests in all hotel parts.\n" +
                    "- Mixing the classical Arabic style known for its hospitality and authenticity along with modern luxury which is present in all hotel services.\n" +
                    "Moreover, the guest can enjoy the luxury of 6 current hotels in addition to 8 under construction hotels, vary from 4 stars to 5 stars Hotels & Resorts. That’s why and among all Boudl Group hotels, Braira Hotels ranks First among four star hotels that meet all families, businessmen, and individual’s needs. The fact is witnessed by all guests. The exquisite services and luxury make repeating the experience a future dream and goal for all guests.\n",
            "“Boudl Group” for hotels and resorts established the new chain of “Aber Hotels” that has been launched in 2016 to meet the needs of several segmentations of our valued guest, as we present a modern hotels throughout best price concept with the quality which we are always promise to guarantee to our valued guests. The budget concept becomes nowadays highly required for travelers and whom seeking for break to change the regular live style.\n" +
                    "The hotel chain is unique in providing special services with a budget rates and characterized by the modern and Arab genuineness designs. The Number of “Aber Hotels” has reached 2 current hotels located in Riyadh. and our strategic plan includes to spread out in entire the kingdom regions.\n",
            "Boudl Apart’ Hotels in Saudi Arabia and Kuwait are characterized by giving guests a sense of at home modern luxury while maintaining the deep rooted Saudi luxurious hospitality, for all suites consist of a separate kitchen, bedroom, sitting room, and a bathroom.\n" +
                    "At Boudl Hotel Apartment, you feel at home after a long hard working or touristic day.\n" +
                    "The hotel is managed by a more than 30 year experienced team.\n" +
                    "“Boudl” brand is spread through31 hotels in KSA and 2 in Kuwait; while Boudl group is planning to spread all over the Gulf area.\n",
            "Bur brand is owned by Boudl Hotels & Resorts, it combines the bakery and cafe, offering its products and services to the customers through a professional team who has extensive experience in the field. The bakery is equipped with the latest ovens, to provide the customers with the finest products in addition to the international and Arabic coffee, prepared by highly skilled staff.\n" +
                    "BUR priority is to satisfy the customer’s needs, and to work on continue improvements to achieve the highest quality standards. Boudl’s strategy is to expand Bur bakery to reach 96 branches in KSA and abroad in line with the kingdom vision 2030. Recently Bur cafe has only two branches in Riyadh.\n",
            "The Argentinean cuisine that offers different flavors for different tastes. You will have the chance to savor unique luscious dishes, meat, and desserts.\n" +
                    "“Pampa Grill” is a chain restaurant offering a serial blend of European and Latin American products. it was established in Riyadh in 2013 at Narcissus Hotel which is owned by Boudl Group. Well-known for its Argentinean style, Pampa Grill skyrocketed for its exquisite taste, excellent service, and high quality.\n" +
                    "Pampa grill 2 branches are in Narcissus Hotels.\n"};

//    String[] desc_ar = {"تأتي فنادق «نارسس » تلبية لإحتياج ضروري ورؤية ذكية تفرضها طبيعة وتاريخ بودل، حيث تعتبر فنادق «نارسس » الصورة الرائعة التي تجمع بين تاريخ المعمار العالمي وبين الرفاهية الحديثة في أبهى صورها. تضم مجموعة فنادق «نارسس » عدد 3 فنادق ومنتجع جميعها من فئة الخمس نجوم كما تضم المرحلة المستقبلية لنارسس فنادق في مدينة مكة المكرمة و المدينة المنورة و الخبر و عسير، كل فندق فيها يمثل واجهة حضارية راقية لتراث عريق . فنادق «نارسس » هي: “نارسس الرياض”، و “كلاسك تاور الرياض”، و “نارسس أبحر ». تحتوى غرف كل فندق من فنادق نارسس على مميزات خاصة كفندق خمس نجوم، ولكن في مجموعها او عمومها تتنوع هذه الغرف بما يتناسب مع احتياجات كافة ضيوف الفنادق من افراد وعائلات وشخصيات هامة .\n",
//            "يُعد بريرا ذو تصنيف الأربع نجوم واحداً من أهم وأشهر العلامات التجارية في مجال السياحة والفنادق لعدة اعتبارات أهمها؛ الفخامة التي يشعر بها الضيوف في كافة أرجاء هذه الفنادق، و الرفاهية الهائلة التي يمكن للضيوف أن ينعموا بها، أضف إلى ذلك قدرة الإدارة على مزج الطابع الكلاسيكي العربي المعروف بالضيافة والأصالة مع وسائل الرفاهية الحديثة التي تتمثّل في الخدمات ولإمكانيات الموجودة بالفندق.\n كما يمكن للضيف بأن يستمتع بالرفاهية في 8 فنادق قائمة حالياً إضافة إلى 8 أخرى تحت الإنشاء. لهذا من بين كل فنادق مجموعة بودل استطاعت فنادق بريرا أن تحتل المرتبة الأولى من بين الفنادق ذات فئة الأربع نجوم كمقصد للعائلات ورجال الأعمال والأفراد. وهذه حقيقة يشهد بها كل من أقام بهذه الفنادق؛ كما أن الرفاهية والخدمات الرائعة تجعل تكرار الزيارة والإقامة هدفاً مستقبلياً \n ",
//            "أنشأت شركه «بودل » للفنادق والمنتجعات سلسلة «فنادق عابر » و اطلقت علامتها التجاريه في عام 2016 لتلبية احتياجات العديد من شرائح المجتمع التي نتشرف بمخاطبتها من خلال مجموعه فنادق حديثه من حيث التصميم والمفهوم وباسعار اقتصاديه مع ضمان الجوده العاليه والاحترافية في تقديم الخدمات لضيوفنا الكرام. المفهوم الاقتصادي الذي نحرص علي تقديمه في هذه المجموعه اصبح حاليا غايه ضروريه للعديد من المسافرين والباحثين عن التغير في نمط الحياه المعتاد.\nتتميز هذه السلسلة من الفنادق بتقديم خدمات مميزه باسعار اقتصاديه ، حيث أن التصاميم تجمع بين الحداثة والاصالة العربية . تم انشاء عدد 6 فنادق من سلسلة فنادق عابر في كل من الرياض والقصيم والمنطقه الجنوبية والمنطقة الشرقية وخطتنا الاستراتيجية تتضمن أن تنتشر هذه المجموعه في كل انحاء المملكة\n",
//            "تعد «بودل » للشقق الفندقية من الفنادق المميزة في السعودية والكويت بطابع الأجنحة الفندقية المتكاملة التي تعطي الضيوف الإحساس برفاهية المنزل العصري الحديث مع الحفاظ على الطابع السعودي المحافظ بتراثه وتقاليده العريقة. يستمتع الضيوف في الفندق بحفاوة وكرم ضيافة سعودية مشهود لها بجانب الرفاهية الكاملة. فجميع غرف الفنادق عبارة عن أجنحة فندقية متكاملة تحوي المطبخ المستقل وغرفة النوم ومكان الجلوس بالإضافة للحمام. في شقق بودل الفندقية يشعر الضيف كأنه في بيت مريح بعد عناء يوم شاق من العمل أو السياحة. الفندق يقوم على إدارته فريق عمل بخبرة تتجاوز 30 عام\nوأصبحت هذه العلامة التجارية منتشرة بأكثر من 35 فندقاً داخل المملكة و 2 في دولة الكويت كما ان شركة بودل للفنادق والمنتجعات ترسم خطة توسعية لتكون منتشرة بدول الخليج كاملة\n",
//            "بر علامة تجارية تمتلكها شركة بودل للفنادق والمنتجعات حيث تدمج بين المخبز والمقهى وتقدم ذلك لعملائها من خلال فريق عمل متميز يمتلك الخبرة الواسعة في هذا المجال . قمنا بتجهيز المخابز بأحدث المعدات والأفران لتلبية جميع احتياجات العملاء.نقدم أجود أنواع القهوة العالميه بالإضافة إلى القهوة العربية ويتم إعدادها بأيدي ماهرة متخصصة في صنع القهوة \nوضعت بُر نصب عينيها إرضاء جميع عملائها وتلبية جميع احتياجاتهم ومتطلباتهم كما تعدهم بالتطوير والتحسين المستمر وتحقيق اعلى معايير الجودة . شركة بودل للفنادق والمنتجعات وضعت في خطتها الاستراتيجية التوسع والانتشار لعلامة بر التجارية ليصل عدد فروعها ٩٦ فرعاً داخل وخارج المملكة وذلك توافقاً مع رؤية المملكة العربية السعودية ٢٠٣٠ م\n",
//            "تقدم سلسلة «بامبا جريل » بين يديك كافة الأذواق والنكهات المختلفة. سيكون لديك الفرصة لتذوّق أطباق مختلفة و فريدة من نوعها من مختلف الأطباق واللحوم والحلويات\n بامبا جريل »، هي سلسلة مطاعم أرجنتينية تقدم المأكولات الأرجنتينية تأسست في عام 2013 الميلادية في مدينة الرياض السعودية بفندق «نارسس » الرياض وتعود ملكيتها لمجموعة بودل للفنادق والمنتجعات واشتهرت بطابعها الأرجنتيني حيث لاقت رواجاً وقبولاً كبير بسبب مذاقها المميّز بالإضافة إلى الخدمة الممتازة والجودة العالية " +
//                    "\nو يصل عدد فروعها حالياً إلى 4 فروع متواجدة في جميع فنادق «نارسس » فئة الخمس نجوم"};

    String[] desc_ar ={"تأتي فنادق «نارسس » تلبية لإحتياج ضروري ورؤية ذكية تفرضها طبيعة وتاريخ بودل، حيث تعتبر فنادق «نارسس » الصورة الرائعة التي تجمع بين تاريخ المعمار العالمي وبين الرفاهية الحديثة في أبهى صورها. تضم مجموعة فنادق «نارسس » عدد 4  فنادق ومنتجع جميعها من فئة الخمس نجوم كما تضم المرحلة المستقبلية لنارسس فنادق في مدينة مكة المكرمة و المدينة المنورة و الخبر و عسير، كل فندق فيها يمثل واجهة حضارية راقية لتراث عريق . فنادق «نارسس » هي: “نارسس الرياض”، و “كلاسك تاور الرياض”الذي سيتم افتتاحه قريباً، و “نارسس أبحر » بمدينة جدة. تحتوى غرف كل فندق من فنادق نارسس على مميزات خاصة كفندق خمس نجوم، ولكن في مجموعها او عمومها تتنوع هذه الغرف بما يتناسب مع احتياجات كافة ضيوف الفنادق من افراد وعائلات وشخصيات هامة .",
            "تعد فنادق و منتجعات الآن واحدة من أهم الأيقونات في عالم الفنادق حيث لاقت رواجاً واسعاً في السنوات الأخيرة داخل المملكة العربية السعودية و اتسعت شهرتها عالمياً لتكون مقصداً للقادمين من مختلف أنحاء العالم, حيث أنها حازت مؤخراً على جائزة التميز السياحي لعام ٢٠١٨ وذلك لعدة اعتبارات أھمھا؛\n" +
                    "- الفخامة التي یشعر بھا الضیوف في كافة أرجاء ھذه الفنادق.\n" +
                    "- الرفاھیة الھائلة التي یمكن للضیوف أن ینعموا بھا.\n" +
                    "- أضف إلى ذلك قدرة الإدارة على مزج الطابع الكلاسیكي العربي المعروف بالضیافة والأصالة مع وسائل الرفاھیة الحدیثة التي تتمثل في الخدمات والإمكانیات الموجودة بالفنادق.\n" +
                    "كما یمكن للضیف بأن یستمتع برفاهية بريرا في ٦  فنادق قائمة حالیًا إضافة إلى 8 أخرى تحت الإنشاء. لھذا من بین كل فنادق مجموعة بودل استطاعت سلسلة فنادق بریرا أن تحتل المرتبة الأولى من بین الفنادق ذات فئة الأربع  و الخمس نجوم  كمقصد للعائلات ورجال الأعمال والأفراد. وھذه حقیقة یشھد بھا كل من أقام بھذه الفنادق؛ كما أن وسائل الرفاھیة والخدمات المميزة تجعل تكرار الزیارة والإقامة ھدفا مستقبلیاً.\n",
            "أنشأت شركه «بودل » للفنادق والمنتجعات سلسلة «فنادق عابر » و اطلقت علامتها التجاريه في عام 2016 لتلبية احتياجات العديد من شرائح المجتمع التي نتشرف بمخاطبتها من خلال مجموعه فنادق حديثة من حيث التصميم والمفهوم وباسعار اقتصادية مع ضمان الجوده العالية والاحترافية في تقديم الخدمات لضيوفنا الكرام. المفهوم الاقتصادي الذي نحرص علي تقديمه في هذه المجموعةأصبح حالياً غايه ضرورية للعديد من المسافرين والباحثين عن التغير في نمط الحياه المعتاد.\n" +
                    "تتميز هذه السلسلة من الفنادق بتقديم خدمات مميزه باسعار اقتصادية، حيث أن التصاميم تجمع بين الحداثة والاصالة العربية. تم إنشاء فندقين من سلسلة فنادق عابر في الرياض وخطتنا الاستراتيجية تتضمن أن تنتشر هذه السلسلة في جميعأنحاء المملكة.\n",
            "تعد «بودل » للشقق الفندقية من الفنادق المميزة في السعودية والكويت بطابع الأجنحة الفندقية المتكاملة التي تعطي الضيوف الإحساس برفاهية المنزل العصري الحديث مع الحفاظ على الطابع السعودي المحافظ بتراثه وتقاليده العريقة. يستمتع الضيوف في الفندق بحفاوة وكرم ضيافة سعودية مشهود لها بجانب الرفاهية الكاملة. فجميع غرف الفنادق عبارة عن أجنحة فندقية متكاملة تحوي المطبخ المستقل وغرفة النوم ومكان الجلوس بالإضافة للحمام. في شقق بودل الفندقية يشعر الضيف كأنه في بيت مريح بعد عناء يوم شاق من العمل أو السياحة. الفندق يقوم على إدارته فريق عمل بخبرة تتجاوز 30 عام.\n" +
                    "وأصبحت هذه العلامة التجارية منتشرة في ٣١  فندقاً داخل المملكة و 2 في دولة الكويت كما ان شركة بودل للفنادق والمنتجعات ترسم خطة توسعية لتكون منتشرة بدول الخليج كاملة.\n",
            "بر علامة تجارية تمتلكها شركة بودل للفنادق والمنتجعات حيث تدمج بين المخبز والمقهى وتقدم ذلك لعملائها من خلال فريق عمل متميز يمتلك الخبرة الواسعة في هذا المجال . قمنا بتجهيز المخابز بأحدث المعدات والأفران لتلبية جميع احتياجات العملاء.نقدم أجود أنواع القهوة العالمية بالإضافة إلى القهوة العربية ويتم إعدادها بأيدي ماهرة متخصصة في صنع القهوة .\n" +
                    "وضعت بُر نصب عينيها إرضاء جميع عملائها وتلبية جميع احتياجاتهم ومتطلباتهم كما تعدهم بالتطوير والتحسين المستمر وتحقيق أعلى معايير الجودة . اللآن سلسلة بر متواجدة بمقرين فقط في الرياض وقد وضعتشركة بودل للفنادق والمنتجعات في خطتها الاستراتيجية التوسع والانتشار لعلامة بر التجارية ليصل عدد فروعها ٩٦ فرعاً داخل وخارج المملكة وذلك توافقاً مع رؤية المملكة العربية السعودية ٢٠٣٠ م.\n",
            "تقدم سلسلة «بامبا جريل » بين يديك كافة الأذواق والنكهات المختلفة. سيكون لديك الفرصة لتذوّق أطباق مختلفة و فريدة من نوعها من مختلف الأطباق واللحوم والحلويات.\n" +
                    "«بامبا جريل »، هي سلسلة مطاعم أرجنتينية تقدم المأكولات الأرجنتينية تأسست في عام 2013 الميلادية في مدينة الرياض السعودية بفندق «نارسس » الرياض وتعود ملكيتها لمجموعة بودل للفنادق والمنتجعات واشتهرت بطابعها الأرجنتيني حيث لاقت رواجاً وقبولاً كبير بسبب مذاقها المميّز بالإضافة إلى الخدمة الممتازة والجودة العالية.\n" +
                    "و يصل عدد فروعها حالياً إلى ٢ فروع متواجدة في فنادق «نارسس » فئة الخمس نجوم.\n"};

//    String[] websites = {"http://www.narcissusriyadh.com/en/default.html", "http://www.brairahotels.com/",
//            "http://www.aberhotels.com/", "http://www.boudl.com/", "", ""};

    //    View rootView;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.fragment_brands);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.fragment_brands_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        } else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        Display display = getWindowManager().getDefaultDisplay();
        size = new Point();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        density = dm.density;
        display.getSize(size);

        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);

        if (language.equalsIgnoreCase("En")) {
            ((TextView) findViewById(R.id.brandDesc)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc1)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc2)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc3)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc4)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc5)).setTypeface(lightTypeface);
        } else {
            ((TextView) findViewById(R.id.brandDesc_ar)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc1_ar)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc2_ar)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc3_ar)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc4_ar)).setTypeface(lightTypeface);
            ((TextView) findViewById(R.id.brandDesc5_ar)).setTypeface(lightTypeface);
        }

        ((TextView) findViewById(R.id.brandTitle)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.brandTitle1)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.brandTitle2)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.brandTitle3)).setTypeface(semiBoldTypeface);

        ((TextView) findViewById(R.id.brandWebsite)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.brandWebsite1)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.brandWebsite2)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.brandWebsite3)).setTypeface(semiBoldTypeface);

        if (language.equalsIgnoreCase("En")) {
            desc1 = (JustifyTextView) findViewById(R.id.brandDesc);
            desc2 = (JustifyTextView) findViewById(R.id.brandDesc1);
            desc3 = (JustifyTextView) findViewById(R.id.brandDesc2);
            desc4 = (JustifyTextView) findViewById(R.id.brandDesc3);
            desc5 = (JustifyTextView) findViewById(R.id.brandDesc4);
            desc6 = (JustifyTextView) findViewById(R.id.brandDesc5);
        } else if (language.equalsIgnoreCase("Ar")) {
            desc_ar1 = findViewById(R.id.brandDesc_ar);
            desc_ar2 = findViewById(R.id.brandDesc1_ar);
            desc_ar3 = findViewById(R.id.brandDesc2_ar);
            desc_ar4 = findViewById(R.id.brandDesc3_ar);
            desc_ar5 = findViewById(R.id.brandDesc4_ar);
            desc_ar6 = findViewById(R.id.brandDesc5_ar);
        }

        website1 = (TextView) findViewById(R.id.brandWebsite);
        website2 = (TextView) findViewById(R.id.brandWebsite1);
        website3 = (TextView) findViewById(R.id.brandWebsite2);
        website4 = (TextView) findViewById(R.id.brandWebsite3);

        menu = (ImageView) findViewById(R.id.menu);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (language.equalsIgnoreCase("En")) {
            desc1.setText(desc[0]);
            desc2.setText(desc[1]);
            desc3.setText(desc[2]);
            desc4.setText(desc[3]);
            desc5.setText(desc[4]);
            desc6.setText(desc[5]);
        } else {
            desc_ar1.setText(desc_ar[0], true);
            desc_ar2.setText(desc_ar[1], true);
            desc_ar3.setText(desc_ar[2], true);
            desc_ar4.setText(desc_ar[3], true);
            desc_ar5.setText(desc_ar[4], true);
            desc_ar6.setText(desc_ar[5], true);
        }

//        desc_ar1.setLineSpacing(1f, 1.2f);
//        desc_ar1.setTextSize(10* BrandsFragment.density);


        website1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent AboutIntent = new Intent(BrandsFragment.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    AboutIntent.putExtra("title", "Narcissus");
                } else {
                    AboutIntent.putExtra("title", "نارسس");
                }
                AboutIntent.putExtra("booking",false);
                AboutIntent.putExtra("url", "http://www.narcissusriyadh.com/en/default.html");
                startActivity(AboutIntent);
            }
        });

        website2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent AboutIntent = new Intent(BrandsFragment.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    AboutIntent.putExtra("title", "Braira");
                } else {
                    AboutIntent.putExtra("title", "بريرا");
                }
                AboutIntent.putExtra("booking",false);
                AboutIntent.putExtra("url", "http://www.brairahotels.com/");
                startActivity(AboutIntent);
            }
        });

        website3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent AboutIntent = new Intent(BrandsFragment.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    AboutIntent.putExtra("title", "Aber");
                } else {
                    AboutIntent.putExtra("title", "عابر");
                }
                AboutIntent.putExtra("booking",false);
                AboutIntent.putExtra("url", "http://aberhotels.com/");
                startActivity(AboutIntent);
            }
        });

        website4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent AboutIntent = new Intent(BrandsFragment.this, WebViewActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    AboutIntent.putExtra("title", "Boudl 'Apart Hotels");
                } else {
                    AboutIntent.putExtra("title", "بودلللشققالفندقية");
                }
                AboutIntent.putExtra("booking",false);
                AboutIntent.putExtra("url", "http://www.boudl.com/");
                startActivity(AboutIntent);
            }
        });


//        mBrandsListView = (ListView) findViewById(R.id.BrandslistView);
//        mAdapter = new BrandsAdapter(getActivity(), titles, logos, images, desc, websites);
//        mBrandsListView.setAdapter(mAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
