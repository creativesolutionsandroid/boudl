package com.cs.boudl.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.boudl.R;

/**
 * Created by CS on 11-12-2017.
 */

public class SignInActivity extends Activity {

    ImageView back_btn;
    EditText etName, etPassword;
    RelativeLayout signIn;
    TextView signup;
    SharedPreferences userPrefs;
    SharedPreferences.Editor  userPrefEditor;
    private static final int SIGNUP_REQUEST = 1;
    String  language;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface;
    SharedPreferences languagePrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS",Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_login);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_login_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Regular.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        ((EditText) findViewById(R.id.etUserName)).setTypeface(lightTypeface);
        ((EditText) findViewById(R.id.etPassword)).setTypeface(lightTypeface);
        ((TextView) findViewById(R.id.signin)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.signupBtn)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.forgotPassword)).setTypeface(semiBoldTypeface);
        ((TextView) findViewById(R.id.text1)).setTypeface(lightTypeface);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        signIn = (RelativeLayout) findViewById(R.id.signInbtn);
        signup = (TextView) findViewById(R.id.signupBtn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPrefEditor.putString("login_status","login");
                userPrefEditor.commit();
                setResult(RESULT_OK);
                finish();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(SignInActivity.this, SignUpActivity.class), SIGNUP_REQUEST);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SIGNUP_REQUEST && resultCode == RESULT_OK){
            userPrefEditor.putString("login_status","login");
            userPrefEditor.commit();
            setResult(RESULT_OK);
            finish();
        }
    }
}
