package com.cs.boudl.activity;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.boudl.Constants;
import com.cs.boudl.MainActivity;
import com.cs.boudl.Model.Notification;
import com.cs.boudl.NetworkUtil;
import com.cs.boudl.NotificationAdapter;
import com.cs.boudl.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CS on 20-01-2017.
 */
public class NotificationFragment extends AppCompatActivity{

    ImageView menu;
    View rootView;
    String  language;
    SharedPreferences languagePrefs;
    TextView headertitle;
    Typeface lightTypeface,semiBoldTypeface,boldTypeface;
    NotificationAdapter mAdapter;
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
        setContentView(R.layout.fragement_notification);
        }else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.fragement_notification_arabic);
        }

        if (language.equalsIgnoreCase("En")) {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Light.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "OpenSans-Bold.ttf");
        }else {
            lightTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Light.ttf");
            semiBoldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Semibold.ttf");
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "Cairo-Bold.ttf");
        }

        mListView = (ListView) findViewById(R.id.notificationsList);
//        ((TextView) findViewById(R.id.title)).setTypeface(boldTypeface);
//        ((TextView) findViewById(R.id.notificationMsg)).setTypeface(lightTypeface);
//        ((TextView) findViewById(R.id.notificationTitle)).setTypeface(semiBoldTypeface);

        headertitle = findViewById(R.id.title);

        if (language.equalsIgnoreCase("En")) {
            headertitle.setText("Notification");
        }else if (language.equalsIgnoreCase("Ar")) {
            headertitle.setText("إشعار");
        }

        menu = (ImageView) findViewById(R.id.menu);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        new GetNotifications().execute();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public class GetNotifications extends AsyncTask<String, Integer, String> {

        String response;
        String networkStatus;
        ProgressDialog dialog;
        InputStream inputStream = null;
        ArrayList<Notification> notificationsList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(NotificationFragment.this);
//            dialog = ProgressDialog.show(NewsFragment.this, "",
//                    "Please wait..");
            dialog = new ProgressDialog(NotificationFragment.this, ProgressDialog.THEME_HOLO_LIGHT);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Please wait");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.Notifications_URL);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(NotificationFragment.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(NotificationFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            Log.i("TAG",""+result);
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {

                                JSONObject jo = ja.getJSONObject(i);
                                Notification notification = new Notification();
                                if(language.equalsIgnoreCase("En")) {
                                    notification.setMessage(jo.getString("MessageEn"));
                                }
                                else{
                                    notification.setMessage(jo.getString("MessageAr"));
                                }
                                notification.setRedirectURL(jo.getString("RedirectURL"));
                                notificationsList.add(notification);
                            }

                            mAdapter = new NotificationAdapter(NotificationFragment.this, notificationsList, language);
                            mListView.setAdapter(mAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(NotificationFragment.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public void onBackPressed() {
        ActivityManager mngr = (ActivityManager) getSystemService( ACTIVITY_SERVICE );

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if(taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            startActivity(new Intent(NotificationFragment.this, MainActivity.class));
            finish();
        }
        else{
            super.onBackPressed();
        }
    }
}
