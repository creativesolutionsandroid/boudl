package com.cs.boudl;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DrawerListAdapter extends ArrayAdapter<String> {

    Context context;
    int layoutResourceId;
    private String[] data;
    int checkedPosition;
    SharedPreferences userPrefs,languagePrefs;
    SharedPreferences.Editor userPrefEditor,languagePrefsEditor;
    String language;

    private int[] unselected_icons;

    public DrawerListAdapter(Context context, int layoutResourceId,
                             String[] data,int[] unselected_icons, int checkedPosition,String language) {

        super(context, layoutResourceId, data);
        this.data = data;
        this.unselected_icons = unselected_icons;
        this.layoutResourceId = layoutResourceId;
        this.checkedPosition = checkedPosition;
        this.language=language;
        Log.i("TAG", "checkedPosition "+checkedPosition);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        if (row == null) {

            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();

            Typeface lightTypeface = Typeface.createFromAsset(context.getAssets(),
                    "OpenSans-Regular.ttf");

            ((TextView) row.findViewById(R.id.sidemenu_name)).setTypeface(lightTypeface);

            holder.txtTitle = (TextView) row.findViewById(R.id.sidemenu_name);
            holder.imgIcon = (ImageView) row.findViewById(R.id.sidemenu_icon);
            holder.dlanguage = row.findViewById(R.id.language);
//            holder.language_arabic = row.findViewById(R.id.language_arabic);

            holder.layout = (RelativeLayout) row
                    .findViewById(R.id.menu_profile_layout);
            holder.sidemenu_list_layout = (RelativeLayout) row
                    .findViewById(R.id.sidemenu_list_layout);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        languagePrefs = context.getSharedPreferences("LANGUAGE_PREFS",Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

//        holder.dlanguage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (language.equalsIgnoreCase("En")) {
//                    languagePrefsEditor.putString("language", "Ar");
//                    languagePrefsEditor.commit();
//                    Intent intent = new Intent(context, MainActivity.class);
//                    context.startActivity(intent);
//                }else {
//                    languagePrefsEditor.putString("language", "En");
//                    languagePrefsEditor.commit();
//                    Intent intent = new Intent(context, MainActivity.class);
//                    context.startActivity(intent);
//                }
//            }
//        });

        if (data[position].equalsIgnoreCase("language") || data[position].equalsIgnoreCase("اللغة")) {
            holder.dlanguage.setVisibility(View.VISIBLE);
        }else {
            holder.dlanguage.setVisibility(View.GONE);
        }

        if (position == 0) {
            holder.sidemenu_list_layout.setVisibility(View.GONE);
            holder.layout.setVisibility(View.VISIBLE);
        } else {
            holder.layout.setVisibility(View.GONE);
            holder.sidemenu_list_layout.setVisibility(View.VISIBLE);
        }

        if(checkedPosition == position){
            holder.sidemenu_list_layout.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }
        else{
            holder.sidemenu_list_layout.setBackgroundColor(Color.TRANSPARENT);
        }

        holder.txtTitle.setTextColor(Color.WHITE);
        holder.imgIcon.setImageResource(unselected_icons[position]);
        holder.txtTitle.setText(data[position]);


        return row;
    }

    static class ViewHolder {

        RelativeLayout layout,sidemenu_list_layout;
        ImageView imgIcon;
        TextView txtTitle , dlanguage;
    }
}